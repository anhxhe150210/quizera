const questionButtons = document.querySelectorAll('#pane__questions div')
let recheckButtons = () => {
    for (const questionButton of questionButtons) {
        const questionid = questionButton.getAttribute('questionid')
        const questionInputs = document.querySelectorAll('#form__question__' + questionid + ' input')
        let checked = false
        for (const questionInput of questionInputs) {
            if (questionInput.checked) {
                checked = true
                break
            }
        }
        if (checked) {
            questionButton.classList.add('bg-gray-500')
        } else {
            questionButton.classList.remove('bg-gray-500')
        }
    }
}

const popupContainer = document.querySelector('#popupContainer')

let currentQuestion = 1;
const maxQuestions = document.querySelectorAll('.question').length;
const practiceId = document.querySelector('#practiceId').innerHTML;
const baseUrl = document.querySelector('#baseUrl').innerHTML;

let setAnswer = async (questionId, answerId, ticked) => {
    await fetch(baseUrl + '/practices/save?practiceId=' + practiceId + '&questionId=' + questionId + '&answerId=' + answerId + '&ticked=' + (ticked ? 1 : 0))
}

// Peek at Answer
const peekAtAnswerPopup = document.querySelector('#popup__peekAtAnswer')
const peekAtAnswer = document.querySelector('#button__peekAtAnswer')
const peekAtAnswerClose = document.querySelector('#button__peekAtAnswer_close')

peekAtAnswer.addEventListener('click', ev => {
    popupContainer.classList.remove('hidden')
    peekAtAnswerPopup.classList.remove('hidden')
})
peekAtAnswerClose.addEventListener('click', ev => {
    popupContainer.classList.add('hidden')
    peekAtAnswerPopup.classList.add('hidden')
})


// Review Progress
const reviewProgressPopup = document.querySelector('#popup__reviewProgress')
const reviewProgress = document.querySelector('#button__reviewProgress')
const reviewProgressClose = document.querySelector('#button__reviewProgress_close')

reviewProgress.addEventListener('click', ev => {
    popupContainer.classList.remove('hidden')
    reviewProgressPopup.classList.remove('hidden')
})
reviewProgressClose.addEventListener('click', ev => {
    popupContainer.classList.add('hidden')
    reviewProgressPopup.classList.add('hidden')
})


// Convert question to proper html
const questionContents = document.querySelectorAll('.questionContent')
const edjsParser = edjsHTML();
for (const questionContent of questionContents) {
    questionContent.innerHTML = edjsParser.parse(JSON.parse(questionContent.innerHTML)).join('\n');
}

let changeQuestion = (n) => {
    document.querySelector('#question__' + currentQuestion).classList.add('hidden')
    currentQuestion = Number(n)
    document.querySelector('#question__' + currentQuestion).classList.remove('hidden')
}

const next = document.querySelector('#next')
next.addEventListener('click', ev => {
    changeQuestion(currentQuestion === maxQuestions ? 1 : currentQuestion + 1)
})

const inputs = document.querySelectorAll('input')
for (const input of inputs) {
    input.addEventListener('change', async ev => {
        const questionId = input.getAttribute('questionid')
        const answerId = input.getAttribute('answerid')
        // console.log('PracticeID: ' + practiceId)
        // console.log('QuestionID: ' + questionId + ', AnswerID: ' + answerId + ', ticked = ' + ev.target.checked)
        await setAnswer(questionId, answerId, ev.target.checked)
        recheckButtons()
    })
}

for (const questionButton of questionButtons) {
    questionButton.addEventListener('click', event => {
        const count = questionButton.getAttribute('count')
        reviewProgressClose.click()
        changeQuestion(count)
    })
}

const filterUnanswered = document.querySelector('#filter__unanswered')
const filterAnswered = document.querySelector('#filter__answered')
const filterAll = document.querySelector('#filter__all')

filterUnanswered.addEventListener('click', ev => {
    document.querySelectorAll('.button__filter').forEach(e => {
        e.classList.remove('bg-gray-500')
        e.classList.remove('text-white')
        e.classList.add('text-gray-500')
    })

    filterUnanswered.classList.add('bg-gray-500')
    filterUnanswered.classList.remove('text-gray-500')
    filterUnanswered.classList.add('text-white')

    questionButtons.forEach(e => {
        e.classList.remove('hidden')
        if (e.classList.contains('bg-gray-500')) {
            e.classList.add('hidden')
        }
    })
})

filterAnswered.addEventListener('click', ev => {
    document.querySelectorAll('.button__filter').forEach(e => {
        e.classList.remove('bg-gray-500')
        e.classList.remove('text-white')
        e.classList.add('text-gray-500')
    })
    filterAnswered.classList.add('bg-gray-500')
    filterAnswered.classList.remove('text-gray-500')
    filterAnswered.classList.add('text-white')

    questionButtons.forEach(e => {
        e.classList.remove('hidden')
        if (!e.classList.contains('bg-gray-500')) {
            e.classList.add('hidden')
        }
    })
})

filterAll.addEventListener('click', ev => {
    document.querySelectorAll('.button__filter').forEach(e => {
        e.classList.remove('bg-gray-500')
        e.classList.remove('text-white')
        e.classList.add('text-gray-500')
    })
    filterAll.classList.add('bg-gray-500')
    filterAll.classList.remove('text-gray-500')
    filterAll.classList.add('text-white')

    questionButtons.forEach(e => {
        e.classList.remove('hidden')
    })
})

// Countdown
const zeroPad = (num, places) => String(num).padStart(places, '0')

const countdownSpans = document.querySelectorAll('.countdown')
const countDownDate = new Date(countdownSpans[0].innerHTML * 1000)
countdownSpans.forEach(e => e.innerHTML = '??:??:??')

const x = setInterval(function () {
    // Get today's date and time
    const now = new Date().getTime()

    // Find the distance between now and the count down date
    const distance = countDownDate - now

    // Time calculations for days, hours, minutes and seconds
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
    const seconds = Math.floor((distance % (1000 * 60)) / 1000)

    // Display the result in the element with id="demo"
    countdownSpans.forEach(e => e.innerHTML = zeroPad(hours, 2) + ':' + zeroPad(minutes, 2) + ':' + zeroPad(seconds, 2))

    // If the count down is finished, write some text
    if (distance < 0) {
        clearInterval(x)
        countdownSpans.forEach(e => e.innerHTML = "EXPIRED")
        window.location.replace(baseUrl + '/practices/review?id=' + practiceId)
    }
}, 1000)


recheckButtons()
