import EditorJS from '@editorjs/editorjs';
import Header from '@editorjs/header';
import SimpleImage from '@editorjs/simple-image'

const questionElement = document.getElementById("question");

const questionEditor = new EditorJS({
    holder: 'editorjs',

    tools: {
        header: Header,
        image: SimpleImage,
    },

    onChange: (api) => {
        api.saver.save().then((data) => {
            console.log(data)
            questionElement.value = JSON.stringify(data)
        })
    },

    data: (() => {
        try {
            console.log(questionElement.value)
            return JSON.parse(questionElement.value)
        } catch (e) {
            return {}
        }
    })()
})
