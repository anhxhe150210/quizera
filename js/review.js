const questionButtons = document.querySelectorAll('#pane__questions div')
let recheckButtons = () => {
    for (const questionButton of questionButtons) {
        const questionid = questionButton.getAttribute('questionid')
        const questionInputs = document.querySelectorAll('#form__question__' + questionid + ' input')
        let checked = false
        for (const questionInput of questionInputs) {
            if (questionInput.checked) {
                checked = true
                break
            }
        }
        if (checked) {
            questionButton.classList.add('bg-gray-500')
        } else {
            questionButton.classList.remove('bg-gray-500')
        }
    }
}

const popupContainer = document.querySelector('#popupContainer')

let currentQuestion = 1;
const maxQuestions = document.querySelectorAll('.question').length;
const practiceId = document.querySelector('#practiceId').innerHTML;
const baseUrl = document.querySelector('#baseUrl').innerHTML;

// Review Progress
const reviewProgressPopup = document.querySelector('#popup__reviewProgress')
const reviewProgress = document.querySelector('#button__reviewProgress')
const reviewProgressClose = document.querySelector('#button__reviewProgress_close')

reviewProgress.addEventListener('click', ev => {
    popupContainer.classList.remove('hidden')
    reviewProgressPopup.classList.remove('hidden')
})
reviewProgressClose.addEventListener('click', ev => {
    popupContainer.classList.add('hidden')
    reviewProgressPopup.classList.add('hidden')
})


// Convert question to proper html
const questionContents = document.querySelectorAll('.questionContent')
const edjsParser = edjsHTML();
for (const questionContent of questionContents) {
    questionContent.innerHTML = edjsParser.parse(JSON.parse(questionContent.innerHTML)).join('\n');
}

let changeQuestion = (n) => {
    document.querySelector('#question__' + currentQuestion).classList.add('hidden')
    currentQuestion = Number(n)
    document.querySelector('#question__' + currentQuestion).classList.remove('hidden')
}

const next = document.querySelector('#next')
next.addEventListener('click', ev => {
    changeQuestion(currentQuestion === maxQuestions ? 1 : currentQuestion + 1)
})

for (const questionButton of questionButtons) {
    questionButton.addEventListener('click', event => {
        const count = questionButton.getAttribute('count')
        reviewProgressClose.click()
        changeQuestion(count)
    })
}

const filterIncorrect = document.querySelector('#filter__incorrect')
const filterAnswered = document.querySelector('#filter__answered')
const filterAll = document.querySelector('#filter__all')

filterIncorrect.addEventListener('click', ev => {
    document.querySelectorAll('.button__filter').forEach(e => {
        e.classList.remove('bg-gray-500')
        e.classList.remove('text-white')
        e.classList.add('text-gray-500')
    })

    filterIncorrect.classList.add('bg-gray-500')
    filterIncorrect.classList.remove('text-gray-500')
    filterIncorrect.classList.add('text-white')

    questionButtons.forEach(e => {
        e.classList.remove('hidden')
        if (!e.hasAttribute('incorrect')) {
            e.classList.add('hidden')
        }
    })
})

filterAnswered.addEventListener('click', ev => {
    document.querySelectorAll('.button__filter').forEach(e => {
        e.classList.remove('bg-gray-500')
        e.classList.remove('text-white')
        e.classList.add('text-gray-500')
    })
    filterAnswered.classList.add('bg-gray-500')
    filterAnswered.classList.remove('text-gray-500')
    filterAnswered.classList.add('text-white')

    questionButtons.forEach(e => {
        e.classList.remove('hidden')
        if (e.hasAttribute('unanswered')) {
            e.classList.add('hidden')
        }
    })
})

filterAll.addEventListener('click', ev => {
    document.querySelectorAll('.button__filter').forEach(e => {
        e.classList.remove('bg-gray-500')
        e.classList.remove('text-white')
        e.classList.add('text-gray-500')
    })
    filterAll.classList.add('bg-gray-500')
    filterAll.classList.remove('text-gray-500')
    filterAll.classList.add('text-white')

    questionButtons.forEach(e => {
        e.classList.remove('hidden')
    })
})


recheckButtons()
