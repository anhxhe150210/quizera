-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Jul 19, 2021 at 04:54 PM
-- Server version: 8.0.25
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quizera`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer`
(
    `id`          int        NOT NULL,
    `questionId`  int                 DEFAULT NULL,
    `answer`      text,
    `correct`     tinyint(1) NOT NULL DEFAULT '0',
    `explanation` text
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`id`, `questionId`, `answer`, `correct`, `explanation`)
VALUES (1, 1, 'It is a link that is stored consecutively in memory, like the primitive array.', 0, NULL),
       (2, 1, 'It is a link which elements has a reference to the another element.', 1, NULL),
       (3, 1, 'It is a metal chain link.', 0, NULL),
       (4, 1, 'It is a Cuban link.', 0, NULL),
       (5, 2, 'It sorts a dataset from the help of another mechanical machine.', 0, NULL),
       (6, 2, 'Selection Sort is Bubble Sort.', 0, NULL),
       (7, 2, 'Selection Sort selects a guaranteed correct value for each index.', 1, NULL),
       (8, 3, 'Yes', 1, NULL),
       (9, 3, 'No', 0, NULL),
       (58, 28, 'iserCeliF', 1, NULL),
       (59, 28, 'FAEKCDHGB', 0, NULL),
       (60, 28, 'Traversal', 0, NULL),
       (61, 28, 'enqueue', 0, NULL),
       (62, 29, '100', 0, NULL),
       (63, 29, '87', 0, NULL),
       (64, 29, '3', 1, NULL),
       (65, 29, '4', 0, NULL),
       (70, 31, 'Quick sort', 0, NULL),
       (71, 31, 'Queues', 1, NULL),
       (72, 31, 'Trees', 0, NULL),
       (73, 31, 'True', 0, NULL),
       (74, 32, ' pop', 0, NULL),
       (75, 32, '3', 1, NULL),
       (76, 32, ' Arrays', 0, NULL),
       (77, 32, 'It deletes the node p', 0, NULL),
       (78, 33, 'Arrays', 0, NULL),
       (79, 33, ' FIFO lists', 0, NULL),
       (80, 33, 'Store a waiting list of printing', 0, NULL),
       (81, 33, 'In push operation = False.', 1, NULL),
       (82, 34, 'thread', 0, NULL),
       (83, 34, 'Tree', 1, NULL),
       (84, 34, 'True', 0, NULL),
       (85, 34, 'Deque', 0, NULL),
       (86, 35, 'it deletes the node after p.', 0, NULL),
       (87, 35, ' At the head', 0, NULL),
       (88, 35, 'Input-restricted deque', 1, NULL),
       (89, 35, ' internal nodes on extended tree', 0, NULL),
       (90, 36, 'dequeue', 0, NULL),
       (91, 36, 'linear arrays', 0, NULL),
       (92, 36, 'Recursion', 1, NULL),
       (93, 36, 'Null case', 0, NULL),
       (94, 37, 'Traversal', 0, NULL),
       (95, 37, 'Trees', 0, NULL),
       (96, 37, ' True', 0, NULL),
       (97, 37, 'Tree', 1, NULL),
       (98, 38, 'sorted binary trees', 0, NULL),
       (99, 38, ' O(n log n)', 0, NULL),
       (100, 38, 'Extended binary tree', 1, NULL),
       (101, 38, 'B.e begins at u and ends at v C. u is processor and v is successor', 0, NULL),
       (102, 39, '(Strings, Lists, Stacks)= False.', 0, NULL),
       (103, 39, 'Managing function calls, The stock span problem, Arithmetic expression evaluation.', 1, NULL),
       (104, 39,
        'because initialization of data members of the LinkedList class is performed by the constructor of the LinkedList class.',
        0, NULL),
       (105, 39,
        'by this way computer can keep track only the address of the first element and the addresses of other elements can be calculated',
        0, NULL),
       (106, 40, 'size of variable name + b. size of (struct tag)', 0, NULL),
       (107, 40, ' base address', 0, NULL),
       (108, 40, 'Dn = log2n + 1', 1, NULL),
       (109, 40, 'underflow', 0, NULL),
       (110, 41, 'subtraction (/sÉbËtrÃ¦kÊn/ )', 1, NULL),
       (111, 41, 'correlation (/ËkÉËrÉËleÉªÊn/)', 0, NULL),
       (112, 41, 'statistics (/stÉËtÉªstÉªk/)', 0, NULL),
       (113, 41, 'problem (/ËprÉËblÉm/)', 0, NULL),
       (114, 42, 'width', 0, NULL),
       (115, 42, 'angle', 1, NULL),
       (116, 42, 'total', 0, NULL),
       (117, 42, 'area', 0, NULL),
       (118, 43, 'fraction', 0, NULL),
       (119, 43, 'correlation', 1, NULL),
       (120, 43, 'dimensions', 0, NULL),
       (121, 43, 'diamond', 0, NULL),
       (122, 44, 'pyramid', 0, NULL),
       (123, 44, 'percentage', 0, NULL),
       (124, 44, 'geometry', 0, NULL),
       (125, 44, 'perimeter', 1, NULL),
       (126, 45, 'trapezoid', 0, NULL),
       (127, 45, 'right angle', 1, NULL),
       (128, 45, 'arithmetic', 0, NULL),
       (129, 45, 'rectangle', 0, NULL),
       (130, 46, 'probability', 1, NULL),
       (131, 46, 'perimeter', 0, NULL),
       (132, 46, 'circumference', 0, NULL),
       (133, 46, 'percentage', 0, NULL),
       (134, 47, 'fraction', 0, NULL),
       (135, 47, 'addition', 1, NULL),
       (136, 47, 'solution', 0, NULL),
       (137, 47, 'division', 0, NULL),
       (138, 48, 'probability', 0, NULL),
       (139, 48, 'percentage', 0, NULL),
       (140, 48, 'arithmetic', 0, NULL),
       (141, 48, 'perimeter', 1, NULL),
       (142, 49, 'algebra', 0, NULL),
       (143, 49, 'interger', 0, NULL),
       (144, 49, 'tangent', 1, NULL),
       (145, 49, 'theorem', 0, NULL),
       (146, 50, 'length', 0, NULL),
       (147, 50, 'proof', 0, NULL),
       (148, 50, 'angle', 0, NULL),
       (149, 50, 'area', 1, NULL),
       (150, 51, 'length', 0, NULL),
       (151, 51, 'oval', 0, NULL),
       (152, 51, 'cone', 0, NULL),
       (153, 51, 'area', 1, NULL),
       (154, 52, 'radius,cylinder', 0, NULL),
       (155, 52, 'percent', 0, NULL),
       (156, 52, 'diameter', 1, NULL),
       (157, 53, 'height', 0, NULL),
       (158, 53, 'width', 1, NULL),
       (159, 53, 'length', 0, NULL),
       (160, 53, 'graph', 0, NULL),
       (161, 54, 'source.addAction(handler)', 0, NULL),
       (162, 54, 'source.setOnAction(handler)', 0, NULL),
       (163, 54, 'source.setOnAction(handler)', 1, NULL),
       (164, 54, 'source.setActionHandler(handler)', 0, NULL),
       (165, 55, ' public void actionPerformed(ActionEvent e)', 0, NULL),
       (166, 55, 'public void actionPerformed(Event e)', 0, NULL),
       (167, 55, 'public void handle(ActionEvent e)', 1, NULL),
       (168, 55, 'public void handle(Event e)', 0, NULL),
       (169, 56, 'ta.setText(s)', 0, NULL),
       (170, 56, 'ta.appendText(s)', 1, NULL),
       (171, 56, 'ta.append(s)', 0, NULL),
       (172, 56, 'ta.insertText(s)', 0, NULL),
       (173, 57, 'chk.getSelected()', 0, NULL),
       (174, 57, 'chk.selected()', 0, NULL),
       (175, 57, 'chk.isSelected()', 1, NULL),
       (176, 57, 'chk.select()', 0, NULL),
       (177, 58, 'ActionEvent', 0, NULL),
       (178, 58, 'MouseEvent', 0, NULL),
       (179, 58, 'StageEvent', 1, NULL),
       (180, 58, 'WindowEvent', 0, NULL),
       (181, 59, 'ActionEvent', 0, NULL),
       (182, 59, 'Action', 0, NULL),
       (183, 59, 'EventHandler', 0, NULL),
       (184, 59, 'EventHandler<T>', 1, NULL),
       (185, 60, '0', 0, NULL),
       (186, 60, '1', 0, NULL),
       (187, 60, '2', 0, NULL),
       (188, 60, 'Unlimited', 1, NULL),
       (189, 61, 'ActionEvent', 0, NULL),
       (190, 61, 'MouseEvent', 0, NULL),
       (191, 61, 'StageEvent', 1, NULL),
       (192, 61, 'WindowEvent', 0, NULL),
       (193, 62, 'interpreter', 0, NULL),
       (194, 62, 'operator', 0, NULL),
       (195, 62, 'compiler', 1, NULL),
       (196, 62, 'built-in-func', 0, NULL),
       (197, 63, 'compilers', 1, NULL),
       (198, 63, 'dictionary', 0, NULL),
       (199, 63, 'custom func', 0, NULL),
       (200, 63, 'interpreters', 0, NULL),
       (201, 64, 'numbers strings lists tuples dictionaries', 0, NULL),
       (202, 64, 'maps', 0, NULL),
       (203, 64, 'Numpy', 1, NULL),
       (204, 64, '%c', 0, NULL),
       (205, 65, '+', 0, NULL),
       (206, 65, 'maps', 0, NULL),
       (207, 65, 'pip', 1, NULL),
       (208, 65, '__init(self)__', 0, NULL),
       (209, 66, 'pip', 0, NULL),
       (210, 66, 'floor', 0, NULL),
       (211, 66, 'while', 0, NULL),
       (212, 66, 'Numpy', 1, NULL),
       (213, 67, '#', 1, NULL),
       (214, 67, '+', 0, NULL),
       (215, 67, '-', 0, NULL),
       (216, 67, '/', 0, NULL),
       (217, 68, 'new_string = quote + multi_line_quote', 0, NULL),
       (218, 68, 'sudo apt-get install build-essential libssl-dev libffi-dev python-dev', 1, NULL),
       (219, 68, 'sudo apt-get install', 0, NULL),
       (220, 69, 'Numpy', 1, NULL),
       (221, 69, '%c', 0, NULL),
       (222, 69, '%s', 0, NULL),
       (223, 69, 'maps', 0, NULL),
       (224, 70, 'while', 0, NULL),
       (225, 70, 'Dog', 0, NULL),
       (226, 70, '\'\'\'', 1, NULL),
       (227, 70, '%s', 0, NULL),
       (228, 71, '__init(self)__', 0, NULL),
       (229, 71, 'print (\"Hello World\")', 0, NULL),
       (230, 71, 'name = \"Derek\"', 1, NULL),
       (231, 71, 'I wi be there', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog`
(
    `id`           int          NOT NULL,
    `authorUser`   varchar(255) DEFAULT NULL,
    `title`        varchar(255) NOT NULL,
    `content`      text,
    `publicDate`   datetime     DEFAULT NULL,
    `thumbnailUrl` text
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `authorUser`, `title`, `content`, `publicDate`, `thumbnailUrl`)
VALUES (1, 'an7', 'The Blog 1',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus nulla, feugiat vel molestie at, fringilla ut urna. Suspendisse interdum tortor eu nunc placerat molestie. Nam rhoncus diam sem, ut finibus justo luctus vel. Praesent id elit leo. Etiam pretium purus nulla, sed porta turpis malesuada non. Mauris lectus erat, placerat at odio nec, consectetur commodo neque. Donec tincidunt, urna id viverra accumsan, ipsum ante varius arcu, eget fermentum enim felis vulputate orci. Aenean et scelerisque mi, in accumsan neque. Etiam nec eros posuere, rutrum lacus et, consequat nunc. Duis mauris lacus, vulputate nec convallis a, porta at quam. Integer fringilla viverra nibh, id viverra turpis vehicula nec. Nam convallis dolor nec lorem vestibulum, in dapibus odio maximus.\r\n\r\nDonec eu enim consequat, pharetra turpis ac, efficitur ligula. Nulla fringilla aliquam massa id porta. Suspendisse vel nunc vel ligula congue lacinia. Sed orci orci, vehicula eget venenatis at, ornare et velit. Curabitur hendrerit vestibulum rhoncus. Morbi eget leo et dui consequat lacinia. Pellentesque dignissim purus consectetur convallis dignissim. Nunc rutrum, orci sit amet vehicula congue, velit sem venenatis dui, vel porta est nunc quis augue. Ut neque massa, vestibulum ac nisi id, convallis pellentesque orci. Nunc blandit posuere lectus dictum scelerisque.\r\n\r\nQuisque vel viverra nibh, ac posuere mi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam congue odio non purus commodo auctor. Sed libero dui, blandit at neque at, laoreet blandit mi. Cras tempus dolor a sapien pharetra, vel bibendum eros pellentesque. Donec a urna erat. Vivamus non pharetra magna. Phasellus efficitur leo vel purus ornare mattis. Fusce quis viverra massa, eget vulputate ligula. Nunc id purus elementum, molestie diam vel, faucibus dolor. Sed in lectus vel erat fermentum scelerisque. Vivamus vulputate sodales aliquet.\r\n\r\nAenean magna nibh, vulputate in faucibus et, consequat sit amet nulla. Fusce ut facilisis eros, eu rutrum eros. Morbi sed luctus nisi, vitae volutpat ipsum. Etiam id dictum orci, sit amet hendrerit mauris. Integer sed est congue, vulputate ex at, blandit libero. Mauris turpis odio, lobortis at lorem et, ornare sodales diam. Nullam a enim lobortis, lobortis eros non, dignissim justo. Praesent faucibus tellus quis sapien tincidunt, eget blandit justo bibendum.\r\n\r\nQuisque dapibus elit non tellus rutrum convallis. In vestibulum feugiat varius. Sed fermentum odio nisi, sed pretium urna pulvinar non. Duis eget tristique dui, vel tincidunt ligula. Curabitur sed tristique leo. Praesent sed turpis sollicitudin, fringilla tellus quis, blandit lacus. Nam vitae ornare tortor. Cras mattis malesuada tellus et finibus. Vestibulum porttitor pulvinar efficitur. Proin ultricies at tortor sed fringilla. In risus est, fringilla eget dapibus non, consequat non neque. Ut ultrices, ante vel accumsan tincidunt, massa nisi bibendum ipsum, et consequat justo elit sed nisi.',
        '2021-05-29 12:31:06', 'http://placekitten.com/1000/500'),
       (2, 'marketing', 'The Blog 2',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse purus nulla, feugiat vel molestie at, fringilla ut urna. Suspendisse interdum tortor eu nunc placerat molestie. Nam rhoncus diam sem, ut finibus justo luctus vel. Praesent id elit leo. Etiam pretium purus nulla, sed porta turpis malesuada non. Mauris lectus erat, placerat at odio nec, consectetur commodo neque. Donec tincidunt, urna id viverra accumsan, ipsum ante varius arcu, eget fermentum enim felis vulputate orci. Aenean et scelerisque mi, in accumsan neque. Etiam nec eros posuere, rutrum lacus et, consequat nunc. Duis mauris lacus, vulputate nec convallis a, porta at quam. Integer fringilla viverra nibh, id viverra turpis vehicula nec. Nam convallis dolor nec lorem vestibulum, in dapibus odio maximus.\r\n\r\nDonec eu enim consequat, pharetra turpis ac, efficitur ligula. Nulla fringilla aliquam massa id porta. Suspendisse vel nunc vel ligula congue lacinia. Sed orci orci, vehicula eget venenatis at, ornare et velit. Curabitur hendrerit vestibulum rhoncus. Morbi eget leo et dui consequat lacinia. Pellentesque dignissim purus consectetur convallis dignissim. Nunc rutrum, orci sit amet vehicula congue, velit sem venenatis dui, vel porta est nunc quis augue. Ut neque massa, vestibulum ac nisi id, convallis pellentesque orci. Nunc blandit posuere lectus dictum scelerisque.\r\n\r\nQuisque vel viverra nibh, ac posuere mi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam congue odio non purus commodo auctor. Sed libero dui, blandit at neque at, laoreet blandit mi. Cras tempus dolor a sapien pharetra, vel bibendum eros pellentesque. Donec a urna erat. Vivamus non pharetra magna. Phasellus efficitur leo vel purus ornare mattis. Fusce quis viverra massa, eget vulputate ligula. Nunc id purus elementum, molestie diam vel, faucibus dolor. Sed in lectus vel erat fermentum scelerisque. Vivamus vulputate sodales aliquet.\r\n\r\nAenean magna nibh, vulputate in faucibus et, consequat sit amet nulla. Fusce ut facilisis eros, eu rutrum eros. Morbi sed luctus nisi, vitae volutpat ipsum. Etiam id dictum orci, sit amet hendrerit mauris. Integer sed est congue, vulputate ex at, blandit libero. Mauris turpis odio, lobortis at lorem et, ornare sodales diam. Nullam a enim lobortis, lobortis eros non, dignissim justo. Praesent faucibus tellus quis sapien tincidunt, eget blandit justo bibendum.\r\n\r\nQuisque dapibus elit non tellus rutrum convallis. In vestibulum feugiat varius. Sed fermentum odio nisi, sed pretium urna pulvinar non. Duis eget tristique dui, vel tincidunt ligula. Curabitur sed tristique leo. Praesent sed turpis sollicitudin, fringilla tellus quis, blandit lacus. Nam vitae ornare tortor. Cras mattis malesuada tellus et finibus. Vestibulum porttitor pulvinar efficitur. Proin ultricies at tortor sed fringilla. In risus est, fringilla eget dapibus non, consequat non neque. Ut ultrices, ante vel accumsan tincidunt, massa nisi bibendum ipsum, et consequat justo elit sed nisi.',
        '2021-05-29 12:31:09', 'http://placekitten.com/1000/501'),
       (3, 'an7', 'The new blog',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vulputate quis mauris a dignissim. Ut eleifend odio ut libero ullamcorper, id imperdiet purus sagittis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin tristique lorem diam, et malesuada augue aliquet at. Nunc dui arcu, rutrum cursus dui vel, molestie porttitor quam. Pellentesque et molestie odio. Mauris bibendum tellus orci, ac tempus mauris tristique imperdiet. Cras eu felis a nulla cursus sollicitudin et eget dolor. Phasellus sed congue tortor, vel facilisis odio. Pellentesque eget euismod tellus, sed dictum mi. Curabitur euismod justo sed dui sodales, nec maximus ex accumsan. Phasellus vitae lacus nec sapien gravida ultrices. Vestibulum rutrum nunc non ex tempus volutpat. Proin commodo sem at dignissim volutpat. Morbi auctor elit risus, nec pharetra augue aliquet sed. Nulla ut tellus tincidunt nisi mollis gravida.\r\n\r\nNunc fermentum sem risus, vitae scelerisque ligula elementum fringilla. Maecenas dictum pretium elit, eget lacinia ipsum. Nam ac quam id quam sollicitudin pulvinar. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus fermentum eros at sem luctus, non aliquet sem aliquet. Etiam interdum libero elit, in cursus metus semper eget. Curabitur augue lectus, aliquet quis libero eget, fringilla convallis erat. Mauris felis turpis, aliquet ac sodales eu, scelerisque vel risus. Morbi sollicitudin ante quis orci bibendum blandit. Proin fringilla nulla ac erat scelerisque, eu imperdiet neque eleifend. Duis gravida lacus non sapien iaculis semper sit amet a nulla. Phasellus vitae eros risus. Quisque egestas posuere erat quis feugiat. Mauris imperdiet consectetur augue et ornare.',
        '2021-05-29 00:00:00', 'http://placekitten.com/1000/502');

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

CREATE TABLE `blog_category`
(
    `blogId`     int NOT NULL,
    `categoryId` int NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `blog_category`
--

INSERT INTO `blog_category` (`blogId`, `categoryId`)
VALUES (2, 1),
       (1, 2),
       (2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `blog_category_list`
--

CREATE TABLE `blog_category_list`
(
    `id`          int          NOT NULL,
    `description` varchar(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `blog_category_list`
--

INSERT INTO `blog_category_list` (`id`, `description`)
VALUES (1, 'Announcement'),
       (2, 'News');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course`
(
    `id`            int          NOT NULL,
    `title`         varchar(512) NOT NULL,
    `ownerUsername` varchar(255)          DEFAULT NULL,
    `description`   text         NOT NULL,
    `updatedDate`   date         NOT NULL DEFAULT '1970-01-01',
    `featured`      tinyint(1)   NOT NULL DEFAULT '0',
    `thumbnailUrl`  text,
    `archived`      tinyint(1)   NOT NULL DEFAULT '0',
    `published`     tinyint(1)   NOT NULL DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `title`, `ownerUsername`, `description`, `updatedDate`, `featured`, `thumbnailUrl`,
                      `archived`, `published`)
VALUES (1, 'Data Structures and Algorithms', 'an7', 'Learn and do quizzes about Data Structures and Algorithms!',
        '2021-07-12', 1, 'https://cafedev.vn/wp-content/uploads/2020/07/cafedev_datastruct_alg.jpeg', 1, 1),
       (2, 'Database', 'an7', 'Hello database!', '2021-07-12', 1,
        'https://svitla.com/uploads_converted/0/2135-database_management_software.webp?1560161553', 1, 1),
       (4, 'Python for Data Science and Machine Learning', 'coursecontent',
        'Learn how to use NumPy, Pandas, Seaborn , Matplotlib , Plotly , Scikit-Learn , Machine Learning, Tensorflow , and more!',
        '2021-07-12', 1, 'https://codelearn.io/CodeCamp/CodeCamp/Upload/Course/cf55489ccd434e8c81c61e6fffc9433f.jpg', 1,
        1),
       (5, 'Advanced Java programming with JavaFx', 'coursecontent',
        'Put to work those hard earned Java programming skills! Use more than just the modern looks of JavaFX!',
        '2021-07-12', 1,
        'https://shrey150.gallerycdn.vsassets.io/extensions/shrey150/javafx-support/0.0.1/1600902126294/Microsoft.VisualStudio.Services.Icons.Default',
        1, 1),
       (6, 'Master the Fundamentals of Math', 'an7',
        'Learn everything from the basics of math, then test your knowledge with 510+ practice questions', '2021-07-12',
        1, 'https://92campus.com/wp-content/uploads/2020/08/maxresdefault.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `course_category`
--

CREATE TABLE `course_category`
(
    `courseId`   int NOT NULL,
    `categoryId` int NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `course_category`
--

INSERT INTO `course_category` (`courseId`, `categoryId`)
VALUES (1, 1),
       (2, 1),
       (4, 1),
       (1, 3),
       (2, 3),
       (4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `course_category_list`
--

CREATE TABLE `course_category_list`
(
    `id`          int          NOT NULL,
    `description` varchar(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `course_category_list`
--

INSERT INTO `course_category_list` (`id`, `description`)
VALUES (1, 'Development'),
       (2, 'Mathematics'),
       (3, 'Computer Science');

-- --------------------------------------------------------

--
-- Table structure for table `course_dimension`
--

CREATE TABLE `course_dimension`
(
    `id`        int          NOT NULL,
    `courseId`  int          NOT NULL,
    `type`      varchar(255) NOT NULL,
    `dimension` varchar(255) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `course_dimension`
--

INSERT INTO `course_dimension` (`id`, `courseId`, `type`, `dimension`)
VALUES (1, 1, 'List', 'About lists'),
       (2, 1, 'Tree', 'About trees'),
       (3, 1, 'Sorting', 'Sorting algorithms'),
       (7, 2, 'Data Structure: List', 'About Lists'),
       (8, 2, 'Data Structure: Tree', 'About Trees'),
       (9, 2, 'Sorting Algorithms', 'About sorts'),
       (10, 2, 'New Topic', 'Example topic: \"Data Structure: List\"');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender`
(
    `id`          int          NOT NULL,
    `description` varchar(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `description`)
VALUES (1, 'Male'),
       (2, 'Female'),
       (3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `lecture`
--

CREATE TABLE `lecture`
(
    `id`       int NOT NULL,
    `courseId` int          DEFAULT NULL,
    `title`    varchar(200) DEFAULT NULL,
    `content`  text
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `lecture`
--

INSERT INTO `lecture` (`id`, `courseId`, `title`, `content`)
VALUES (1, 1, 'LinkedList',
        'LinkedList comprised of Nodes, each node has a reference to another Node, like a chain. A -> B -> C -> D'),
       (2, 1, 'Binary Tree',
        'Binary Tree is a tree with a master node, from there each node only have at least 0 node and at most 2 nodes, called left-node and right-node.'),
       (3, 1, 'Selection Sort',
        'Selection Sort is an algorithm to sort a dataset. How it works is with each position, go through all the data to select a guaranteed appropriate data for that position.');

-- --------------------------------------------------------

--
-- Table structure for table `price`
--

CREATE TABLE `price`
(
    `id`       int            NOT NULL,
    `courseId` int            DEFAULT NULL,
    `days`     int            NOT NULL,
    `price`    decimal(20, 2) NOT NULL,
    `saleFrom` decimal(20, 2) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `price`
--

INSERT INTO `price` (`id`, `courseId`, `days`, `price`, `saleFrom`)
VALUES (1, 1, 29, '22.00', '31.00'),
       (4, 1, 60, '24.00', '12.30'),
       (5, 2, 30, '12.30', '12.30'),
       (6, 2, 60, '24.00', '24.00'),
       (7, 4, 30, '12.30', '12.30'),
       (8, 4, 60, '24.00', '24.00'),
       (9, 6, 30, '12.30', '12.30'),
       (10, 6, 120, '36.00', '36.00'),
       (11, 5, 30, '12.30', '12.30'),
       (12, 5, 60, '24.00', '24.00');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question`
(
    `id`          int NOT NULL,
    `courseId`    int DEFAULT NULL,
    `question`    text,
    `dimensionId` int DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `courseId`, `question`, `dimensionId`)
VALUES (1, 1,
        '{\"time\":1622128448210,\"blocks\":[{\"id\":\"Ndvrw5WU4X\",\"type\":\"paragraph\",\"data\":{\"text\":\"What is a LinkedList?\"}}],\"version\":\"2.21.0\"}',
        1),
       (2, 2,
        '{\"time\":1622128468988,\"blocks\":[{\"id\":\"-3skj0jup9\",\"type\":\"paragraph\",\"data\":{\"text\":\"How is a Selection Sort carried out?&nbsp;&nbsp;\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (3, 1,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"Is a LinkedList elements stored consecutively in memory?\"}}],\"version\":\"2.21.0\"}',
        1),
       (28, 1,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"What is written to the screen for the input \\\"FileComp***ression**\\\" ??\"}}],\"version\":\"2.21.0\"}',
        1),
       (29, 1,
        '{\"time\":1626085382019,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"Suppose the f(n) function is defined on the set of integer numbers as below. What is the value of f(-5)?\"}}],\"version\":\"2.21.0\"}',
        1),
       (31, 1,
        '{\"time\":1626086746959,\"blocks\":[{\"id\":\"nUzeFfD8Kb\",\"type\":\"paragraph\",\"data\":{\"text\":\"Which data structure allows deleting data elements from front and inserting at rear?\"}}],\"version\":\"2.21.0\"}',
        3),
       (32, 1,
        '{\"time\":1626086785686,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"Suppose the f(n) function is defined on the set of integer numbers as below. What is the value of f(-5)?\"}}],\"version\":\"2.21.0\"}',
        3),
       (33, 1,
        '{\"time\":1626086824006,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"Which of the following is true about linked list implementation of stack?\"}}],\"version\":\"2.21.0\"}',
        1),
       (34, 1,
        '{\"time\":1626086898546,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"To represent hierarchical relationship between elements, which data structure is suitable?\"}}],\"version\":\"2.21.0\"}',
        2),
       (35, 1,
        '{\"time\":1626086931804,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"Identify the data structure which allows deletions at both ends of the list but insertion at only one end\"}}],\"version\":\"2.21.0\"}',
        1),
       (36, 1,
        '{\"time\":1626086957582,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"An algorithm that calls itself directly or indirectly is known as\"}}],\"version\":\"2.21.0\"}',
        3),
       (37, 1,
        '{\"time\":1626086992265,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"To represent hierarchical relationship between elements, which data structure is suitable?\"}}],\"version\":\"2.21.0\"}',
        2),
       (38, 1,
        '{\"time\":1626087026262,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"A binary tree whose every node has either zero or two children is called\"}}],\"version\":\"2.21.0\"}',
        2),
       (39, 1,
        '{\"time\":1626087062696,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"Which one of the following is an application of Stack Data Structure?\"}}],\"version\":\"2.21.0\"}',
        3),
       (40, 1,
        '{\"time\":1626087104683,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"The depth of a complete binary tree is given by\"}}],\"version\":\"2.21.0\"}',
        2),
       (41, 6,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"a connection or relationship between two or more facts, numbers, etc\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (42, 6,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"the space between two lines or surfaces at the point at which they touch each other, measured in degrees\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (43, 6,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"a connection or relationship between two or more facts, numbers, etc.\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (44, 6,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"the outer edge of an area of land or the border around it\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (45, 6,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"an angle of 90\\u00C2\\u00B0\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (46, 6,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"the level of possibility of something happening or being true\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (47, 6,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"the process of adding numbers or amounts together\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (48, 6,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"the outer edge of an area of land or the border around it\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (49, 6,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"a straight line that touches the outside of a curve but does not cross it\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (50, 6,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"the space between two lines or surfaces at the point at which they touch each other, measured in degrees\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (51, 6,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"the size of a flat surface calculated by multiplying its length by its width\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (52, 6,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"a straight line that reaches from one point on the edge of a round shape or object, through its center, to a point on the opposite edge\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (53, 6,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"the distance across something from one side to the other\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (54, 5,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"To register a source for an action event with a handler, use __________\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (55, 5,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"A JavaFX action event handler contains a method ________\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (56, 5,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"The method __________ appends a string s into the text area ta\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (57, 5,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"________ checks whether the CheckBox chk is selected.\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (58, 5,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"Which of the following are not classes in JavaFX for representing an event?\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (59, 5,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"A JavaFX event handler for event type T is an instance of _______.\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (60, 5,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"How many items can be added into a ComboBox object?\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (61, 5,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"Which of the following are not classes in JavaFX for representing an event?\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (62, 4,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"Computer program transforms code written in programming language by compiling to a lower level machine language that can be executed only on a target machine\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (63, 4,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"Need recompile for changes\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (64, 4,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"a scientific computing python package\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (65, 4,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"manages software packages for python\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (66, 4,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"a scientific computing python package\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (67, 4,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"Tag for a single line comment\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (68, 4,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"few more packages and development tools to install to ensure robust set-up of programming environment\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (69, 4,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"a scientific computing python package\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (70, 4,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"Tag for a multiline comment\"}}],\"version\":\"2.21.0\"}',
        NULL),
       (71, 4,
        '{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"Store \\\"Derek\\\" to a variable named \'name\'\"}}],\"version\":\"2.21.0\"}',
        NULL);

-- --------------------------------------------------------

--
-- Table structure for table `question_dimension`
--

CREATE TABLE `question_dimension`
(
    `questionId`  int NOT NULL,
    `dimensionId` int NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `question_dimension`
--

INSERT INTO `question_dimension` (`questionId`, `dimensionId`)
VALUES (1, 1),
       (3, 1),
       (1, 3),
       (2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz`
(
    `id`                int NOT NULL,
    `courseId`          int          DEFAULT NULL,
    `title`             varchar(200) DEFAULT NULL,
    `levelId`           int          DEFAULT NULL,
    `durationInSeconds` int          DEFAULT NULL,
    `numberOfQuestions` int          DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`id`, `courseId`, `title`, `levelId`, `durationInSeconds`, `numberOfQuestions`)
VALUES (1, 1, 'Practice Quiz 1', 2, 900, 20),
       (2, 1, 'Practice Quiz 2', 3, 30, 20),
       (11, 1, 'Practice Quiz 3', 1, 900, 20),
       (12, 2, 'Practice Quiz 1', 2, 900, 20),
       (13, 2, 'Final Exam', 3, 900, 20),
       (14, 2, 'Pratice Exam 1', 3, 900, 20),
       (15, 4, 'Practice Quiz ', 1, 900, 20),
       (16, 4, 'Pratice Exam ', 2, 900, 20),
       (17, 4, 'Final Exam ', 3, 900, 20),
       (18, 5, 'Basics Quizzes ', 1, 900, 20),
       (19, 5, 'Pratices Quiz 1 ', 2, 900, 20),
       (20, 5, 'Exam Practice ', 1, 900, 20),
       (21, 6, 'The Basics ', 1, 900, 20),
       (22, 6, 'Intermediate Quiz', 2, 900, 20),
       (23, 6, 'Final Exam', 3, 900, 20);

-- --------------------------------------------------------

--
-- Table structure for table `quiz_level_list`
--

CREATE TABLE `quiz_level_list`
(
    `id`          int          NOT NULL,
    `description` varchar(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `quiz_level_list`
--

INSERT INTO `quiz_level_list` (`id`, `description`)
VALUES (1, 'Easy'),
       (2, 'Medium'),
       (3, 'Hard');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_question`
--

CREATE TABLE `quiz_question`
(
    `quizId`            int          NOT NULL,
    `filterBy`          int          NOT NULL,
    `target`            varchar(255) NOT NULL,
    `numberOfQuestions` int          NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `quiz_question`
--

INSERT INTO `quiz_question` (`quizId`, `filterBy`, `target`, `numberOfQuestions`)
VALUES (1, 1, 'About lists', 4),
       (1, 1, 'Sorting algorithms', 3),
       (2, 1, 'Sorting algorithms', 20);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role`
(
    `id`          int          NOT NULL,
    `description` varchar(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `description`)
VALUES (1, 'Guest'),
       (2, 'Customer'),
       (3, 'Marketing'),
       (4, 'Sale'),
       (5, 'Expert'),
       (6, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider`
(
    `id`      int          NOT NULL,
    `title`   varchar(255) NOT NULL,
    `content` json         NOT NULL,
    `hidden`  tinyint(1)   NOT NULL DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user`
(
    `username`  varchar(255) NOT NULL,
    `password`  varchar(32)  NOT NULL,
    `roleId`    int          NOT NULL DEFAULT '2',
    `email`     varchar(200) NOT NULL,
    `verified`  tinyint(1)   NOT NULL DEFAULT '0',
    `fullName`  varchar(255)          DEFAULT NULL,
    `gender`    int                   DEFAULT NULL,
    `mobile`    varchar(255)          DEFAULT NULL,
    `avatarUrl` text
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `roleId`, `email`, `verified`, `fullName`, `gender`, `mobile`, `avatarUrl`)
VALUES ('an7', '202cb962ac59075b964b07152d234b70', 6, 'xuanan2001@gmail.com', 1, 'An Xuan Hoang', 1, '12341111',
        'http://placekitten.com/300/300'),
       ('coursecontent', '202cb962ac59075b964b07152d234b70', 5, 'intagaming@gmail.com', 0, 'Course Content', NULL, '',
        ''),
       ('marketing', '202cb962ac59075b964b07152d234b70', 3, 'anhxhe150210@fpt.edu.vn', 1, 'Marketing 1', NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_exam`
--

CREATE TABLE `user_exam`
(
    `id`       int          NOT NULL,
    `username` varchar(255) NOT NULL,
    `courseId` int          NOT NULL,
    `startAt`  datetime     NOT NULL,
    `endAt`    datetime     NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_exam_submission`
--

CREATE TABLE `user_exam_submission`
(
    `examId`     int NOT NULL,
    `questionId` int NOT NULL,
    `answerId`   int NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_practice`
--

CREATE TABLE `user_practice`
(
    `id`             int          NOT NULL,
    `username`       varchar(255) NOT NULL,
    `quizId`         int          NOT NULL,
    `startAt`        datetime     NOT NULL,
    `filterString`   text,
    `forceSubmitted` tinyint(1)   NOT NULL DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user_practice`
--

INSERT INTO `user_practice` (`id`, `username`, `quizId`, `startAt`, `filterString`, `forceSubmitted`)
VALUES (9, 'an7', 1, '2021-06-01 22:42:57', NULL, 1),
       (10, 'an7', 1, '2021-06-01 22:57:33', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_practice_answer`
--

CREATE TABLE `user_practice_answer`
(
    `practiceId` int NOT NULL,
    `questionId` int NOT NULL,
    `answerId`   int NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user_practice_answer`
--

INSERT INTO `user_practice_answer` (`practiceId`, `questionId`, `answerId`)
VALUES (9, 1, 2),
       (10, 1, 2),
       (9, 1, 3),
       (9, 1, 4),
       (10, 3, 8),
       (9, 3, 9);

-- --------------------------------------------------------

--
-- Table structure for table `user_practice_question`
--

CREATE TABLE `user_practice_question`
(
    `practiceId` int NOT NULL,
    `questionId` int NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user_practice_question`
--

INSERT INTO `user_practice_question` (`practiceId`, `questionId`)
VALUES (9, 1),
       (10, 1),
       (9, 3),
       (10, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_subscription`
--

CREATE TABLE `user_subscription`
(
    `id`       int          NOT NULL,
    `username` varchar(255) NOT NULL,
    `courseId` int          NOT NULL,
    `priceId`  int          NOT NULL,
    `fromDate` datetime     NOT NULL,
    `toDate`   datetime     NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answer`
--
ALTER TABLE `answer`
    ADD PRIMARY KEY (`id`),
    ADD KEY `answers_questions_id_fk` (`questionId`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
    ADD PRIMARY KEY (`id`),
    ADD KEY `blog_users_username_fk` (`authorUser`);

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
    ADD PRIMARY KEY (`blogId`, `categoryId`),
    ADD KEY `blog_categories_blog_category_list_id_fk` (`categoryId`);

--
-- Indexes for table `blog_category_list`
--
ALTER TABLE `blog_category_list`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
    ADD PRIMARY KEY (`id`),
    ADD KEY `courses_users_username_fk` (`ownerUsername`);

--
-- Indexes for table `course_category`
--
ALTER TABLE `course_category`
    ADD PRIMARY KEY (`courseId`, `categoryId`),
    ADD KEY `course_categories_course_category_list_id_fk` (`categoryId`);

--
-- Indexes for table `course_category_list`
--
ALTER TABLE `course_category_list`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_dimension`
--
ALTER TABLE `course_dimension`
    ADD PRIMARY KEY (`id`),
    ADD KEY `course_topics_courses_id_fk` (`courseId`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lecture`
--
ALTER TABLE `lecture`
    ADD PRIMARY KEY (`id`),
    ADD KEY `lecture_FK` (`courseId`);

--
-- Indexes for table `price`
--
ALTER TABLE `price`
    ADD PRIMARY KEY (`id`),
    ADD KEY `price_FK` (`courseId`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
    ADD PRIMARY KEY (`id`),
    ADD KEY `questions_quizzes_id_fk` (`courseId`),
    ADD KEY `question_FK_1` (`dimensionId`);

--
-- Indexes for table `question_dimension`
--
ALTER TABLE `question_dimension`
    ADD PRIMARY KEY (`questionId`, `dimensionId`),
    ADD KEY `question_topic_dimension_id_fk` (`dimensionId`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
    ADD PRIMARY KEY (`id`),
    ADD KEY `quizzes_lectures_id_fk` (`courseId`),
    ADD KEY `quizzes_quizzes_level_list_id_fk` (`levelId`);

--
-- Indexes for table `quiz_level_list`
--
ALTER TABLE `quiz_level_list`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_question`
--
ALTER TABLE `quiz_question`
    ADD KEY `quiz_question_FK` (`quizId`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
    ADD PRIMARY KEY (`username`),
    ADD KEY `users_roles_id_fk` (`roleId`),
    ADD KEY `users_gender_id_fk` (`gender`);

--
-- Indexes for table `user_exam`
--
ALTER TABLE `user_exam`
    ADD PRIMARY KEY (`id`),
    ADD KEY `user_exams_courses_id_fk` (`courseId`),
    ADD KEY `user_exams_users_username_fk` (`username`);

--
-- Indexes for table `user_exam_submission`
--
ALTER TABLE `user_exam_submission`
    ADD KEY `user_exam_submission_answers_id_fk` (`answerId`),
    ADD KEY `user_exam_submission_questions_id_fk` (`questionId`),
    ADD KEY `user_exam_submission_user_exams_id_fk` (`examId`);

--
-- Indexes for table `user_practice`
--
ALTER TABLE `user_practice`
    ADD PRIMARY KEY (`id`),
    ADD KEY `user_practices_quizzes_id_fk` (`quizId`),
    ADD KEY `user_practices_users_username_fk` (`username`);

--
-- Indexes for table `user_practice_answer`
--
ALTER TABLE `user_practice_answer`
    ADD PRIMARY KEY (`practiceId`, `questionId`, `answerId`),
    ADD KEY `user_practice_answer_answer_id_fk` (`answerId`),
    ADD KEY `user_practice_answer_question_id_fk` (`questionId`);

--
-- Indexes for table `user_practice_question`
--
ALTER TABLE `user_practice_question`
    ADD PRIMARY KEY (`practiceId`, `questionId`),
    ADD KEY `user_practice_question_question_id_fk` (`questionId`);

--
-- Indexes for table `user_subscription`
--
ALTER TABLE `user_subscription`
    ADD PRIMARY KEY (`id`),
    ADD KEY `user_subscriptions_course_prices_courseId_priceId_fk` (`courseId`, `priceId`),
    ADD KEY `user_subscriptions_users_username_fk` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer`
--
ALTER TABLE `answer`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 232;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 5;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 7;

--
-- AUTO_INCREMENT for table `course_dimension`
--
ALTER TABLE `course_dimension`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 11;

--
-- AUTO_INCREMENT for table `lecture`
--
ALTER TABLE `lecture`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 8;

--
-- AUTO_INCREMENT for table `price`
--
ALTER TABLE `price`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 13;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 72;

--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 24;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_exam`
--
ALTER TABLE `user_exam`
    MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_practice`
--
ALTER TABLE `user_practice`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 11;

--
-- AUTO_INCREMENT for table `user_subscription`
--
ALTER TABLE `user_subscription`
    MODIFY `id` int NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answer`
--
ALTER TABLE `answer`
    ADD CONSTRAINT `answers_questions_id_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blog`
--
ALTER TABLE `blog`
    ADD CONSTRAINT `blog_users_username_fk` FOREIGN KEY (`authorUser`) REFERENCES `user` (`username`) ON DELETE SET NULL;

--
-- Constraints for table `blog_category`
--
ALTER TABLE `blog_category`
    ADD CONSTRAINT `blog_categories_blog_category_list_id_fk` FOREIGN KEY (`categoryId`) REFERENCES `blog_category_list` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `blog_categories_blog_id_fk` FOREIGN KEY (`blogId`) REFERENCES `blog` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `course`
--
ALTER TABLE `course`
    ADD CONSTRAINT `courses_users_username_fk` FOREIGN KEY (`ownerUsername`) REFERENCES `user` (`username`) ON DELETE SET NULL;

--
-- Constraints for table `course_category`
--
ALTER TABLE `course_category`
    ADD CONSTRAINT `course_categories_course_category_list_id_fk` FOREIGN KEY (`categoryId`) REFERENCES `course_category_list` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `course_categories_courses_id_fk` FOREIGN KEY (`courseId`) REFERENCES `course` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `course_dimension`
--
ALTER TABLE `course_dimension`
    ADD CONSTRAINT `course_topics_courses_id_fk` FOREIGN KEY (`courseId`) REFERENCES `course` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `lecture`
--
ALTER TABLE `lecture`
    ADD CONSTRAINT `lecture_FK` FOREIGN KEY (`courseId`) REFERENCES `course` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `price`
--
ALTER TABLE `price`
    ADD CONSTRAINT `price_FK` FOREIGN KEY (`courseId`) REFERENCES `course` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `question`
--
ALTER TABLE `question`
    ADD CONSTRAINT `question_FK` FOREIGN KEY (`courseId`) REFERENCES `course` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `question_FK_1` FOREIGN KEY (`dimensionId`) REFERENCES `course_dimension` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `question_dimension`
--
ALTER TABLE `question_dimension`
    ADD CONSTRAINT `question_topic_dimension_id_fk` FOREIGN KEY (`dimensionId`) REFERENCES `course_dimension` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `question_topics_questions_id_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `quiz`
--
ALTER TABLE `quiz`
    ADD CONSTRAINT `quiz_FK` FOREIGN KEY (`courseId`) REFERENCES `course` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `quizzes_quizzes_level_list_id_fk` FOREIGN KEY (`levelId`) REFERENCES `quiz_level_list` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `quiz_question`
--
ALTER TABLE `quiz_question`
    ADD CONSTRAINT `quiz_question_FK` FOREIGN KEY (`quizId`) REFERENCES `quiz` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
    ADD CONSTRAINT `users_gender_id_fk` FOREIGN KEY (`gender`) REFERENCES `gender` (`id`),
    ADD CONSTRAINT `users_roles_id_fk` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`);

--
-- Constraints for table `user_exam`
--
ALTER TABLE `user_exam`
    ADD CONSTRAINT `user_exams_courses_id_fk` FOREIGN KEY (`courseId`) REFERENCES `course` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `user_exams_users_username_fk` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE;

--
-- Constraints for table `user_exam_submission`
--
ALTER TABLE `user_exam_submission`
    ADD CONSTRAINT `user_exam_submission_answers_id_fk` FOREIGN KEY (`answerId`) REFERENCES `answer` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `user_exam_submission_questions_id_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `user_exam_submission_user_exams_id_fk` FOREIGN KEY (`examId`) REFERENCES `user_exam` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_practice`
--
ALTER TABLE `user_practice`
    ADD CONSTRAINT `user_practices_quizzes_id_fk` FOREIGN KEY (`quizId`) REFERENCES `quiz` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `user_practices_users_username_fk` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE;

--
-- Constraints for table `user_practice_answer`
--
ALTER TABLE `user_practice_answer`
    ADD CONSTRAINT `user_practice_answer_answer_id_fk` FOREIGN KEY (`answerId`) REFERENCES `answer` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `user_practice_answer_question_id_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `user_practice_answer_user_practice_id_fk` FOREIGN KEY (`practiceId`) REFERENCES `user_practice` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_practice_question`
--
ALTER TABLE `user_practice_question`
    ADD CONSTRAINT `user_practice_question_question_id_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `user_practice_question_user_practice_id_fk` FOREIGN KEY (`practiceId`) REFERENCES `user_practice` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_subscription`
--
ALTER TABLE `user_subscription`
    ADD CONSTRAINT `user_subscriptions_users_username_fk` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
