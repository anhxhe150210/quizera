# Quizera

Quiz-central Management System

## Installation


1. Please run the docker-compose.yml file in `docker/` folder if you don't have a MySQL database running.
```shell
docker-compose up -d -f "docker/docker-compose.yml"
```
2. Import the MySQL database schema from `sql/` folder. Note that the file is the whole server dump (instead of an individual database).
3. Open the project. Set it up running using the __GlassFish 5__ configuration. Tomcat won't work.
4. Initialize the Maven project. You don't have to do this if you open the project using IntelliJ IDEA.
```shell
mvn install
```
5. Configure the MySQL configuration in the `dao` package (in the MysqlDao abstract class).
6. Run the project.
