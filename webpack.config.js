const path = require('path');

module.exports = {
    mode: "development",
    entry: {
        styles: './js/styles.js',
        questionEditor: './js/questionEditor.js',
        quiz: './js/quiz.js',
        review: './js/review.js',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'src/main/webapp'),
    },

    module: {
        rules: [
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                        }
                    },
                    {
                        loader: 'postcss-loader'
                    }
                ]
            }
        ]
    }
};