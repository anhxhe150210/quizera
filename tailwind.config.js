module.exports = {
  purge: [
    './src/main/webapp/WEB-INF/**/*.jsp',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {
      backgroundColor: ['active']
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
