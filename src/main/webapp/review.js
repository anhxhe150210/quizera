/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/
(() => { // webpackBootstrap
    /******/
    var __webpack_modules__ = ({

        /***/ "./js/review.js":
        /*!**********************!*\
          !*** ./js/review.js ***!
          \**********************/
        /***/ (() => {

            eval("const questionButtons = document.querySelectorAll('#pane__questions div')\r\nlet recheckButtons = () => {\r\n    for (const questionButton of questionButtons) {\r\n        const questionid = questionButton.getAttribute('questionid')\r\n        const questionInputs = document.querySelectorAll('#form__question__' + questionid + ' input')\r\n        let checked = false\r\n        for (const questionInput of questionInputs) {\r\n            if (questionInput.checked) {\r\n                checked = true\r\n                break\r\n            }\r\n        }\r\n        if (checked) {\r\n            questionButton.classList.add('bg-gray-500')\r\n        } else {\r\n            questionButton.classList.remove('bg-gray-500')\r\n        }\r\n    }\r\n}\r\n\r\nconst popupContainer = document.querySelector('#popupContainer')\r\n\r\nlet currentQuestion = 1;\r\nconst maxQuestions = document.querySelectorAll('.question').length;\r\nconst practiceId = document.querySelector('#practiceId').innerHTML;\r\nconst baseUrl = document.querySelector('#baseUrl').innerHTML;\r\n\r\n// Review Progress\r\nconst reviewProgressPopup = document.querySelector('#popup__reviewProgress')\r\nconst reviewProgress = document.querySelector('#button__reviewProgress')\r\nconst reviewProgressClose = document.querySelector('#button__reviewProgress_close')\r\n\r\nreviewProgress.addEventListener('click', ev => {\r\n    popupContainer.classList.remove('hidden')\r\n    reviewProgressPopup.classList.remove('hidden')\r\n})\r\nreviewProgressClose.addEventListener('click', ev => {\r\n    popupContainer.classList.add('hidden')\r\n    reviewProgressPopup.classList.add('hidden')\r\n})\r\n\r\n\r\n// Convert question to proper html\r\nconst questionContents = document.querySelectorAll('.questionContent')\r\nconst edjsParser = edjsHTML();\r\nfor (const questionContent of questionContents) {\r\n    questionContent.innerHTML = edjsParser.parse(JSON.parse(questionContent.innerHTML)).join('\\n');\r\n}\r\n\r\nlet changeQuestion = (n) => {\r\n    document.querySelector('#question__' + currentQuestion).classList.add('hidden')\r\n    currentQuestion = Number(n)\r\n    document.querySelector('#question__' + currentQuestion).classList.remove('hidden')\r\n}\r\n\r\nconst next = document.querySelector('#next')\r\nnext.addEventListener('click', ev => {\r\n    changeQuestion(currentQuestion === maxQuestions ? 1 : currentQuestion + 1)\r\n})\r\n\r\nfor (const questionButton of questionButtons) {\r\n    questionButton.addEventListener('click', event => {\r\n        const count = questionButton.getAttribute('count')\r\n        reviewProgressClose.click()\r\n        changeQuestion(count)\r\n    })\r\n}\r\n\r\nconst filterIncorrect = document.querySelector('#filter__incorrect')\r\nconst filterAnswered = document.querySelector('#filter__answered')\r\nconst filterAll = document.querySelector('#filter__all')\r\n\r\nfilterIncorrect.addEventListener('click', ev => {\r\n    document.querySelectorAll('.button__filter').forEach(e => {\r\n        e.classList.remove('bg-gray-500')\r\n        e.classList.remove('text-white')\r\n        e.classList.add('text-gray-500')\r\n    })\r\n\r\n    filterIncorrect.classList.add('bg-gray-500')\r\n    filterIncorrect.classList.remove('text-gray-500')\r\n    filterIncorrect.classList.add('text-white')\r\n\r\n    questionButtons.forEach(e => {\r\n        e.classList.remove('hidden')\r\n        if (!e.hasAttribute('incorrect')) {\r\n            e.classList.add('hidden')\r\n        }\r\n    })\r\n})\r\n\r\nfilterAnswered.addEventListener('click', ev => {\r\n    document.querySelectorAll('.button__filter').forEach(e => {\r\n        e.classList.remove('bg-gray-500')\r\n        e.classList.remove('text-white')\r\n        e.classList.add('text-gray-500')\r\n    })\r\n    filterAnswered.classList.add('bg-gray-500')\r\n    filterAnswered.classList.remove('text-gray-500')\r\n    filterAnswered.classList.add('text-white')\r\n\r\n    questionButtons.forEach(e => {\r\n        e.classList.remove('hidden')\r\n        if (e.hasAttribute('unanswered')) {\r\n            e.classList.add('hidden')\r\n        }\r\n    })\r\n})\r\n\r\nfilterAll.addEventListener('click', ev => {\r\n    document.querySelectorAll('.button__filter').forEach(e => {\r\n        e.classList.remove('bg-gray-500')\r\n        e.classList.remove('text-white')\r\n        e.classList.add('text-gray-500')\r\n    })\r\n    filterAll.classList.add('bg-gray-500')\r\n    filterAll.classList.remove('text-gray-500')\r\n    filterAll.classList.add('text-white')\r\n\r\n    questionButtons.forEach(e => {\r\n        e.classList.remove('hidden')\r\n    })\r\n})\r\n\r\n\r\nrecheckButtons()\r\n\n\n//# sourceURL=webpack://quizera/./js/review.js?");

            /***/
        })

        /******/
    });
    /************************************************************************/
    /******/
    /******/ 	// startup
    /******/ 	// Load entry module and return exports
    /******/ 	// This entry module can't be inlined because the eval devtool is used.
    /******/
    var __webpack_exports__ = {};
    /******/
    __webpack_modules__["./js/review.js"]();
    /******/
    /******/
})()
;