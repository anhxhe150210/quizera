<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.regex.Pattern" %>
<%@ page import="java.util.regex.Matcher" %><%--
  Created by IntelliJ IDEA.
  User: an7
  Date: 5/18/21
  Time: 10:35 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<div class="bg-gray-800 h-16 bottom-0 mt-12 md:relative md:h-screen z-10 w-full md:w-48">
  <div
    class="md:mt-15 md:w-48 sticky md:sticky md:left-0 md:top-0 content-center md:content-start text-left justify-between">
    <ul class="list-reset flex flex-row md:flex-col py-0 md:py-3 px-1 md:px-2 text-center md:text-left">
      <span class="py-1 md:py-3 pl-1 align-middle text-white">Course Categories:</span>
      <c:forEach items="${categories}" var="category">
        <li class="mr-3 flex-1"><a
          class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-green-800 hover:border-pink-500"
          href="${pageContext.request.contextPath}/courses?category=${category.id}"><span
          class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">
            ${category.description}</span>
        </a></li>
      </c:forEach>
    </ul>
  </div>
</div>