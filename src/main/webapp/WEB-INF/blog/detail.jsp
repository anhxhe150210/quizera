<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: xuana
  Date: 5/22/2021
  Time: 6:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>${blog.title}</title>
  <meta name="author" content="Nguyễn Tiến Đạt">
  <meta name="description" content="">

  <!-- Tailwind -->
  <script src="${pageContext.request.contextPath}/styles.js"></script>
  <style>
      @import url('https://fonts.googleapis.com/css?family=Karla:400,700&display=swap');

      .font-family-karla {
          font-family: karla;
      }
  </style>

  <!-- AlpineJS -->
  <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
  <!-- Font Awesome -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"
          integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
</head>
<body class="bg-white font-family-karla">

<!-- Top Bar Nav -->
<c:import url="../topnav.jsp"/>
<%--<header class="flex flex-row justify-between items-center space-x-4 bg-yellow-400 py-6 px-6">--%>
<%--  <a href="#" class="block">--%>
<%--    <span class="sr-only">themes.dev</span>--%>
<%--    <img class="h-8" src="https://cryptologos.cc/logos/electroneum-etn-logo.png" alt="Quizera Logo" title="Quizera">--%>
<%--  </a>--%>
<%--  <nav class="flex flex-row space-x-6 font-semibold">--%>
<%--    <a href="#" class="text-gray-600 hover:underline ">Home</a>--%>
<%--    <a href="#" class="text-gray-600 hover:underline">About us</a>--%>
<%--    <a href="#" class="text-gray-600 hover:underline">Services</a>--%>
<%--    <a href="#" class="text-gray-600 hover:underline active:first:">Blog List</a>--%>
<%--    <a href="#" class="text-gray-600 hover:underline">Contact</a>--%>
<%--  </nav>--%>

<%--</header>--%>

<!-- Text Header -->
<%--<header class="w-full container mx-auto">--%>
<%--  <div class="flex flex-col items-center py-12">--%>
<%--    <a class="font-bold text-gray-800 uppercase hover:text-gray-700 text-5xl" href="#">--%>
<%--      Tên Bài BLog--%>
<%--    </a>--%>
<%--    <p class="text-lg text-gray-600">--%>
<%--      Giới thiệu gì đó--%>
<%--    </p>--%>
<%--  </div>--%>
<%--</header>--%>

<!-- Topic Nav -->
<nav class="w-full py-4 border-t border-b bg-gray-100" x-data="{ open: false }">
  <div class="block sm:hidden">
    <a
      href="#"
      class="block md:hidden text-base font-bold uppercase text-center flex justify-center items-center"
      @click="open = !open"
    >
      Topics <i :class="open ? 'fa-chevron-down': 'fa-chevron-up'" class="fas ml-2"></i>
    </a>
  </div>
  <div :class="open ? 'block': 'hidden'" class="w-full flex-grow sm:flex sm:items-center sm:w-auto">
    <div
      class="w-full container mx-auto flex flex-col sm:flex-row items-center justify-center text-sm font-bold uppercase mt-0 px-6 py-2">
      <a href="#" class="hover:bg-gray-400 rounded py-2 px-4 mx-2">Technology</a>
      <a href="#" class="hover:bg-gray-400 rounded py-2 px-4 mx-2">Automotive</a>
      <a href="#" class="hover:bg-gray-400 rounded py-2 px-4 mx-2">Finance</a>
      <a href="#" class="hover:bg-gray-400 rounded py-2 px-4 mx-2">Politics</a>
      <a href="#" class="hover:bg-gray-400 rounded py-2 px-4 mx-2">Culture</a>
      <a href="#" class="hover:bg-gray-400 rounded py-2 px-4 mx-2">Sports</a>
    </div>
  </div>
</nav>


<div class=" mx-auto flex flex-wrap py-6 bg-yellow-100">

  <!-- Post Section -->
  <section class="w-full md:w-2/3 flex flex-col items-center px-3">

    <article class="flex flex-col shadow my-4">
      <!-- Article Image -->
      <a href="#" class="hover:opacity-75">
        <img src="${blog.thumbnailUrl}">
      </a>
      <div class="bg-white flex flex-col justify-start p-6">
        <a href="#" class="text-yellow-400 text-sm font-bold uppercase pb-4">Technology</a>
        <a href="#" class="text-3xl font-bold hover:text-gray-700 pb-4">${blog.title}</a>
        <p href="#" class="text-sm pb-8">
          By <a href="#" class="font-semibold hover:text-gray-800">${blog.author.getDisplayName()}</a>, Published on
          April 25th, 2020
        </p>
        <p>${blog.content}</p>
      </div>
    </article>

    <div class="w-full flex pt-6">
      <a href="#" class="w-1/2 bg-white shadow hover:shadow-md text-left p-6">
        <p class="text-lg text-yellow-400 font-bold flex items-center"><i class="fas fa-arrow-left pr-1"></i> Previous
        </p>
        <p class="pt-2">Blog Trước</p>
      </a>
      <a href="#" class="w-1/2 bg-white shadow hover:shadow-md text-right p-6">
        <p class="text-lg text-yellow-400 font-bold flex items-center justify-end">Next <i
          class="fas fa-arrow-right pl-1"></i></p>
        <p class="pt-2">Blog Sau</p>
      </a>
    </div>

    <div class="w-full flex flex-col text-center md:text-left md:flex-row shadow bg-white mt-10 mb-10 p-6">
      <div class="w-full md:w-1/5 flex justify-center md:justify-start pb-4">
        <img
          src="${blog.author.getAvatarUrl()}"
          class="rounded-full shadow h-32 w-32">
      </div>
      <div class="flex-1 flex flex-col justify-center md:justify-start">
        <p class="font-semibold text-2xl">${blog.author.getDisplayName()}</p>
        <div class="flex items-center justify-center md:justify-start text-2xl no-underline text-yellow-400 pt-4">
          <a class="" href="#">
            <i class="fab fa-facebook"></i>
          </a>
          <a class="pl-4" href="#">
            <i class="fab fa-instagram"></i>
          </a>
          <a class="pl-4" href="#">
            <i class="fab fa-twitter"></i>
          </a>
          <a class="pl-4" href="#">
            <i class="fab fa-linkedin"></i>
          </a>
        </div>
      </div>
    </div>

  </section>

  <!-- Sidebar Section -->
  <aside class="w-full md:w-1/3 flex flex-col items-center px-3">

    <div class="w-full bg-white shadow flex flex-col my-4 p-6">
      <p class="text-xl font-semibold pb-5">About Us</p>
      <p class="pb-2">day la cho ve about us nhung ma khong biet viet cai gi o day het ca? co nen cho them phan lastest
        blog vao hay khong nhi?.</p>
      <a href="#"
         class="w-full bg-yellow-400 text-white font-bold text-sm uppercase rounded hover:bg-yellow-400 flex items-center justify-center px-2 py-3 mt-4">
        Get to know us
      </a>
    </div>

    <div class="w-full bg-white shadow flex flex-col my-4 p-6">
      <p class="text-xl font-semibold pb-5">Instagram</p>
      <div class="grid grid-cols-3 gap-3">
        <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=1">
        <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=2">
        <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=3">
        <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=4">
        <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=5">
        <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=6">
        <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=7">
        <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=8">
        <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=9">
      </div>
      <a href="#"
         class="w-full bg-yellow-400 text-white font-bold text-sm uppercase rounded hover:bg-yellow-400 flex items-center justify-center px-2 py-3 mt-6">
        <i class="fab fa-instagram mr-2"></i> Follow @datnedatne
      </a>
    </div>

  </aside>

</div>
<c:import url="../footer.jsp"/>
</body>
</html>