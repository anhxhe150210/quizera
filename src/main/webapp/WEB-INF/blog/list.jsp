<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: xuana
  Date: 5/22/2021
  Time: 6:08 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Blogs</title>
  <meta name="author" content="David Grzyb">
  <meta name="description" content="">


  <script src="${pageContext.request.contextPath}/styles.js"></script>
  <style>
      @import url('https://fonts.googleapis.com/css?family=Karla:400,700&display=swap');

      .font-family-karla {
          font-family: karla;
      }
  </style>


  <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"
          integrity="sha256-KzZiKy0DWYsnwMF+X1DvQngQ2/FxF7MF3Ff72XcpuPs=" crossorigin="anonymous"></script>
</head>

<body class="bg-yellow-50 font-family-karla">

<!-- Top Bar Nav -->
<%--<header class="flex flex-row justify-between items-center space-x-4 bg-yellow-400 py-6 px-6">--%>
<%--  <a href="#" class="block">--%>
<%--    <span class="sr-only">themes.dev</span>--%>
<%--    <img class="h-8" src="https://cryptologos.cc/logos/electroneum-etn-logo.png" alt="Quizera Logo" title="Quizera">--%>
<%--  </a>--%>
<%--  <nav class="flex flex-row space-x-6 font-semibold">--%>
<%--    <a href="#" class="text-gray-600 hover:underline ">Home</a>--%>
<%--    <a href="#" class="text-gray-600 hover:underline">About us</a>--%>
<%--    <a href="#" class="text-gray-600 hover:underline">Services</a>--%>
<%--    <a href="#" class="text-gray-600 hover:underline active:first:">Blog List</a>--%>
<%--    <a href="#" class="text-gray-600 hover:underline">Contact</a>--%>
<%--  </nav>--%>

<%--</header>--%>
<c:import url="../topnav.jsp"/>

<!-- Text Header -->
<%--<header class="w-full mx-auto">--%>
<%--  <div class="w-full bg-cover bg-center"--%>
<%--       style="height:32rem; background-image: url(http://blog.priceedge.eu/wp-content/uploads/2018/07/agenda-analysis-business-990818.jpg);">--%>
<%--    <div class="flex items-center justify-center h-full w-full bg-gray-900 bg-opacity-50">--%>
<%--      <div class="text-center">--%>
<%--        <h1 class="text-white text-2xl font-semibold uppercase md:text-3xl">Quizera <span--%>
<%--          class=" text-yellow-400">BLOG</span></h1>--%>
<%--        <button--%>
<%--          class="mt-4 px-4 py-2 bg-yellow-400 text-white text-sm uppercase font-medium rounded hover:bg-yellow-500 focus:outline-none focus:bg-blue-500">--%>
<%--          Start project--%>
<%--        </button>--%>
<%--      </div>--%>
<%--    </div>--%>
<%--  </div>--%>
<%--</header>--%>

<!-- Topic Nav -->
<%--<nav class="mt-14 w-full py-4 border-t border-b bg-gray-100" x-data="{ open: false }">--%>
<%--  <div class="block sm:hidden">--%>
<%--    <a--%>
<%--      href="#"--%>
<%--      class="block md:hidden text-base font-bold uppercase text-center flex justify-center items-center"--%>
<%--      @click="open = !open"--%>
<%--    >--%>
<%--      Topics <i :class="open ? 'fa-chevron-down': 'fa-chevron-up'" class="fas ml-2"></i>--%>
<%--    </a>--%>
<%--  </div>--%>
<%--  <div :class="open ? 'block': 'hidden'" class="w-full flex-grow sm:flex sm:items-center sm:w-auto">--%>
<%--    <div--%>
<%--      class="w-full container mx-auto flex flex-col sm:flex-row items-center justify-center text-sm font-bold uppercase mt-0 px-6 py-2">--%>
<%--      <a href="#" class="hover:bg-gray-400 rounded py-2 px-4 mx-2">Technology</a>--%>
<%--      <a href="#" class="hover:bg-gray-400 rounded py-2 px-4 mx-2">Automotive</a>--%>
<%--      <a href="#" class="hover:bg-gray-400 rounded py-2 px-4 mx-2">Finance</a>--%>
<%--      <a href="#" class="hover:bg-gray-400 rounded py-2 px-4 mx-2">Politics</a>--%>
<%--      <a href="#" class="hover:bg-gray-400 rounded py-2 px-4 mx-2">Culture</a>--%>
<%--      <a href="#" class="hover:bg-gray-400 rounded py-2 px-4 mx-2">Sports</a>--%>
<%--    </div>--%>
<%--  </div>--%>
<%--</nav>--%>


<div class="mx-auto flex flex-wrap py-6 bg-yellow-100 mt-14">

  <!-- Posts Section -->
  <section class="w-full md:w-2/3 flex flex-col items-center px-3">

    <c:forEach items="${blogs}" var="blog">
      <article class="flex flex-col shadow my-4">
        <!-- Article Image -->
        <a href="#" class="hover:opacity-75">
          <img src="${blog.thumbnailUrl}">
        </a>
        <div class="bg-white flex flex-col justify-start p-6">
          <div class="flex flex-row gap-4">
            <c:forEach items="${blog.categories}" var="category">
              <a href="${pageContext.request.contextPath}/blogs?category=${category.id}"
                 class="flex justify-center items-center text-yellow-400 text-sm font-bold uppercase py-1 px-2 bg-gray-800 rounded-md">${category.description}</a>
            </c:forEach>
          </div>
          <p class="text-3xl font-bold hover:text-gray-700 pb-4 mt-2">${blog.title}</p>
          <p href="#" class="text-sm pb-3">
            By <a href="#" class="font-semibold hover:text-gray-800">${blog.author.getDisplayName()}</a>, Published on
            <fmt:formatDate value="${blog.publicDate}" pattern="MM.dd.yyyy"/>
          </p>
          <p class="pb-6">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis porta dui. Ut
            eu
            iaculis massa. Sed ornare ligula lacus, quis iaculis dui porta volutpat. In sit amet posuere magna..</p>
          <a href="${pageContext.request.contextPath}/blogs?id=${blog.id}"
             class="uppercase text-gray-800 hover:text-black">Continue Reading <i class="fas fa-arrow-right"></i></a>
        </div>
      </article>
    </c:forEach>


    <%--    <article class="flex flex-col shadow my-4">--%>
    <%--      <!-- Article Image -->--%>
    <%--      <a href="#" class="hover:opacity-75">--%>
    <%--        <img src="https://source.unsplash.com/collection/1346951/1000x500?sig=2">--%>
    <%--      </a>--%>
    <%--      <div class="bg-white flex flex-col justify-start p-6">--%>
    <%--        <a href="#" class="text-yellow-400 text-sm font-bold uppercase pb-4">Automotive, Finance</a>--%>
    <%--        <a href="#" class="text-3xl font-bold hover:text-gray-700 pb-4">BLOG VE Automotive</a>--%>
    <%--        <p href="#" class="text-sm pb-3">--%>
    <%--          By <a href="#" class="font-semibold hover:text-gray-800">NGUYEN TIEN DAT</a>, Published on January 12th, 2020--%>
    <%--        </p>--%>
    <%--        <a href="#" class="pb-6">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis porta dui. Ut eu--%>
    <%--          iaculis massa. Sed ornare ligula lacus, quis iaculis dui porta volutpat. In sit amet posuere magna..</a>--%>
    <%--        <a href="#" class="uppercase text-gray-800 hover:text-black">Continue Reading <i class="fas fa-arrow-right"></i></a>--%>
    <%--      </div>--%>
    <%--    </article>--%>

    <%--    <article class="flex flex-col shadow my-4">--%>
    <%--      <!-- Article Image -->--%>
    <%--      <a href="#" class="hover:opacity-75">--%>
    <%--        <img src="https://source.unsplash.com/collection/1346951/1000x500?sig=3">--%>
    <%--      </a>--%>
    <%--      <div class="bg-white flex flex-col justify-start p-6">--%>
    <%--        <a href="#" class="text-yellow-400 text-sm font-bold uppercase pb-4">Sports</a>--%>
    <%--        <a href="#" class="text-3xl font-bold hover:text-gray-700 pb-4">BLOG VE SPORTS</a>--%>
    <%--        <p href="#" class="text-sm pb-3">--%>
    <%--          By <a href="#" class="font-semibold hover:text-gray-800">NGUYEN TIEN DAT</a>, Published on October 22nd, 2019--%>
    <%--        </p>--%>
    <%--        <a href="#" class="pb-6">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis porta dui. Ut eu--%>
    <%--          iaculis massa. Sed ornare ligula lacus, quis iaculis dui porta volutpat. In sit amet posuere magna..</a>--%>
    <%--        <a href="#" class="uppercase text-gray-800 hover:text-black">Continue Reading <i class="fas fa-arrow-right"></i></a>--%>
    <%--      </div>--%>
    <%--    </article>--%>

    <!-- Pagination -->
    <%--    <div class="flex items-center py-8">--%>
    <%--      <a href="#"--%>
    <%--         class="h-10 w-10 bg-yellow-400 hover:bg-yellow-400 font-semibold text-white text-sm flex items-center justify-center">1</a>--%>
    <%--      <a href="#"--%>
    <%--         class="h-10 w-10 font-semibold text-gray-800 hover:bg-yellow-400 hover:text-white text-sm flex items-center justify-center">2</a>--%>
    <%--      <a href="#"--%>
    <%--         class="h-10 w-10 font-semibold text-gray-800 hover:text-gray-900 text-sm flex items-center justify-center ml-3">Next--%>
    <%--        <i class="fas fa-arrow-right ml-2"></i></a>--%>
    <%--    </div>--%>

  </section>

  <!-- Sidebar Section -->
  <aside class="w-full md:w-1/3 flex flex-col items-center px-3">

    <div class="w-full bg-white shadow flex flex-col my-4 p-6">
      <p class="text-xl font-semibold pb-5">About Us</p>
      <p class="pb-2">We are Quizera.</p>
      <a href="#"
         class="w-full bg-yellow-400 text-white font-bold text-sm uppercase rounded hover:bg-yellow-400 flex items-center justify-center px-2 py-3 mt-4">
        Get to know us
      </a>
    </div>

    <div class="container my-4 p-6">
      <div class="flex flex-col -m-4">
        <%--        <div class="p-4 md">--%>
        <%--          <div class="h-full border-2 border-gray-200 border-opacity-60 rounded-lg overflow-hidden bg-white">--%>
        <%--            <img class="lg:h-48 md:h-36 w-full object-cover object-center"--%>
        <%--                 src="https://source.unsplash.com/collection/1346951/150x150?sig=1" alt="blog">--%>
        <%--            <div class="p-6">--%>
        <%--              <h2 class="tracking-widest text-xs title-font font-medium text-gray-400 mb-1">Lastest Product</h2>--%>
        <%--              <h1 class="title-font text-lg font-medium text-gray-900 mb-3">The Catalyzer</h1>--%>
        <%--              <p class="leading-relaxed mb-3">o day la thong tin ve lastest product (product moi/).</p>--%>
        <%--              <div class="flex items-center flex-wrap ">--%>
        <%--                <a class="text-indigo-500 inline-flex items-center md:mb-2 lg:mb-0">Learn More--%>
        <%--                  <svg class="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" fill="none"--%>
        <%--                       stroke-linecap="round" stroke-linejoin="round">--%>
        <%--                    <path d="M5 12h14"></path>--%>
        <%--                    <path d="M12 5l7 7-7 7"></path>--%>
        <%--                  </svg>--%>
        <%--                </a>--%>
        <%--                <span--%>
        <%--                  class="text-gray-400 mr-3 inline-flex items-center lg:ml-auto md:ml-0 ml-auto leading-none text-sm pr-3 py-1 border-r-2 border-gray-200">--%>
        <%--                <svg class="w-4 h-4 mr-1" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round"--%>
        <%--                     stroke-linejoin="round" viewBox="0 0 24 24">--%>
        <%--                  <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>--%>
        <%--                  <circle cx="12" cy="12" r="3"></circle>--%>
        <%--                </svg>1.2K--%>
        <%--              </span>--%>
        <%--                <span class="text-gray-400 inline-flex items-center leading-none text-sm">--%>
        <%--                <svg class="w-4 h-4 mr-1" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round"--%>
        <%--                     stroke-linejoin="round" viewBox="0 0 24 24">--%>
        <%--                  <path--%>
        <%--                    d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z"></path>--%>
        <%--                </svg>6--%>
        <%--              </span>--%>
        <%--              </div>--%>
        <%--            </div>--%>
        <%--          </div>--%>
        <%--        </div>--%>

        <div class="w-full bg-white shadow flex flex-col my-4 p-6">
          <p class="text-xl font-semibold pb-5">Instagram</p>
          <div class="grid grid-cols-3 gap-3">
            <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=1">
            <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=2">
            <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=3">
            <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=4">
            <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=5">
            <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=6">
            <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=7">
            <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=8">
            <img class="hover:opacity-75" src="https://source.unsplash.com/collection/1346951/150x150?sig=9">
          </div>
          <a href="#"
             class="w-full bg-yellow-400 text-white font-bold text-sm uppercase rounded hover:bg-yellow-400 flex items-center justify-center px-2 py-3 mt-6">
            <i class="fab fa-instagram mr-2"></i> Follow @datnedatne
          </a>
        </div>
      </div>
    </div>

  </aside>

</div>

<%--<footer class="w-full border-t bg-white pb-12">
  <div
    class="relative w-full flex items-center invisible md:visible md:pb-12"
    x-data="getCarouselData()"
  >
    <button
      class="absolute bg-yellow-400 hover:bg-yellow-400 text-white text-2xl font-bold hover:shadow rounded-full w-16 h-16 ml-12"
      x-on:click="decrement()">
      &#8592;
    </button>
    <template x-for="image in images.slice(currentIndex, currentIndex + 6)" :key="images.indexOf(image)">
      <img class="w-1/6 hover:opacity-75" :src="image">
    </template>
    <button
      class="absolute right-0 bg-yellow-400 hover:bg-yellow-400 text-white text-2xl font-bold hover:shadow rounded-full w-16 h-16 mr-12"
      x-on:click="increment()">
      &#8594;
    </button>
  </div>
  <div class="w-full container mx-auto flex flex-col items-center">
    <div class="flex flex-col md:flex-row text-center md:text-left md:justify-between py-6">
      <a href="#" class="uppercase px-3">About Us</a>
      <a href="#" class="uppercase px-3">Privacy Policy</a>
      <a href="#" class="uppercase px-3">Terms & Conditions</a>
      <a href="#" class="uppercase px-3">Contact Us</a>
    </div>
    <div class="uppercase pb-6">&copy; Quizera</div>
  </div>
</footer>

<script>
    function getCarouselData() {
        return {
            currentIndex: 0,
            images: [
                'https://source.unsplash.com/collection/1346951/800x800?sig=1',
                'https://source.unsplash.com/collection/1346951/800x800?sig=2',
                'https://source.unsplash.com/collection/1346951/800x800?sig=3',
                'https://source.unsplash.com/collection/1346951/800x800?sig=4',
                'https://source.unsplash.com/collection/1346951/800x800?sig=5',
                'https://source.unsplash.com/collection/1346951/800x800?sig=6',
                'https://source.unsplash.com/collection/1346951/800x800?sig=7',
                'https://source.unsplash.com/collection/1346951/800x800?sig=8',
                'https://source.unsplash.com/collection/1346951/800x800?sig=9',
            ],
            increment() {
                this.currentIndex = this.currentIndex === this.images.length - 6 ? 0 : this.currentIndex + 1;
            },
            decrement() {
                this.currentIndex = this.currentIndex === this.images.length - 6 ? 0 : this.currentIndex - 1;
            },
        }
    }
</script>--%>
<c:import url="../footer.jsp"/>
</body>
</html>