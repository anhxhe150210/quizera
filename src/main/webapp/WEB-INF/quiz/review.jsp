<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: xuana
  Date: 5/31/2021
  Time: 9:51 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Review</title>
  <script src="${pageContext.request.contextPath}/styles.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/editorjs-html@3.0.3/build/edjsHTML.browser.js"></script>
</head>
<body>
<div id="popupContainer" class="fixed h-screen w-screen bg-gray-600 bg-opacity-80 hidden">
  <div id="popup__reviewProgress"
       class="fixed w-1/2 h-1/2 top-1/4 left-1/4 bg-white border-2 border-black flex flex-col p-4 hidden">
    <div class="flex flex-row justify-between items-center p-2">
      <span class="font-medium">Review Progress</span>
      <div id="button__reviewProgress_close"
           class="cursor-pointer flex items-center justify-center text-gray-600 border-2 border-gray-600 w-8 h-8 rounded-full">
        <span>X</span>
      </div>
    </div>

    <div class="flex flex-row justify-start">
      <div class="flex flex-row">
        <div id="filter__answered"
             class="button__filter cursor-pointer py-2 px-4 uppercase text-gray-500 border-2 border-gray-500">
          Answered
        </div>
        <div id="filter__incorrect"
             class="button__filter cursor-pointer py-2 px-4 uppercase text-gray-500 border-2 border-gray-500">
          Incorrect
        </div>
        <div id="filter__all"
             class="button__filter cursor-pointer py-2 px-4 uppercase border-2 border-gray-500 bg-gray-500 text-white">
          All Questions
        </div>
      </div>
    </div>

    <%--  Question choosing  --%>
    <div id="pane__questions" class="bg-gray-200 h-full w-full flex flex-row flex-wrap content-start gap-2">
      <c:set var="count" scope="page" value="1"/>
      <c:forEach items="${practice.submissions}" var="submission">
        <c:set var="questionCorrect" scope="page" value="${true}"/>
        <c:forEach items="${submission.question.answers}" var="answer">
          <c:set var="checked" scope="page"
                 value="${submission.answers.stream().anyMatch(a -> a.getId() == answer.id)}"/>
          <c:set var="correct" scope="page"
                 value="${answers.stream().anyMatch(a -> a.getId() == answer.id && a.correct)}"/>
          <c:if test="${(checked && !correct) || (!checked && correct)}">
            <c:set var="questionCorrect" scope="page" value="${false}"/>
          </c:if>
        </c:forEach>

        <div count="${count}"
             questionid="${submission.question.id}" ${questionCorrect ? '' : 'incorrect'} ${submission.answers.isEmpty() ? 'unanswered' : ''}
             class="cursor-pointer flex items-center justify-center text-white border-2 border-gray-600 w-8 h-8 rounded-md ${questionCorrect ? 'bg-green-500' : 'bg-red-500'}">
          <span>${count}</span>
        </div>
        <c:set var="count" scope="page" value="${count + 1}"/>
      </c:forEach>
    </div>
  </div>

</div>
<c:set var="count" scope="page" value="1"/>
<span id="baseUrl" class="hidden">${pageContext.request.contextPath}</span>
<span id="practiceId" class="hidden">${practice.id}</span>
<div class="flex flex-col h-screen">
  <c:forEach items="${practice.submissions}" var="submission">
    <div id="question__${count}" class="${count == 1 ? '' : 'hidden'} h-full flex flex-col question">
      <div class="flex flex-row h-20 bg-gray-600 justify-between p-4 gap-4">
        <a href="${pageContext.request.contextPath}/practices">
          Back
        </a>
        <div class="h-full flex items-center text-2xl">
          <span>${count} / ${practice.submissions.size()}</span>
        </div>
      </div>

      <div class="h-full flex flex-col p-4">
        <div class="bg-gray-800 flex justify-between text-white py-2 px-4 rounded-md">
          <span>${count})</span>
          <span>Question ID: ${submission.question.id}</span>
        </div>
        <div class="h-full m-2 questionPane">
          <div class="questionContent">${submission.question.question}</div>
          <form class="flex flex-col mt-6">
            <c:forEach items="${submission.question.answers}" var="answer">
              <c:set var="checked" scope="page"
                     value="${submission.answers.stream().anyMatch(a -> a.getId() == answer.id)}"/>
              <c:set var="correct" scope="page"
                     value="${answers.stream().anyMatch(a -> a.getId() == answer.id && a.correct)}"/>
              <label>
                <input type="checkbox"
                       class="${correct ? (checked ? 'text-green-500' : 'text-yellow-500') : (checked ? 'text-red-500' : 'bg-gray-500')}" ${(checked || correct) ? 'checked' : ''}
                       disabled>
                  ${answer.answer}
              </label>
            </c:forEach>
          </form>
          <form id="form__question__${submission.question.id}" class="flex flex-col mt-6">
          </form>
        </div>
      </div>
    </div>
    <c:set var="count" scope="page" value="${count + 1}"/>
  </c:forEach>

  <div class="h-20 bg-green-400 flex flex-row justify-between items-center px-4">
    <div id="button__reviewProgress"
         class="cursor-pointer py-1 px-2 border-2 border-white rounded-md text-white bg-green-400 font-medium w-40 text-center">
      Review
      Progress
    </div>
    <div id="next"
         class="cursor-pointer py-1 px-2 border-2 border-white rounded-md text-white bg-green-400 font-medium w-40 text-center select-none">
      Next
    </div>
  </div>
</div>
<script src="${pageContext.request.contextPath}/review.js"></script>
</body>
</html>
