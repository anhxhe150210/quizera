<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: xuana
  Date: 5/31/2021
  Time: 9:51 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
  <script src="${pageContext.request.contextPath}/styles.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/editorjs-html@3.0.3/build/edjsHTML.browser.js"></script>
</head>
<body>
<div id="popupContainer" class="fixed h-screen w-screen bg-gray-600 bg-opacity-80 hidden">
  <div id="popup__peekAtAnswer"
       class="fixed w-1/2 h-1/2 top-1/4 left-1/4 bg-white border-2 border-black flex flex-col p-4 hidden">
    <div class="flex flex-row justify-between items-center p-2">
      <span class="font-medium">Peek at Answer</span>
      <div id="button__peekAtAnswer_close"
           class="cursor-pointer flex items-center justify-center text-gray-600 border-2 border-gray-600 w-8 h-8 rounded-full">
        <span>X</span>
      </div>
    </div>
    <div class="bg-gray-200 h-full w-full overflow-y-scroll">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum iure quasi ratione vero. Accusantium assumenda
      doloremque et eum eveniet, ipsam itaque laudantium modi nisi nulla placeat possimus qui, ratione totam?
    </div>
  </div>

  <div id="popup__reviewProgress"
       class="fixed w-1/2 h-1/2 top-1/4 left-1/4 bg-white border-2 border-black flex flex-col p-4 hidden">
    <div class="flex flex-row justify-between items-center p-2">
      <span class="font-medium">Review Progress</span>
      <div id="button__reviewProgress_close"
           class="cursor-pointer flex items-center justify-center text-gray-600 border-2 border-gray-600 w-8 h-8 rounded-full">
        <span>X</span>
      </div>
    </div>

    <div class="flex flex-row justify-between">
      <div class="flex flex-row">
        <div id="filter__unanswered"
             class="button__filter cursor-pointer py-2 px-4 uppercase text-gray-500 border-2 border-gray-500">
          Unanswered
        </div>
        <div id="filter__marked"
             class="button__filter cursor-pointer py-2 px-4 uppercase text-gray-500 border-2 border-gray-500">
          Marked
        </div>
        <div id="filter__answered"
             class="button__filter cursor-pointer py-2 px-4 uppercase text-gray-500 border-2 border-gray-500">
          Answered
        </div>
        <div id="filter__all"
             class="button__filter cursor-pointer py-2 px-4 uppercase border-2 border-gray-500 bg-gray-500 text-white">
          All Questions
        </div>
      </div>
      <a href="${pageContext.request.contextPath}/practices/submit?id=${practice.id}"
         class="py-2 px-4 uppercase text-gray-500 border-2 border-gray-500">
        Score exam now
      </a>
    </div>

    <%--  Question choosing  --%>
    <div id="pane__questions" class="bg-gray-200 h-full w-full flex flex-row flex-wrap content-start gap-2">
      <c:set var="count" scope="page" value="1"/>
      <c:forEach items="${practice.submissions}" var="submission">
        <div count="${count}" questionid="${submission.question.id}"
             class="cursor-pointer flex items-center justify-center text-gray-600 border-2 border-gray-600 w-8 h-8 rounded-md">
          <span>${count}</span>
        </div>
        <c:set var="count" scope="page" value="${count + 1}"/>
      </c:forEach>
    </div>
  </div>

</div>
<c:set var="count" scope="page" value="1"/>
<span id="baseUrl" class="hidden">${pageContext.request.contextPath}</span>
<span id="practiceId" class="hidden">${practice.id}</span>
<div class="flex flex-col h-screen">
  <c:forEach items="${practice.submissions}" var="submission">
    <div id="question__${count}" class="${count == 1 ? '' : 'hidden'} h-full flex flex-col question">
      <div class="flex flex-row h-20 bg-gray-600 justify-end p-4 gap-4">
        <div class="h-full flex items-center text-2xl">
          <span>${count} / ${practice.submissions.size()}</span>
        </div>
        <div class="h-full flex items-center text-2xl bg-indigo-400 px-2">
          <span
            class="countdown">${practice.startAt.toInstant().getEpochSecond() + practice.quiz.durationInSeconds}</span>
        </div>
      </div>

      <div class="h-full flex flex-col p-4">
        <div class="bg-gray-800 flex justify-between text-white py-2 px-4 rounded-md">
          <span>${count})</span>
          <span>Question ID: ${submission.question.id}</span>
        </div>
        <div class="h-full m-2 bg-green-400 questionPane">
          <div class="questionContent">${submission.question.question}</div>
          <form id="form__question__${submission.question.id}" class="flex flex-col mt-6">
            <c:forEach items="${submission.question.answers}" var="answer">
              <label>
                <input id="answer__${submission.question.id}__${answer.id}" type="checkbox" answerid="${answer.id}"
                       questionid="${answer.question.id}" ${submission.answers.stream().anyMatch(a -> a.getId() == answer.id) ? 'checked' : ''}>
                  ${answer.answer}
              </label>
            </c:forEach>
          </form>
        </div>
        <div class="flex justify-end rounded-md gap-2">
          <span id="button__peekAtAnswer"
                class="cursor-pointer py-1 px-2 border-2 border-gray-300 rounded-md text-gray-600 bg-white font-medium">Peek at
        Answer</span>
          <span id="button__markForReview"
                class="cursor-pointer py-1 px-2 border-2 border-gray-300 rounded-md text-white bg-green-400 font-medium">Mark for
        Review</span>
        </div>
      </div>
    </div>
    <c:set var="count" scope="page" value="${count + 1}"/>
  </c:forEach>

  <div class="h-20 bg-green-400 flex flex-row justify-between items-center px-4">
    <div id="button__reviewProgress"
         class="cursor-pointer py-1 px-2 border-2 border-white rounded-md text-white bg-green-400 font-medium w-40 text-center">
      Review
      Progress
    </div>
    <div id="next"
         class="cursor-pointer py-1 px-2 border-2 border-white rounded-md text-white bg-green-400 font-medium w-40 text-center select-none">
      Next
    </div>
  </div>
</div>
<script src="${pageContext.request.contextPath}/quiz.js"></script>
</body>
</html>
