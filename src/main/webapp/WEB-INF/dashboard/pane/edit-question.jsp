<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Edit Question</h3>
  </div>
</div>

<div class="flex flex-col gap-4">
  <a
    href="${pageContext.request.contextPath}/dashboard/questions?course=${question.course.id}"><span
    class="inline-block px-4 py-2 bg-red-500 rounded border text-white m-1">Back</span></a>

  <div id="editorjs" class="bg-white"></div>

  <form class="flex flex-col" action="${pageContext.request.contextPath}/dashboard/questions/update" method="POST">
    <input type="hidden" name="questionId" value="${question.getId()}">
    <input id="question" name="question" type="hidden" value="${questionEscaped}">

    <table>
      <thead>
      <tr>
        <th>Answer</th>
        <th>Is Correct?</th>
        <th>Explanation</th>
        <th>Actions</th>
      </tr>
      </thead>

      <tbody>
      <c:forEach items="${question.getAnswers()}" var="answer">
        <tr class="h-10">
          <td><input class="w-full rounded" type="text" name="answer_${answer.getId()}" value="${answer.getAnswer()}">
          </td>
          <td>
            <div class="flex justify-center items-center">
              <input type="checkbox" name="answer_correct_${answer.getId()}" ${answer.isCorrect() ? "checked" : ""}>
            </div>
          </td>
          <td>
            <input class="w-full rounded" type="text" name="answer_explanation_${answer.getId()}"
                   value="${answer.getExplanation()}">
          </td>
          <td>
            <div class="flex justify-center items-center">
              <a class="bg-red-500 px-4 py-2"
                 href="${pageContext.request.contextPath}/dashboard/answers/delete?id=${answer.getId()}">Delete</a>
            </div>
          </td>
        </tr>
      </c:forEach>
      </tbody>
    </table>
    <ul>
      <li><a
        href="${pageContext.request.contextPath}/dashboard/answers/add?question=${question.getId()}">+
        Add answer</a></li>
    </ul>

    <label for="dimension" class="mt-4">
      Dimension
      <select name="dimension" id="dimension">
        <option value="">-- Select --</option>
        <c:forEach items="${dimensions}" var="d">
          <option value="${d.id}" ${d.id == question.dimension.id ? "selected" : ""}>${d}</option>
        </c:forEach>
      </select>
    </label>

    <button type="submit" class="mt-4 p-4 bg-blue-500 text-white text-xl">Submit</button>
  </form>
</div>

<script src="${pageContext.request.contextPath}/questionEditor.js"></script>