<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Edit quiz</h3>
  </div>
</div>

<div class="flex flex-row gap-6">
  <a class="inline-block px-4 py-2 bg-red-500 rounded border text-white m-1" href="${pageContext.request.contextPath}/dashboard/quizzes?id=${quiz.course.id}">Back</a>
</div>

<form id="form" class="flex flex-col gap-4 mt-6 w-1/2"
      action="${pageContext.request.contextPath}/dashboard/quizzes/update"
      method="POST">
  <input type="hidden" name="id" value="${quiz.id}">

  <label for="title" class="mx-2">
    Title:
    <input type="text" id="title" class="w-full m-2 rounded" name="title" value="${quiz.title}">
  </label>

  <label for="levelId" class="mx-2">
    Level Id:
    <input type="number" id="levelId" class="w-full m-2 rounded" name="levelId" value="${quiz.level.id}">
  </label>

  <label for="durationInSeconds" class="mx-2">
    Duration in seconds:
    <input type="number" id="durationInSeconds" class="w-full m-2 rounded" name="durationInSeconds"
           value="${quiz.durationInSeconds}">
  </label>

  <div id="filterBy__inputs" class="flex flex-row gap-4 ml-2">
    Choose question filter:
    <label>
      <input type="radio" name="filterBy" value="1" checked> Dimension
      <input type="radio" name="filterBy" value="2"> Type
    </label>
  </div>

  <label for="numberOfQuestions" class="mx-2">
    Number of questions:
    <input type="number" id="numberOfQuestions" class="w-full m-2 rounded" name="numberOfQuestions"
           value="${quiz.numberOfQuestions}">
  </label>

  <span class="mx-2">Picking questions from:</span>
  <div id="filters" class="flex flex-col gap-4 ml-2">
  </div>
  <span class="ml-2 self-start cursor-pointer bg-green-500 px-4 py-2 rounded text-white" onclick="addFilter()">Add</span>

  <input type="submit" class="p-4 bg-green-500 cursor-pointer m-2 rounded text-white" value="Submit">
</form>

<script>
    const dimensions = "${dimensions}".split(",");
    const types = "${types}".split(",");

    let filterType = ${quiz.questionFilters.size() != 0 ? quiz.questionFilters[0].filterBy.filterNumber : 1};
    let filterCount = 0;

    const initializeFilter = (select, target = null) => {
        const values = filterType === 1 ? dimensions : types;
        select.replaceChildren();
        const defaultOption = document.createElement("option");
        defaultOption.value = "";
        defaultOption.innerHTML = "-- Select --";
        select.appendChild(defaultOption);
        for (let value of values) {
            const option = document.createElement("option");
            option.value = value;
            option.innerHTML = value;
            if (target !== null && value === target) {
                option.selected = true;
            }
            select.appendChild(option);
        }
    }


    const deleteFilter = (filterNumber) => {
        filterNumber = Number(filterNumber);
        if (filterCount <= 1) return;

        for (let i = filterNumber + 1; i <= filterCount; i++) {
            const selectedValue = document.querySelector('#filter_' + i).value;
            console.log(i + ", selected: " + selectedValue);
            document.querySelectorAll('#filter_' + (i - 1) + ' option').forEach(e => {
                e.selected = e.value === selectedValue;
            });

            document.getElementById('filter_' + (i - 1) + '_number').value = document.getElementById('filter_' + i + '_number').value;
        }
        document.getElementById('group_filter_' + filterCount).remove();
        filterCount--;
    }

    const addFilter = (target = null, quesNum = null) => {
        const newFilterCount = ++filterCount;
        const group = document.createElement("div");
        group.id = "group_filter_" + newFilterCount;
        group.classList.add("flex", "flex-row");

        const select = document.createElement("select");
        select.id = "filter_" + newFilterCount;
        select.name = "filter_" + newFilterCount;
        select.classList.add("rounded-l");
        group.appendChild(select);

        const input = document.createElement("input");
        input.id = "filter_" + newFilterCount + "_number";
        input.type = "number";
        input.name = "filter_" + newFilterCount + "_number";
        input.classList.add("rounded-r", "mr-1");
        input.placeholder = "# of q.";
        if (quesNum !== null) {
            input.value = quesNum;
        }
        group.appendChild(input);

        const span = document.createElement("span");
        span.addEventListener("click", () => deleteFilter(newFilterCount));
        span.classList.add("cursor-pointer", "p-4", "bg-red-500", "px-4", "py-2", "rounded", "text-white");
        span.innerHTML = "Delete";
        group.appendChild(span);

        document.getElementById("filters").appendChild(group);

        initializeFilter(select, target);
    }

    document.querySelectorAll("#filterBy__inputs input").forEach(e => {
        e.addEventListener("change", ev => {
            const element = ev.target;
            if (element.checked) {
                filterType = Number(element.value);
                for (const select of document.querySelectorAll("#filters select")) {
                    initializeFilter(select);
                }
            }
        })
    });

    const init = () => {

        <c:set var="n" scope="request" value="1"/>
        <c:forEach items="${quiz.questionFilters}" var="filter">
        addFilter("${filter.target}", ${filter.numberOfQuestions});
        </c:forEach>
    };

    init();
</script>