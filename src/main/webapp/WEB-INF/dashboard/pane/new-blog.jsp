<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Edit blog</h3>
  </div>
</div>

<a class="inline-block px-4 py-2 bg-red-500 rounded border text-white m-1" href="${pageContext.request.contextPath}/dashboard/blogs">Back</a>

<form id="form" class="flex flex-col" action="${pageContext.request.contextPath}/dashboard/blogs/add"
      method="POST">
  <label for="title" class="ml-2">Title</label>
  <input type="text" id="title" name="title" class="mx-2 rounded" value="${blog.title}">

  <label for="content" class="ml-2">Content</label>
  <textarea form="form" name="content" id="content" class="mx-2 rounded" cols="30" rows="10">${blog.content}</textarea>

  <label for="thumbnailUrl" class="ml-2">Thumbnail URL</label>
  <input type="text" id="thumbnailUrl" name="thumbnailUrl" class="mx-2 rounded" value="${blog.thumbnailUrl}">

  <button type="submit" class="p-4 bg-blue-500 text-white text-xl m-2 rounded">Submit</button>
</form>
