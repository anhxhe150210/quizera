<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Edit course categories</h3>
  </div>
</div>

<a class="inline-block px-4 py-2 bg-red-500 rounded border text-white m-1" href="${pageContext.request.contextPath}/dashboard/courses/update?id=${course.id}">Back</a>

<form id="form" class="flex flex-col" action="${pageContext.request.contextPath}/dashboard/courses/editCategories"
      method="POST">
  <input type="hidden" name="id" value="${course.getId()}">

  <c:forEach items="${categories}" var="category">
    <label class="ml-2">
        ${category.getDescription()}
      <input type="checkbox" name="categories"
             value="${category.getId()}" ${courseCategories.contains(category.getId()) ? "checked" : ""}>
    </label>
  </c:forEach>

  <button type="submit" class="p-4 bg-blue-500 text-white text-xl mx-2 rounded">Submit</button>
</form>
