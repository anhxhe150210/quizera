<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Chapters</h3>
  </div>
</div>

<a href="${pageContext.request.contextPath}/dashboard/courses/update?id=${course.id}">Back</a>

<table class="w-full">
  <thead>
  <tr class="h-20 bg-gray-500 text-left text-xl">
    <th class="p-6">Index</th>
    <th class="p-6">Title</th>
    <th class="p-6">Number of lectures</th>
    <th class="p-6">Actions</th>
  </tr>
  </thead>
  <tbody>
  <c:forEach items="${chapters}" var="chapter">
    <tr class="h-20 text-xl border-b border-gray-500">
      <td class="p-6">${chapter.getIndex()}</td>
      <td class="p-6">${chapter.getTitle()}</td>
      <td class="p-6">${chapter.getLectures().size()}</td>
      <td class="p-6">
        <a href="${pageContext.request.contextPath}/dashboard/chapters/update?id=${chapter.getId()}">Edit</a>
        <a href="${pageContext.request.contextPath}/dashboard/chapters/delete?id=${chapter.getId()}">Delete</a>
      </td>
    </tr>
  </c:forEach>
  </tbody>
</table>
<a href="${pageContext.request.contextPath}/dashboard/chapters/add?courseId=${course.getId()}">+ Add a chapter</a>
