<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: an7
  Date: 5/19/21
  Time: 12:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Lectures</h3>
  </div>
</div>

<a href="${pageContext.request.contextPath}/dashboard/chapters/update?id=${chapter.id}">Back</a>

<div class="m-2">
  <table class="w-full">
    <thead>
    <tr class="h-20 bg-gray-500 text-left text-xl">
      <th class="p-6">Id</th>
      <th class="p-6">Title</th>
      <th class="p-6">Actions</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${lectures}" var="lecture">
      <tr class="h-20 text-xl border-b border-gray-500">
        <td class="p-6">${lecture.id}</td>
        <td class="p-6">${lecture.title}</td>
        <td class="p-6">
          <div class="flex flex-row gap-2">
            <a href="${pageContext.request.contextPath}/dashboard/lectures/update?id=${lecture.id}">
              Edit
            </a>
            <a href="${pageContext.request.contextPath}/dashboard/lectures/delete?id=${lecture.id}">
              Delete
            </a>
          </div>
        </td>
      </tr>
    </c:forEach>
    </tbody>
  </table>
  <a href="${pageContext.request.contextPath}/dashboard/lectures/add?id=${chapter.id}">+ Add a lecture</a>
</div>
