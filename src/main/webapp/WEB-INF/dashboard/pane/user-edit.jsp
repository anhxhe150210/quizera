<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Edit user</h3>
  </div>
</div>

<div class="m-2">
  <a class="inline-block px-4 py-2 bg-red-500 rounded border text-white m-1" href="${pageContext.request.contextPath}/dashboard/users">Back</a>
  <div class="flex flex-col items-center py-20 text-xl">
    <div class="flex flex-col text-center space-y-2">
      <div class="w-full flex justify-center">
        <img class="w-32 h-32 rounded-full" src="${theUser.getAvatarUrl()}" alt="avatar"/>
      </div>
      <span class="text-3xl">${theUser.getUsername()}</span>
    </div>
    <div class="flex flex-row justify-center mt-14">
      <a href="${pageContext.request.contextPath}/dashboard/users/delete?username=${theUser.getUsername()}"
         class="bg-red-600 text-white py-4 px-6 rounded"
         onclick="return confirm('You sure want to delete user ${theUser.getUsername()}?')">Delete</a>
    </div>
    <form action="${pageContext.request.contextPath}/dashboard/users/update" method="POST"
          class="flex flex-col w-1/3 mt-14 space-y-3">
      <input type="hidden" name="username" value="${theUser.getUsername()}">
      <div class="flex flex-row justify-between">
        <span>Username:</span>
        <span>${theUser.getUsername()}</span>
      </div>
      <div class="flex flex-row justify-between">
        <span>Email:</span>
        <span>${theUser.getEmail()}</span>
      </div>
      <div class="flex flex-row justify-between">
        <label for="role">Role:</label>
        <select id="role" class="rounded" name="role" ${theUser.getUsername() == user.getUsername() ? "disabled" : ""}>
          <option value="CUSTOMER" ${theUser.getRole() == "CUSTOMER" ? "selected" : ""}>Customer</option>
          <option value="MARKETING" ${theUser.getRole() == "MARKETING" ? "selected" : ""}>Marketing</option>
          <option value="SALE" ${theUser.getRole() == "SALE" ? "selected" : ""}>Sale</option>
          <option value="EXPERT" ${theUser.getRole() == "EXPERT" ? "selected" : ""}>Expert</option>
          <option value="ADMIN" ${theUser.getRole() == "ADMIN" ? "selected" : ""}>Admin</option>
        </select>
      </div>

      <div class="flex flex-row justify-between items-center w-full h-10">
        <span>Verify status:</span>
        <span>${theUser.isVerified() ? "Verified" : "Unverified"}</span>
      </div>

      <div class="flex flex-row justify-between items-center w-full h-10">
        <label for="fullName">Full name:</label>
        <input id="fullName" type="text" name="fullName" class="rounded" value="${theUser.getFullName()}">
      </div>

      <div class="flex flex-row justify-between items-center w-full h-10">
        <label for="gender">Gender:</label>
        <select id="gender" name="gender" class="rounded">
          <option value="" ${theUser.getGender() == null ? "selected" : ""}>-- Select one --</option>
          <option value="1" ${theUser.getGender() != null && theUser.getGender().getId() == 1 ? "selected" : ""}>Male
          </option>
          <option value="2" ${theUser.getGender() != null && theUser.getGender().getId() == 2 ? "selected" : ""}>
            Female
          </option>
          <option value="3" ${theUser.getGender() != null && theUser.getGender().getId() == 3 ? "selected" : ""}>Other
          </option>
        </select>
      </div>

      <div class="flex flex-row justify-between items-center w-full h-10">
        <label for="mobile">Mobile phone:</label>
        <input id="mobile" type="text" name="mobile" class="rounded" value="${theUser.getMobile()}">
      </div>

      <div class="flex flex-row justify-between items-center w-full h-10">
        <label for="avatarUrl">Avatar URL:</label>
        <input id="avatarUrl" type="text" name="avatarUrl" class="rounded" value="${theUser.getAvatarUrl()}">
      </div>

      <button class="bg-blue-500 py-2 px-6 text-white rounded" type="submit">Save Profile</button>
    </form>
  </div>
</div>
