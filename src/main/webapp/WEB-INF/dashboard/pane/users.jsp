<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: an7
  Date: 5/19/21
  Time: 12:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Users</h3>
  </div>
</div>

<div class="m-2">
  <form action="${pageContext.request.contextPath}/dashboard/users" class="flex flex-row">
    <label for="filter-type">Filter by</label>
    <select id="filter-type" name="filter-type" class="w-1/5 rounded">
      <option value="username">Username</option>
      <option value="email">Email</option>
    </select>
    <input type="search" name="filter" placeholder="Filter" value="${filter}"
           class="ml-4 w-full bg-gray-900 text-white transition border border-transparent focus:outline-none focus:border-gray-400 rounded py-3 px-2 pl-10 appearance-none leading-normal">
  </form>

  <table class="w-full">
    <thead>
    <tr class="h-20 bg-gray-500 text-left text-xl">
      <th class="p-6">Username</th>
      <th class="p-6">Email</th>
      <th class="p-6">Role</th>
      <th class="p-6 text-center">Actions</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${users}" var="listUser">
      <tr class="h-20 text-xl border-b border-gray-500">
        <td class="p-6">${listUser.getUsername()}</td>
        <td class="p-6">${listUser.getEmail()}</td>
        <td class="p-6">${listUser.getRole().getName()}</td>
        <td class="p-6">
          <div class="flex flex-row gap-2 justify-center">
            <a href="${pageContext.request.contextPath}/dashboard/users/update?username=${listUser.getUsername()}">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                   stroke="#50e3c2" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                <path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path>
                <polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon>
              </svg>
            </a>
          </div>
        </td>
      </tr>
    </c:forEach>
    </tbody>
  </table>

  <div class="flex flex-row gap-3">
    <span>Page</span>
    <%
      String query = (String) request.getAttribute("query");
      int currentPage = (Integer) request.getAttribute("page");
      int maxPage = (Integer) request.getAttribute("maxPage");

      int startOfPageNav = Math.max(1, currentPage - 2);
      for (int thePage = startOfPageNav; thePage <= Math.min(maxPage, startOfPageNav + 4); thePage++) {
        if (thePage == currentPage) {
    %>
    <span><%=thePage%></span>
    <%
        continue;
      }
    %>
    <a
      href="${pageContext.request.contextPath}/dashboard/users<%=query%>&page=<%=thePage%>"><%=thePage%>
    </a>
    <%}%>

  </div>
  <form action="${pageContext.request.contextPath}/dashboard/users" class="">

  </form>
</div>
