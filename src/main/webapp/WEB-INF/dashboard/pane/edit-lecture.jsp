<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Edit lecture</h3>
  </div>
</div>

<a href="${pageContext.request.contextPath}/dashboard/lectures?id=${lecture.chapter.id}">Back</a>
<form id="form" class="flex flex-col" action="${pageContext.request.contextPath}/dashboard/lectures/update"
      method="POST">
  <input type="hidden" name="id" value="${lecture.id}">

  <label for="title">
    Title:
    <input type="text" id="title" name="title" value="${lecture.title}">
  </label>

  <label for="content">
    Content:
    <input type="text" id="content" name="content" value="${lecture.content}">
  </label>

  <input type="submit" value="Submit">
</form>
