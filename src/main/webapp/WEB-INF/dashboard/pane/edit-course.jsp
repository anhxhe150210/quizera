<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Edit course</h3>
  </div>
</div>

<a href="${pageContext.request.contextPath}/dashboard/courses" class="inline-block px-4 py-2 bg-red-500 rounded border text-white m-1">Back</a>

<div class="flex flex-col">
  <a href="${pageContext.request.contextPath}/dashboard/courses/editCategories?id=${course.getId()}" class="w-1/12 rounded m-1 border p-1 text-center text-white bg-green-500 hover:bg-green-700">Edit Categories</a>
  <a href="${pageContext.request.contextPath}/dashboard/courses/prices?id=${course.getId()}" class="w-1/12 rounded m-1 border p-1 text-center text-white bg-green-500 hover:bg-green-700">Edit Prices</a>
  <a href="${pageContext.request.contextPath}/dashboard/courses/dimensions?id=${course.getId()}" class="w-1/12 rounded m-1 border p-1 text-center text-white bg-green-500 hover:bg-green-700">Edit Dimensions</a>
</div>

<form id="form" class="flex flex-col" action="${pageContext.request.contextPath}/dashboard/courses/update"
      method="POST">
  <input type="hidden" name="id" value="${course.getId()}">


  <label for="title" class="mx-2">Title</label>
  <input type="text" id="title" class="m-2 rounded" name="title" value="${course.getTitle()}">

  <label for="description" class="mx-2">Description</label>
  <textarea form="form" name="description" id="description" class="m-2 rounded" cols="30" rows="10">${course.getDescription()}</textarea>

  <label for="featured" class="my-1 mx-2">Featured?</label>
  <input type="checkbox" id="featured" name="featured" class="my-1 mx-2" ${course.isFeatured() ? "checked" : ""}>

  <label for="thumbnailUrl" class="my-1 mx-2">Thumbnail URL</label>
  <input type="text" id="thumbnailUrl" name="thumbnailUrl" class="my-1 mx-2 rounded" value="${course.getThumbnailUrl()}">

  <label for="archived" class="my-1 mx-2">Archived?</label>
  <input type="checkbox" id="archived" name="archived" class="my-1 mx-2" ${course.isArchived() ? "checked" : ""}>

  <label for="published" class="my-1 mx-2">Published?</label>
  <input type="checkbox" id="published" name="published" class="my-1 mx-2" ${course.isPublished() ? "checked" : ""}>

  <button type="submit" class="mt-1 p-4 bg-blue-500 hover:bg-blue-700 text-white text-xl mx-2 rounded">Submit</button>
</form>
