<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Edit price</h3>
  </div>
</div>

<div class="flex flex-row gap-6">
  <a class="inline-block px-4 py-2 bg-red-500 rounded border text-white m-1" class="p-4 bg-red-600" href="${pageContext.request.contextPath}/dashboard/courses/prices?id=${price.course.id}">Back</a>
</div>

<form id="form" class="flex flex-col gap-4 mt-6 w-1/2"
      action="${pageContext.request.contextPath}/dashboard/courses/prices/update"
      method="POST">
  <input type="hidden" name="id" value="${price.id}">

  <label for="days">
    Duration (in days):
    <input type="number" id="days" class="w-full" name="days" value="${price.days}">
  </label>

  <label for="listPrice">
    List Price:
    <input type="number" step="0.01" id="listPrice" class="w-full" name="listPrice" value="${price.saleFrom}">
  </label>

  <label for="salePrice">
    Sale Price:
    <input type="number" step="0.01" id="salePrice" class="w-full" name="salePrice" value="${price.price}">
  </label>

  <input type="submit" class="p-4 bg-green-500 cursor-pointer" value="Submit">
</form>
