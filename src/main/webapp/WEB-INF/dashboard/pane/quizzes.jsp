<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: an7
  Date: 5/19/21
  Time: 12:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Quizzes</h3>
  </div>
</div>

<%--<a href="${pageContext.request.contextPath}/dashboard/chapters/update?id=${chapter.id}">Back</a>--%>
<div class="p-2">
  <label>
    Select Course
    <select id="select_course" class="rounded">
      <option value="">-- Select Course --</option>
      <c:forEach items="${courses}" var="iCourse">
        <option
          value="${iCourse.id}" ${(course != null && course.id == iCourse.id) ? "selected" : ""}>${iCourse.title}</option>
      </c:forEach>
    </select>
  </label>
</div>
<c:if test="${course != null}">
  <div class="m-2">
    <table class="w-full mb-3">
      <thead>
      <tr class="h-20 bg-gray-500 text-left text-xl">
        <th class="p-6">Id</th>
        <th class="p-6">Title</th>
        <th class="p-6">Level</th>
        <th class="p-6 text-center">Duration (second)</th>
        <th class="p-6 text-center"># of questions</th>
        <th class="p-6">Actions</th>
      </tr>
      </thead>
      <tbody>
      <c:forEach items="${quizzes}" var="quiz">
        <tr class="h-20 text-xl border-b border-gray-500">
          <td class="p-6">${quiz.id}</td>
          <td class="p-6">${quiz.title}</td>
          <td class="p-6">${quiz.level.description}</td>
          <td class="p-6 text-center">${quiz.durationInSeconds}</td>
          <td class="p-6 text-center">${quiz.numberOfQuestions}</td>
          <td class="p-6">
            <div class="flex flex-row gap-2">
              <a href="${pageContext.request.contextPath}/dashboard/quizzes/update?id=${quiz.id}" class="inline-block mr-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="#50e3c2" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                  <path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path>
                  <polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon>
                </svg>
              </a>
              <a href="${pageContext.request.contextPath}/dashboard/quizzes/delete?id=${quiz.id}" class="inline-block">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="#d0021b" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                  <polyline points="3 6 5 6 21 6"></polyline>
                  <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                  <line x1="10" y1="11" x2="10" y2="17"></line>
                  <line x1="14" y1="11" x2="14" y2="17"></line>
                </svg>
              </a>
            </div>
          </td>
        </tr>
      </c:forEach>
      </tbody>
    </table>
    <a href="${pageContext.request.contextPath}/dashboard/quizzes/add?id=${course.id}" class="border rounded bg-green-500 hover:bg-green-700 p-2 text-white mt-2">+ Add a quiz</a>
  </div>
</c:if>

<script>
    const select = document.getElementById("select_course");

    select.addEventListener("change", ev => {
        const course = select.value;
        console.log(course)
        if (course == null || course === "") {
            return;
        }
        window.location.href = "${pageContext.request.contextPath}/dashboard/quizzes?id=" + select.value;
    })
</script>
