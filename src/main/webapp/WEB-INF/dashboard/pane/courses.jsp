<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: an7
  Date: 5/19/21
  Time: 12:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Courses</h3>
  </div>
</div>

<div class="m-2 mt-4">
  <a href="${pageContext.request.contextPath}/dashboard/courses/add"
     class="border rounded bg-green-500 hover:bg-green-700 p-2 text-white mt-2">Add Course</a>
  <form action="${pageContext.request.contextPath}/dashboard/courses/list" method="POST" class="flex flex-row pt-2">
    <span class="inline-block text-center align-middle p-2">Filter by</span>

    <div>
      <c:forEach items="${categories}" var="category">
        <label>
            ${category.getDescription()}
          <input type="checkbox" name="categories" value="${category.getId()}">
        </label>
      </c:forEach>
      <label for="visibility">
        Visibility
        <select name="visibility" id="visibility" class="rounded">
          <option value="all">All</option>
          <option value="visible">Visible</option>
          <option value="hidden">Hidden</option>
        </select>
      </label>
      <label for="name">
        Name:
        <input id="name" type="text" name="name" class="rounded">
      </label>
    </div>
    <button type="submit" class="p-2 ml-2 border rounded align-middle bg-blue-500 hover:bg-blue-700 text-white">Search
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#ffffff"
           stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="inline">
        <circle cx="11" cy="11" r="8"></circle>
        <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
      </svg>
    </button>
  </form>

  <table class="w-full">
    <thead>
    <tr class="h-20 bg-gray-700 text-left text-xl">
      <th class="p-6 text-center text-white">Id</th>
      <th class="p-6 text-center text-white">Title</th>
      <th class="p-6 text-center text-white">Categories</th>
      <th class="p-6 text-center text-white">Number of lessons</th>
      <th class="p-6 text-center text-white">Owner</th>
      <th class="p-6 text-center text-white">Visibility</th>
      <th class="p-6 text-center text-white">Action</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${courses}" var="listCourse">
      <tr class="h-20 text-xl border-b border-gray-500">
        <td class="p-6 text-center">${listCourse.getId()}</td>
        <td class="p-6 text-center">${listCourse.getTitle()}</td>
        <td class="p-6 text-center">
          <c:forEach items="${listCourse.getCategories()}" var="courseCategory">
            <span>${courseCategory.getDescription()}</span> <br>
          </c:forEach>
        </td>
        <td class="p-6 text-center">${listCourse.getNumberOfLessons()}</td>
        <td class="p-6 text-center">${listCourse.getOwner().getDisplayName()}</td>
        <td class="p-6 text-center">${listCourse.isArchived() ? "Hidden" : "Visible"}</td>
        <td class="p-6 text-center">
          <a href="${pageContext.request.contextPath}/dashboard/courses/update?id=${listCourse.getId()}"
             class="inline-block mr-2">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="#50e3c2" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
              <path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path>
              <polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon>
            </svg>
          </a>
          <a href="${pageContext.request.contextPath}/dashboard/courses/delete?id=${listCourse.getId()}"
             class="inline-block"
             onclick="return confirm('Delete course ${listCourse.title}?')">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="#d0021b" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
              <polyline points="3 6 5 6 21 6"></polyline>
              <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
              <line x1="10" y1="11" x2="10" y2="17"></line>
              <line x1="14" y1="11" x2="14" y2="17"></line>
            </svg>
          </a>
        </td>
      </tr>
    </c:forEach>
    </tbody>
  </table>
</div>
