<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Edit Questions</h3>
  </div>
</div>
<div class="p-2">
  <label>
    Select Course
    <select id="select_course" class="rounded">
      <option value="">-- Select Course --</option>
      <c:forEach items="${courses}" var="iCourse">
        <option
          value="${iCourse.id}" ${(course != null && course.id == iCourse.id) ? "selected" : ""}>${iCourse.title}</option>
      </c:forEach>
    </select>
  </label>
</div>

<c:if test="${questions != null}">
  <table class="divide-y divide-gray-200 w-full">
    <thead>
    <tr>
      <th class="px-4 py-2 text-center">ID</th>
      <th class="px-4 py-2 text-center">Question</th>
      <th class="px-4 py-2 text-center">Type</th>
      <th class="px-4 py-2 text-center">Dimension</th>
      <th class="px-4 py-2 text-center">Actions</th>
    </tr>
    </thead>
    <tbody class="divide-y divide-gray-200">
    <c:forEach items="${questions}" var="q">
      <tr>
        <td class="px-4 py-2 text-center">${q.id}</td>
        <td class="question px-4 py-2 text-center">${q.question}</td>
        <td class="px-4 py-2 text-center">${q.dimension.type}</td>
        <td class="px-4 py-2 text-center">${q.dimension.dimension}</td>
        <td class="px-4 py-2 text-center">
          <a
            href="${pageContext.request.contextPath}/dashboard/questions/update?id=${q.id}">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="#50e3c2" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                 class="inline-block mr-2">
              <path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path>
              <polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon>
            </svg>
          </a>
          <a
            href="${pageContext.request.contextPath}/dashboard/questions/delete?id=${q.id}"
            onclick="return confirm('You sure want to delete question ${q.id}?')">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="#d0021b" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="inline-block">
              <polyline points="3 6 5 6 21 6"></polyline>
              <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
              <line x1="10" y1="11" x2="10" y2="17"></line>
              <line x1="14" y1="11" x2="14" y2="17"></line>
            </svg>
          </a>
        </td>
      </tr>
    </c:forEach>
    </tbody>
  </table>
  <a class="rounded bg-green-500 text-white p-2 hover:bg-green-700 m-2" href="${pageContext.request.contextPath}/dashboard/questions/import?courseId=${course.id}">Add/Import</a>
</c:if>

<script src="https://cdn.jsdelivr.net/npm/editorjs-html@3.0.3/build/edjsHTML.browser.js"></script>
<script>
    const select = document.getElementById("select_course");

    select.addEventListener("change", ev => {
        const course = select.value;
        console.log(course)
        if (course == null || course === "") {
            return;
        }
        window.location.href = "${pageContext.request.contextPath}/dashboard/questions?course=" + select.value;
    })

    const edjsParser = edjsHTML();
    document.querySelectorAll(".question").forEach(e => {
        e.innerHTML = edjsParser.parse(JSON.parse(e.innerHTML)).join('\n');
    })
</script>
