<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Edit chapter</h3>
  </div>
</div>

<a href="${pageContext.request.contextPath}/dashboard/chapters?id=${chapter.course.id}">Back</a>

<a
  href="${pageContext.request.contextPath}/dashboard/lectures?id=${chapter.id}">
  Edit Lectures
</a>
<a
  href="${pageContext.request.contextPath}/dashboard/quizzes?id=${chapter.id}">
  Edit Quizzes
</a>
<form id="form" class="flex flex-col" action="${pageContext.request.contextPath}/dashboard/chapters/update"
      method="POST">
  <input type="hidden" name="id" value="${chapter.getId()}">

  <label for="index">
    Index:
    <input type="number" id="index" name="index" value="${chapter.getIndex()}">
  </label>

  <label for="title">
    Title:
    <input type="text" id="title" name="title" value="${chapter.getTitle()}">
  </label>

  <input type="submit" value="Submit">
</form>
