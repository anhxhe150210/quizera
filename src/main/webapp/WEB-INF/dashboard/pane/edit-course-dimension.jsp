<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Edit dimension</h3>
  </div>
</div>

<a class="inline-block px-4 py-2 bg-red-500 rounded border text-white m-1" href="${pageContext.request.contextPath}/dashboard/courses/dimensions?id=${dimension.course.id}">Back</a>

<form id="form" class="flex flex-col" action="${pageContext.request.contextPath}/dashboard/courses/dimensions/update"
      method="POST">
  <input type="hidden" name="id" value="${dimension.getId()}">

  <label for="type">
    Type:
    <input type="text" id="type" name="type" value="${dimension.getType()}">
  </label>

  <label for="dimension">
    Dimension:
    <input type="text" id="dimension" name="dimension" value="${fn:escapeXml(dimension.getDimension())}">
  </label>

  <input type="submit" value="Submit">
</form>
