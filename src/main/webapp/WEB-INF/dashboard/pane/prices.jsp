<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Edit course prices</h3>
  </div>
</div>

<a class="inline-block px-4 py-2 bg-red-500 rounded border text-white m-1"
   href="${pageContext.request.contextPath}/dashboard/courses/update?id=${course.id}">Back</a>

<table class="w-full divide-y divide-gray-200">
  <thead>
  <tr>
    <th class="px-4 py-2">Duration (days)</th>
    <th class="px-4 py-2">List Price</th>
    <th class="px-4 py-2">Sale Price</th>
    <th class="px-4 py-2">Actions</th>
  </tr>
  </thead>
  <tbody class="divide-y divide-gray-200">
  <c:forEach items="${prices}" var="price">
    <tr>
      <td class="text-center">${price.days}</td>
      <td class="text-center">${price.saleFrom}</td>
      <td class="text-center">${price.price}</td>
      <td>
        <div class="px-4 py-2 flex gap-2 justify-center">
          <a href="${pageContext.request.contextPath}/dashboard/courses/prices/update?id=${price.id}">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="#50e3c2" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
              <path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path>
              <polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon>
            </svg>
          </a>
          <a href="${pageContext.request.contextPath}/dashboard/courses/prices/delete?id=${price.id}"
             onclick="return confirm('Delete price ${price.price}?')">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="#d0021b" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
              <polyline points="3 6 5 6 21 6"></polyline>
              <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
              <line x1="10" y1="11" x2="10" y2="17"></line>
              <line x1="14" y1="11" x2="14" y2="17"></line>
            </svg>
          </a>
        </div>
      </td>
    </tr>
  </c:forEach>
  </tbody>
</table>
<a class="border rounded bg-green-500 hover:bg-green-700 p-2 text-white mt-2"
   href="${pageContext.request.contextPath}/dashboard/courses/prices/add?id=${course.id}">+ Add Price</a>
