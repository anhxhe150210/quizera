<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Import questions</h3>
  </div>
</div>

<form action="${pageContext.request.contextPath}/dashboard/questions/import" method="POST">
  <input type="hidden" name="courseId" value="${courseId}">
  <p class="m-2">Please provide a semicolon-separated list without header. <br>
    Each line would be a different question. <br>
    The first column is the question, the n columns followed is the answer. <br>
    The correct answer will have a "~" (tilde) symbol at the beginning.</p>
  <label for="csv" class="ml-2">CSV</label>
  <textarea id="csv" name="csv" class="w-full mx-2 rounded"></textarea>
  <button type="submit" class="p-4 bg-blue-500 text-white text-xl w-full rounded mx-2 my-3">Submit</button>
</form>
