<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: an7
  Date: 5/19/21
  Time: 12:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="bg-gray-800 pt-3">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">Blogs</h3>
  </div>
</div>

<div class="m-2">
  <a href="${pageContext.request.contextPath}/dashboard/blogs/add"
     class="border rounded bg-green-500 hover:bg-green-700 p-2 text-white mt-2">Add Blog</a>

  <table class="w-full mt-2">
    <thead>
    <tr class="h-20 bg-gray-500 text-left text-xl">
      <th class="p-6">Title</th>
      <th class="p-6">Author</th>
      <th class="p-6">Action</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${blogs}" var="blog">
      <tr class="h-20 text-xl border-b border-gray-500">
        <td class="p-6">${blog.title}</td>
        <td class="p-6">${blog.author.getDisplayName()}</td>
        <td class="p-6">
          <a href="${pageContext.request.contextPath}/dashboard/blogs/update?id=${blog.id}" class="inline-block mr-2">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="#50e3c2" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
              <path d="M20 14.66V20a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h5.34"></path>
              <polygon points="18 2 22 6 12 16 8 16 8 12 18 2"></polygon>
            </svg>
          </a>
          <a href="${pageContext.request.contextPath}/dashboard/blogs/delete?id=${blog.id}"
             onclick="return confirm('Delete blog ${blog.title}?')" class="inline-block">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                 stroke="#d0021b" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
              <polyline points="3 6 5 6 21 6"></polyline>
              <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
              <line x1="10" y1="11" x2="10" y2="17"></line>
              <line x1="14" y1="11" x2="14" y2="17"></line>
            </svg>
          </a>
        </td>
      </tr>
    </c:forEach>
    </tbody>
  </table>
</div>
