<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<html>
<head>
  <title>Dashboard</title>
  <script src="${pageContext.request.contextPath}/styles.js"></script>
  <script src="https://kit.fontawesome.com/051f6677c6.js" crossorigin="anonymous"></script>
</head>
<body class="bg-gray-800 font-sans leading-normal tracking-normal mt-12">

<%@include file="../topnav.jsp" %>

<div class="flex flex-col md:flex-row">

  <%@include file="component/sidebar.jsp" %>

  <div class="main-content flex-1 bg-gray-100 mt-12 md:mt-2 pb-24 md:pb-5">
    <c:choose>
      <c:when test="${pane == null}">
        <div class="bg-gray-800 pt-3">
          <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
            <h3 class="font-bold pl-2">Welcome</h3>
          </div>
        </div>
      </c:when>
      <c:otherwise>
        <c:import url="pane/${pane}.jsp"/>
      </c:otherwise>
    </c:choose>
  </div>
</div>

<script src="${pageContext.request.contextPath}/dashboard.js"></script>
</body>
</html>
