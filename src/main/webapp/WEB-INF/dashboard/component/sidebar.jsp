<%@ page import="java.util.regex.Pattern" %>
<%@ page import="java.util.regex.Matcher" %><%--
  Created by IntelliJ IDEA.
  User: an7
  Date: 5/18/21
  Time: 10:35 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<div class="bg-gray-800 shadow-xl h-16 fixed bottom-0 mt-12 md:relative md:h-screen z-10 w-full md:w-48">
  <div class="md:mt-12 md:w-48 md:fixed md:left-0 md:top-0 content-center md:content-start text-left justify-between">
    <ul class="list-reset flex flex-row md:flex-col py-0 md:py-3 px-1 md:px-2 text-center md:text-left">
      <li class="mr-3 flex-1">
        <a href="${pageContext.request.contextPath}/dashboard"
           class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 ${pane == null ? "border-pink-500" : "border-gray-800 hover:border-pink-500"}">
          <i class="fas fa-tasks pr-0 md:pr-3"></i><span
          class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">Home</span>
        </a>
      </li>
      <c:if test="${user.getRole() == 'ADMIN'}">
        <li class="mr-3 flex-1">
          <a href="${pageContext.request.contextPath}/dashboard/users"
             class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 ${pane == "users" ? "border-purple-500" : "border-gray-800 hover:border-purple-500"}">
            <i class="fa fa-envelope pr-0 md:pr-3"></i><span
            class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">Users</span>
          </a>
        </li>
      </c:if>
      <c:if test="${user.getRole() == 'EXPERT' || user.getRole() == 'ADMIN'}">
        <li class="mr-3 flex-1">
          <a href="${pageContext.request.contextPath}/dashboard/questions"
             class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 ${pane == "questions" ? "border-green-500" : "border-gray-800 hover:border-green-500"}">
            <i class="fa fa-envelope pr-0 md:pr-3"></i><span
            class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">Edit Questions</span>
          </a>
        </li>
      </c:if>
      <c:if test="${user.getRole() == 'EXPERT' || user.getRole() == 'ADMIN'}">
        <li class="mr-3 flex-1">
          <a href="${pageContext.request.contextPath}/dashboard/courses"
             class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 ${pane == "courses" ? "border-green-500" : "border-gray-800 hover:border-green-500"}">
            <i class="fa fa-envelope pr-0 md:pr-3"></i><span
            class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">Courses</span>
          </a>
        </li>
      </c:if>
      <c:if test="${user.getRole() == 'MARKETING' || user.getRole() == 'ADMIN'}">
        <li class="mr-3 flex-1">
          <a href="${pageContext.request.contextPath}/dashboard/blogs"
             class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 ${pane == "blogs" ? "border-green-500" : "border-gray-800 hover:border-green-500"}">
            <i class="fa fa-envelope pr-0 md:pr-3"></i><span
            class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">Blogs</span>
          </a>
        </li>
      </c:if>
      <c:if test="${user.getRole() == 'EXPERT' || user.getRole() == 'ADMIN'}">
        <li class="mr-3 flex-1">
          <a href="${pageContext.request.contextPath}/dashboard/quizzes"
             class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 ${pane == "quizzes" ? "border-green-500" : "border-gray-800 hover:border-green-500"}">
            <i class="fa fa-envelope pr-0 md:pr-3"></i><span
            class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">Edit Quiz</span>
          </a>
        </li>
      </c:if>
      <%--            <li class="mr-3 flex-1">--%>
      <%--                <a href="#" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-blue-600">--%>
      <%--                    <i class="fas fa-chart-area pr-0 md:pr-3 text-blue-600"></i><span class="pb-1 md:pb-0 text-xs md:text-base text-white md:text-white block md:inline-block">Analytics</span>--%>
      <%--                </a>--%>
      <%--            </li>--%>
      <%--            <li class="mr-3 flex-1">--%>
      <%--                <a href="#" class="block py-1 md:py-3 pl-0 md:pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-gray-800 hover:border-red-500">--%>
      <%--                    <i class="fa fa-wallet pr-0 md:pr-3"></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">Payments</span>--%>
      <%--                </a>--%>
      <%--            </li>--%>
    </ul>
  </div>


</div>
