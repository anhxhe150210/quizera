<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.t4.quizera.model.User" %><%--
  Created by IntelliJ IDEA.
  User: an7
  Date: 5/14/21
  Time: 10:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Profile</title>
  <script src="${pageContext.request.contextPath}/styles.js"></script>
</head>
<body class="mt-20">
<c:import url="topnav.jsp"/>

<div class="flex flex-col items-center mx-20 my-4 py-20 border border-2 border-gray-400 rounded-md">
  <div class="flex flex-col text-center space-y-2">
    <div class="w-full flex justify-center">
      <%--      <img class="w-32 h-32 rounded-full" src="http://placekitten.com/301/301" alt="avatar"/>--%>
      <img class="w-32 h-32 rounded-full" src="${user.getAvatarUrl()}" alt="avatar"/>
    </div>
    <span class="text-3xl">${user.getDisplayName()}</span>
  </div>

  <form action="${pageContext.request.contextPath}/profile" method="POST"
        class="flex flex-col w-1/3 mt-14 space-y-4 items-center">
    <div class="flex flex-row justify-between items-center w-full h-10">
      <label for="username">Username:</label>
      <input class="bg-gray-300 rounded" id="username" type="text" value="${user.getUsername()}" disabled>
    </div>

    <div class="flex flex-row justify-between items-center w-full h-10">
      <label for="email">Email:</label>
      <input class="bg-gray-300 rounded" id="email" type="text" value="${user.getEmail()}" disabled>
    </div>

    <div class="flex flex-row justify-between items-center w-full h-10">
      <span>Password:</span>
      <span><a class="text-blue-700 font-bold"
               href="${pageContext.request.contextPath}/changepwd">Change Password</a></span>
    </div>

    <div class="flex flex-row justify-between items-center w-full h-10">
      <span>Role:</span>
      <span>${user.getRole().getName()}</span>
    </div>

    <div class="flex flex-row justify-between items-center w-full h-10">
      <span>Verify status:</span>
      <span>${user.isVerified() ? "Verified" : "Unverified"}</span>
    </div>

    <div class="flex flex-row justify-between items-center w-full h-10">
      <label for="fullName">Full name:</label>
      <input id="fullName" type="text" name="fullName" class="rounded" value="${user.getFullName()}">
    </div>

    <div class="flex flex-row justify-between items-center w-full h-10">
      <label for="gender">Gender:</label>
      <select id="gender" name="gender" class="rounded">
        <option value="" ${user.getGender() == null ? "selected" : ""}>-- Select one --</option>
        <option value="1" ${user.getGender() != null && user.getGender().getId() == 1 ? "selected" : ""}>Male</option>
        <option value="2" ${user.getGender() != null && user.getGender().getId() == 2 ? "selected" : ""}>Female</option>
        <option value="3" ${user.getGender() != null && user.getGender().getId() == 3 ? "selected" : ""}>Other</option>
      </select>
    </div>

    <div class="flex flex-row justify-between items-center w-full h-10">
      <label for="mobile">Mobile phone:</label>
      <input id="mobile" type="text" name="mobile" class="rounded" value="${user.getMobile()}">
    </div>

    <div class="flex flex-row justify-between items-center w-full h-10">
      <label for="avatarUrl">Avatar URL:</label>
      <input id="avatarUrl" type="text" name="avatarUrl" class="rounded" value="${user.getAvatarUrl()}">
    </div>

    <button class="bg-blue-500 py-2 px-6 rounded text-white" type="submit">Save Profile</button>
  </form>
</div>
<c:import url="footer.jsp"/>
</body>
</html>
