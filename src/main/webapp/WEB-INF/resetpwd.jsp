<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Reset Password</title>
  <script src="${pageContext.request.contextPath}/styles.js"></script>

  <c:set var="message" scope="request" value="${message}"/>
</head>
<body class="flex items-center justify-center h-screen">
<form class="flex flex-col w-1/4 space-y-3" action="resetpwd" method="POST">
  <h1 class="text-3xl">Reset Password</h1>

  <c:if test="${message != null}">
    <span>${message}</span>
  </c:if>

  <label class="block" for="email">
    <span>Email</span>
    <input
      class="border appearance-none focus:outline-none mt-1 w-full rounded-md border-gray-300 shadow-sm focus:ring focus:border-indigo-300 focus:ring-indigo-200"
      type="text" id="email" name="email"/>
  </label>

  <button class="w-full border bg-blue-700 mt-2 shadow-md rounded-md py-3 font-bold text-white active:bg-blue-900"
          type="submit">Reset Password
  </button>
</form>
</body>
</html>
