<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: xuana
  Date: 5/22/2021
  Time: 11:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>${course.title}</title>
  <script src="${pageContext.request.contextPath}/styles.js"></script>
  <style>
      @import url(https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.min.css);

      .text {
          height: 100px;
          overflow: hidden;
      }

      .container {
          position: relative;
      }

      label {
          position: absolute;
          top: 100%;
      }

      label:after {
          content: "Show More";
      }

      input:checked + label:after {
          content: "Show Less";
      }

      input:checked ~ div {
          height: 100%;
      }
  </style>

</head>
<body>
<c:import url="topnav.jsp"/>
<div class="pt-16 pl-10 pr-10 min-w-screen min-h-screen flex-col items-center overflow-hidden relative">
  <div class="w-full max-w-6xl rounded bg-white shadow-xl p-10 lg:p-20 mx-auto text-gray-800 relative md:text-left">
    <div class="md:flex items-center -mx-10">
      <div class="w-full md:w-1/2 px-10 mb-10 md:mb-0">
        <div class="relative">
          <img src="https://imgur.com/CF8xFJb.png" class="w-full relative z-10" alt=""/>
        </div>
      </div>
      <div class="w-full px-10">
        <div class="mb-10">
          <h1 class="font-bold uppercase text-3xl mb-5">${course.getTitle()}</h1>
        </div>
        <div>
          <div class="inline-block align-bottom mr-5">
            <span class="text-s leading-none align-baseline">Creator: <a href="#"
                                                                         class="font-bold">${course.getOwnerRepresentation()}</a> </span><br/>
            <span
              class="text-s leading-none align-baseline">Updated date: <span> ${course.getUpdatedDate()} </span></span><br/>
            <br/>
            <span class="text-s leading-none align-baseline">Course Price</span><br/>
            <span class="text-2xl leading-none align-baseline">$</span>
            <span class="text-2xl line-through leading-none align-baseline">${showPrice.getSaleFrom()}</span>
            <br/>
            <span class="text-2xl leading-none align-baseline">$</span>
            <span class="font-bold text-5xl leading-none align-baseline">${showPrice.getPrice()}</span>
          </div>
          <br/>
          <br/>
          <div class="inline-block align-bottom">
            <c:choose>
              <c:when test="${bought}">
              <span
                class="bg-gray-400 opacity-75 text-yellow-900 rounded-full px-10 py-2 font-semibold">
                OWNED
              </span>
              </c:when>
              <c:otherwise>
                <a href="${pageContext.request.contextPath}/checkout?courseId=${course.id}"
                   class="bg-yellow-300 opacity-75 hover:opacity-100 text-yellow-900 hover:text-gray-900 rounded-full px-10 py-2 font-semibold">
                  BUY NOW
                </a>
              </c:otherwise>
            </c:choose>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="w-full max-w-6xl rounded bg-white shadow-xl p-10 lg:p-20 mx-auto text-gray-800 relative md:text-left">
    <div class="md:flex-col items-center -mx-10">
      <div class="w-full md:w-1/2 px-10 mb-10 md:mb-0"></div>
      <div class="w-full px-10">
        <div class="container mb-10">
          <h1 class="font-bold uppercase text-2xl mb-5">Course description</h1>
          <input id="ch" class="hidden" type="checkbox"/>
          <label for="ch" class="underline hover:cursor-pointer"></label>
          <div class="text text-s">${course.getDescription()}</div>
        </div>
      </div>
    </div>
  </div>
</div>
<c:import url="footer.jsp"/>
</body>
</html>
