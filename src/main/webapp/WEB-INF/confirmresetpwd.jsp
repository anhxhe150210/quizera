<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Reset Password</title>
  <script src="${pageContext.request.contextPath}/styles.js"></script>
</head>
<body class="flex items-center justify-center h-screen">
<form class="flex flex-col w-1/4 space-y-3" action="confirmresetpwd" method="POST">
  <input type="hidden" name="code" value="${code}"/>

  <h1 class="text-3xl">Reset Password</h1>

  <c:if test="${message != null}">
    <span>${message}</span>
  </c:if>

  <label class="block text-grey-darker text-sm font-bold mb-2" for="newpassword">
    <span>New password</span>
    <input class="appearance-none border rounded w-full py-2 px-3 text-grey-darker"
           type="password" id="newpassword" name="newpassword" placeholder="New password"/>
  </label>

  <label class="block text-grey-darker text-sm font-bold mb-2" for="repassword">
    <span>Re-input your new password</span>
    <input class="appearance-none border rounded w-full py-2 px-3 text-grey-darker"
           type="password" id="repassword" name="repassword" placeholder="Re-input new password"/>
  </label>

  <button class="w-full border bg-blue-700 mt-2 shadow-md rounded-md py-3 font-bold text-white active:bg-blue-900"
          type="submit">Confirm
  </button>
</form>
</body>
</html>
