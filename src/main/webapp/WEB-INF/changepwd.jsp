<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.t4.quizera.model.User" %><%--
  Created by IntelliJ IDEA.
  User: Seth_Etherald
  Date: 5/12/2021
  Time: 6:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Change your password</title>
  <script src="${pageContext.request.contextPath}/styles.js"></script>
  <%
    User user = (User) request.getSession().getAttribute("user");

    if (user == null) {
      response.sendRedirect(request.getContextPath() + "/login");
      return;
    }
  %>
</head>
<body class="flex items-center justify-center h-screen">
<form class="flex flex-col w-1/4 space-y-3" action="changepwd" method="POST">
  <a class="text-indigo-500 text-xl" href="${pageContext.request.contextPath}/profile"><< Back</a>
  <h1 class="text-3xl">Change your password</h1>

  <c:if test="${message != null}">
    <span>${message}</span>
  </c:if>

  <label class="block text-grey-darker text-sm font-bold mb-2" for="username">
    <span>Username</span>
    <input class="appearance-none border rounded w-full py-2 px-3 text-grey-darker"
           type="text" id="username" name="username" placeholder="<%=user.getUsername()%>" disabled/>
  </label>

  <label class="block text-grey-darker text-sm font-bold mb-2" for="oldpassword">
    <span>Old password</span>
    <input class="appearance-none border rounded w-full py-2 px-3 text-grey-darker"
           type="password" id="oldpassword" name="oldpassword" placeholder="Old password"/>
  </label>

  <label class="block text-grey-darker text-sm font-bold mb-2" for="newpassword">
    <span>New password</span>
    <input class="appearance-none border rounded w-full py-2 px-3 text-grey-darker"
           type="password" id="newpassword" name="newpassword" placeholder="New password"/>
  </label>

  <label class="block text-grey-darker text-sm font-bold mb-2" for="repassword">
    <span>Re-input your new password</span>
    <input class="appearance-none border rounded w-full py-2 px-3 text-grey-darker"
           type="password" id="repassword" name="repassword" placeholder="Re-input new password"/>
  </label>

  <button class="w-full border bg-blue-700 mt-2 shadow-md rounded-md py-3 font-bold text-white active:bg-blue-900"
          type="submit">Confirm
  </button>
</form>
</body>
</html>
