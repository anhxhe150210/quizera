<%--
  Created by IntelliJ IDEA.
  User: Seth_Etherald
  Date: 7/24/2021
  Time: 7:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

</head>
<body>
<footer class="w-full border-t bg-white pb-12 text-black self-end">
  <div class="w-full container mx-auto flex flex-col items-center">
    <div class="flex flex-col md:flex-row text-center md:text-left md:justify-between py-6">
      <a href="#" class="uppercase px-3">About Us</a>
      <a href="#" class="uppercase px-3">Privacy Policy</a>
      <a href="#" class="uppercase px-3">Terms & Conditions</a>
      <a href="#" class="uppercase px-3">Contact Us</a>
    </div>
    <div class="uppercase pb-6">&copy; Quizera</div>
  </div>
</footer>
</body>
</html>
