<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="z-50 h-14 fixed top-0 flex flex-row w-full justify-between py-2 bg-gray-800 px-10 items-center">
  <div class="h-full flex flex-row">
    <a id="logo"
       class="flex justify-center items-center w-20 bg-gradient-to-tr from-pink-600 to-indigo-300 rounded-md shadow-lg"
       href="${pageContext.request.contextPath}">
      <span>Quizera</span>
    </a>
    <a class="flex justify-center items-center w-20 text-white"
       href="${pageContext.request.contextPath}/blogs">
      <span>Blogs</span>
    </a>

    <a class="flex justify-center items-center w-20 text-white"
       href="${pageContext.request.contextPath}/courses">
      <span>Courses</span>
    </a>
    <div class="dropdown inline-block relative">
      <button
        class="bg-gray-700 hover:bg-gray-600 text-white font-semibold py-2 px-4 rounded inline-flex items-center">
        <span class="mr-1">Category</span>
        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
          <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
        </svg>
      </button>
      <ul class="dropdown-menu absolute hidden text-white pt-1">
        <c:choose>
          <c:when test="${categoryType == 'blogs'}">
            <c:forEach items="${categories}" var="category">
              <li class=""><a class="bg-gray-700 hover:bg-gray-600 py-2 px-4 block whitespace-no-wrap"
                              href="${pageContext.request.contextPath}/blogs?category=${category.id}">
                  ${category.description}
              </a></li>
            </c:forEach>
          </c:when>
          <c:otherwise>
            <c:forEach items="${categories}" var="category">
              <li class=""><a class="bg-gray-700 hover:bg-gray-600 py-2 px-4 block whitespace-no-wrap"
                              href="${pageContext.request.contextPath}/courses?category=${category.id}">
                  ${category.description}
              </a></li>
            </c:forEach>
          </c:otherwise>
        </c:choose>
      </ul>
    </div>
  </div>

  <form class="mb-4 w-full md:mb-0 md:w-1/3"
        action="${pageContext.request.contextPath}/${categoryType == "blogs" ? "blogs" : "courses"}" method="GET">
    <%--      <label class="hidden" for="search-form">Search</label>--%>
    <input class="bg-grey-lightest border-2 focus:border-orange p-2 rounded-lg shadow-inner w-full text-black"
           name="courseName"
           placeholder="Search"
           type="text" value="">
    <button class="hidden">Submit</button>
  </form>


  <div class="h-full flex flex-row space-x-3">
    <c:choose>
      <c:when test="${user != null}">
        <c:if test="${user.getRole() != 'GUEST' && user.getRole() != 'CUSTOMER'}">
          <a href="${pageContext.request.contextPath}/dashboard">
            <div
              class="flex justify-center items-center px-5 h-full inline-block bg-gray-700 text-white font-bold active:bg-gray-900 rounded">
              <span>Dashboard</span>
            </div>
          </a>
        </c:if>
        <a href="${pageContext.request.contextPath}/practices">
          <div
            class="flex justify-center items-center px-5 h-full inline-block bg-gray-700 text-white font-bold active:bg-gray-900 rounded">
            <span>My Practices</span>
          </div>
        </a>
        <a href="${pageContext.request.contextPath}/profile">
          <div
            class="flex justify-center items-center px-5 h-full inline-block bg-gray-700 text-white font-bold active:bg-gray-900 rounded">
            <span>Profile</span>
          </div>
        </a>
        <a href="${pageContext.request.contextPath}/logout">
          <div
            class="flex justify-center items-center px-5 h-full inline-block bg-gray-700 text-white font-bold active:bg-gray-900 rounded">
            <span>Logout</span>
          </div>
        </a>
      </c:when>
      <c:otherwise>
        <a href="${pageContext.request.contextPath}/login">
          <div
            class="flex justify-center items-center w-20 h-full inline-block bg-gray-700 text-white font-bold active:bg-gray-900 rounded">
            <span>Login</span>
          </div>
        </a>
      </c:otherwise>
    </c:choose>
  </div>
</nav>
<style>
    .dropdown:hover .dropdown-menu {
        display: block;
    }
</style>
