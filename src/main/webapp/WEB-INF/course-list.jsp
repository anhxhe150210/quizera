<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 6/20/2021
  Time: 8:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Course list</title>
  <script src="${pageContext.request.contextPath}/styles.js"></script>
</head>
<body class="bg-gray-800 font-sans leading-normal tracking-normal mt-12 flex items-start flex-col justify-center">
<c:import url="topnav.jsp"/>
<div class="flex flex-row items-start w-full">
  <c:import url="sider.jsp"/>
  <div class="flex flex-col md:flex-col mt-12 md:mt-2 pb-24 md:pb-5 w-full items-center">
    <h1 class="w-full my-2 text-4xl font-bold leading-tight text-white">Course list</h1>
    <c:forEach items="${course}" var="listCourse">
      <div class="py-3 w-5/6">
        <div class="flex w-full bg-white rounded-lg overflow-hidden m-auto">
          <div class="max-w-xs w-1/3 bg-cover" style="background-image: url('${listCourse.getThumbnailUrl()}')">
          </div>
          <div class="w-2/3 p-5">
            <h1 class="text-gray-900 font-bold text-2xl"><a
              href="/quizera/course?id=${listCourse.getId()}">${listCourse.getTitle()}</a></h1>
            <p class="mt-2 text-gray-600 text-lg">${listCourse.getDescription()}</p>
            <h3>By ${listCourse.getOwner().getDisplayName()}</h3>
            <div class="flex item-center justify-between mt-3">
              <h1 class="text-gray-700 font-bold text-xl">${listCourse.getLowestPrice()}</h1>
              <button class="py-2 bg-gray-800 text-white text-xs font-bold uppercase rounded"><c:choose>
                <c:when test="${coursesOwnership[listCourse.id]}">
              <span
                class="bg-gray-400 opacity-75 text-yellow-900 rounded px-10 py-2 font-semibold">
                OWNED
              </span>
                </c:when>
                <c:otherwise>
                  <c:url value="/checkout" var="url">
                    <c:param name="courseId" value="${listCourse.id}"/>
                    <c:choose>
                      <c:when test="${category != null}">
                        <c:param name="redirect" value="/courses?category=${category}"/>
                      </c:when>
                      <c:otherwise>
                        <c:param name="redirect" value="/courses"/>
                      </c:otherwise>
                    </c:choose>
                  </c:url>
                  <a href="${url}"
                     class="bg-yellow-300 opacity-75 hover:opacity-100 text-yellow-900 hover:text-gray-900 rounded px-10 py-2 font-semibold">
                    Purchase
                  </a>
                </c:otherwise>
              </c:choose></button>

            </div>
          </div>
        </div>
      </div>

    </c:forEach>
    <div class="flex flex-row gap-3 h-7 mb-5 w-full">
      <span class="text-white">Page</span>
      <%
        String query = (String) request.getAttribute("query");
        int currentPage = (Integer) request.getAttribute("page");
        int maxPage = (Integer) request.getAttribute("maxPage");

        int startOfPageNav = Math.max(1, currentPage - 2);
        for (int thePage = startOfPageNav; thePage <= Math.min(maxPage, startOfPageNav + 4); thePage++) {
          if (thePage == currentPage) {
      %>
      <span class="border rounded-full w-7 text-center hover:bg-blue-700 text-white"><%=thePage%></span>
      <%
          continue;
        }
      %>
      <a
        href="${pageContext.request.contextPath}/courses<%=query%>&page=<%=thePage%>"><%=thePage%>
      </a>
      <%}%>


    </div>


  </div>
</div>
<c:import url="footer.jsp"/>
</body>
</html>
