<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Login</title>
  <script src="${pageContext.request.contextPath}/styles.js"></script>
</head>
<body class="flex items-center justify-center h-screen">
<form class="flex flex-col w-1/4 space-y-3" action="login" method="POST">
  <a class="text-indigo-500 text-xl" href="${pageContext.request.contextPath}/"><< Back</a>

  <h1 class="text-3xl">Login</h1>

  <c:if test="${message != null}">
    <span>${message}</span>
  </c:if>

  <label class="block text-grey-darker text-sm font-bold mb-2" for="username">
    <span>Username</span>
    <input class="appearance-none border rounded w-full py-2 px-3 text-grey-darker"
           type="text" id="username" name="username" placeholder="Username"/>
  </label>

  <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
    <span>Password</span>
    <input class="appearance-none border rounded w-full py-2 px-3 text-grey-darker"
           type="password" id="password" name="password" placeholder="Password"/>
  </label>
  <div class="flex items-center justify-between">
    <div class="flex items-center">
      <input id="remember_me" name="remember_me" type="checkbox"
             class="h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded">
      <label for="remember_me" class="ml-2 block text-sm text-gray-900">
        Remember me
      </label>
    </div>

    <div class="text-sm">
      <a href="resetpwd" class="font-medium text-indigo-600 hover:text-indigo-500">
        Forgot your password?
      </a>
    </div>
  </div>
  <button class="w-full border bg-blue-700 mt-2 shadow-md rounded-md py-3 font-bold text-white active:bg-blue-900"
          type="submit">Login
  </button>
  <p class="text-sm">Doesn't have an account yet? <a class="font-medium text-indigo-600 hover:text-indigo-500"
                                                     href="register">Register</a></p>
</form>
</body>
</html>
