<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Seth_Etherald
  Date: 5/12/2021
  Time: 6:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Register</title>
  <script src="${pageContext.request.contextPath}/styles.js"></script>
</head>
<body class="flex items-center justify-center h-screen">
<form class="flex flex-col w-1/4 space-y-3" action="register" method="POST">
  <h1 class="text-3xl">Create an account</h1>

  <c:if test="${message != null}">
    <span>${message}</span>
  </c:if>

  <label class="block text-grey-darker text-sm font-bold mb-2" for="username">
    <span>Username</span>
    <input class="appearance-none border rounded w-full py-2 px-3 text-grey-darker"
           type="text" id="username" name="username" placeholder="Username"/>
  </label>

  <label class="block text-grey-darker text-sm font-bold mb-2" for="email">
    <span>Email</span>
    <input class="appearance-none border rounded w-full py-2 px-3 text-grey-darker"
           type="text" id="email" name="email" placeholder="Email Address"/>
  </label>

  <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
    <span>Password</span>
    <input class="appearance-none border rounded w-full py-2 px-3 text-grey-darker"
           type="password" id="password" name="password" placeholder="Password"/>
  </label>

  <label class="block text-grey-darker text-sm font-bold mb-2" for="repassword">
    <span>Re-input your password</span>
    <input class="appearance-none border rounded w-full py-2 px-3 text-grey-darker"
           type="password" id="repassword" name="repassword" placeholder="Re-input password"/>
  </label>

  <button class="w-full border bg-blue-700 mt-2 shadow-md rounded-md py-3 font-bold text-white active:bg-blue-900"
          type="submit">Register
  </button>
  <p class="text-sm">Already has an account? <a class="font-medium text-indigo-600 hover:text-indigo-500"
                                                href="login">Login</a></p>
</form>

</body>
</html>
