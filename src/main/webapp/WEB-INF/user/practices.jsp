<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Practices</title>
  <script src="${pageContext.request.contextPath}/styles.js"></script>
</head>
<body>
<c:import url="../topnav.jsp"/>
<div class="p-10">
  <a class="inline-block mt-14 bg-green-500 rounded-md shadow-md px-4 py-2 text-white"
     href="${pageContext.request.contextPath}/practices/new">New Practice</a>

  <div class="mt-6 border-2 border-black rounded">
    <table class="w-full p-20">
      <thead>
      <tr class="h-20 bg-gray-500 text-left text-xl">
        <th class="p-6">Practice</th>
        <th class="p-6">Date</th>
        <th class="p-6">Score</th>
        <th class="p-6">Actions</th>
      </tr>
      </thead>
      <tbody>
      <c:forEach items="${practices}" var="practice">
        <tr class="h-20 text-xl border-b border-gray-500">
          <td class="p-6">${practice.quiz.title}</td>
          <td class="p-6"><fmt:formatDate value="${practice.startAt}" pattern="MM.dd.yyyy"/></td>
          <td class="p-6">${scores.get(practice.id)}%</td>
          <td class="p-6">
            <c:choose>
              <c:when test="${practice.isTimeout()}">
                <a href="${pageContext.request.contextPath}/practices/review?id=${practice.id}">
                  Review
                </a>
              </c:when>
              <c:otherwise>
                <a href="${pageContext.request.contextPath}/practices/handle?id=${practice.id}">
                  Continue
                </a>
              </c:otherwise>
            </c:choose>
          </td>
        </tr>
      </c:forEach>
      </tbody>
    </table>
  </div>
</div>
<c:import url="../footer.jsp"/>
</body>
</html>
