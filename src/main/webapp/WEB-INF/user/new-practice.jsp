<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Practices</title>
  <script src="${pageContext.request.contextPath}/styles.js"></script>
</head>
<body>
<c:import url="../topnav.jsp"/>
<div class="bg-gray-800 pt-3 mt-14">
  <div class="rounded-tl-3xl bg-gradient-to-r from-blue-900 to-gray-800 p-4 shadow text-2xl text-white">
    <h3 class="font-bold pl-2">New Practice</h3>
  </div>
</div>
<a class="inline-block px-4 py-2 bg-red-500 rounded border text-white ml-20 mt-5" href="${pageContext.request.contextPath}/practices">Back</a>
<div class="flex flex-col my-5 mx-20 p-10 border border-black rounded-md gap-4">
  <label for="course">Select a course</label>
  <select id="course" class="rounded">
    <option value="">--- Choose a course ---</option>
    <c:forEach items="${courses}" var="course">
      <option value="${course.id}" ${courseId == course.id ? 'selected' : ''}>${course.title}</option>
    </c:forEach>
  </select>

  <c:if test="${quizzes != null}">
    <label for="quiz">Select a Quiz</label>
    <select id="quiz" class="rounded">
      <c:forEach items="${quizzes}" var="quiz">
        <option value="${quiz.id}">${quiz.title}</option>
      </c:forEach>
    </select>

    <button id="submit" class="bg-blue-700 px-4 py-2 rounded-md shadow-md text-lg text-white">Practice this quiz</button>
  </c:if>
</div>
<c:import url="../footer.jsp"/>
<script>
    const quizSelect = document.querySelector('#quiz')
    if (quizSelect) {
        document.querySelector('#submit').addEventListener('click', ev => {
            window.location.href = '${pageContext.request.contextPath}/practices/add?quizId=' + quizSelect.value
        })
    }

    const courseSelect = document.querySelector('#course')
    courseSelect.addEventListener('change', ev => {
        const id = courseSelect.value
        if (id === '') {
            return
        }
        window.location.replace('${pageContext.request.contextPath}/practices/new?courseId=' + id)
    })
</script>
</body>
</html>
