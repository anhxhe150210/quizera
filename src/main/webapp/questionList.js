/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/
(() => { // webpackBootstrap
    /******/
    var __webpack_modules__ = ({

        /***/ "./js/questionList.js":
        /*!****************************!*\
          !*** ./js/questionList.js ***!
          \****************************/
        /***/ (() => {

            eval("const select = document.getElementById(\"select_course\");\r\n\r\nselect.addEventListener(\"change\", ev => {\r\n    const course = select.value;\r\n    console.log(course)\r\n    if (course == null || course === \"\") {\r\n        return;\r\n    }\r\n    window.location.href = \"${pageContext.request.contextPath}/dashboard/questions?course=\" + select.value;\r\n})\r\n\r\nconst edjsParser = edjsHTML();\r\ndocument.querySelectorAll(\".question\").forEach(e => {\r\n    e.innerHTML = edjsParser.parse(JSON.parse(e.innerHTML));\r\n})\r\n\n\n//# sourceURL=webpack://quizera/./js/questionList.js?");

            /***/
        })

        /******/
    });
    /************************************************************************/
    /******/
    /******/ 	// startup
    /******/ 	// Load entry module and return exports
    /******/ 	// This entry module can't be inlined because the eval devtool is used.
    /******/
    var __webpack_exports__ = {};
    /******/
    __webpack_modules__["./js/questionList.js"]();
    /******/
    /******/
})()
;