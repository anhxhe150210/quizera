package com.t4.quizera.filter;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Category;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebFilter(filterName = "CategoryFilter")
public class CategoryFilter implements Filter {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public CategoryFilter(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  public void init(FilterConfig config) {}

  public void destroy() {}

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws ServletException, IOException {
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    HttpServletResponse httpResponse = (HttpServletResponse) response;

    List<Category> categories = new ArrayList<>();
    String path = httpRequest.getServletPath();
    if (path != null && path.equals("/blogs")) {
      categories.addAll(quizeraDAO.getBlogCategories());
      request.setAttribute("categoryType", "blogs");
    } else {
      categories.addAll(quizeraDAO.getCourseCategories());
      request.setAttribute("categoryType", "courses");
    }
    request.setAttribute("categories", categories);

    chain.doFilter(request, response);
  }
}
