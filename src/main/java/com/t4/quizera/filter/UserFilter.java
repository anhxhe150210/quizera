package com.t4.quizera.filter;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.User;
import com.t4.quizera.util.UidUtils;
import com.t4.quizera.util.Utils;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "UserFilter")
public class UserFilter implements Filter {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public UserFilter(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  public void init(FilterConfig config) {}

  public void destroy() {}

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws ServletException, IOException {
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    HttpServletResponse httpResponse = (HttpServletResponse) response;
    boolean isValid = UidUtils.validate(httpRequest, httpResponse, false);

    HttpSession session = httpRequest.getSession();

    if (isValid) {
      String username = Utils.cookiesToMap(httpRequest.getCookies()).get("username");
      User currentUser = (User) session.getAttribute("user");

      if (currentUser == null || !currentUser.getUsername().equalsIgnoreCase(username)) {
        User user = quizeraDAO.getUser(username);
        if (user == null) { // Not in database
          UidUtils.logout(httpRequest, httpResponse);
          session.removeAttribute("user");
        } else {
          session.setAttribute("user", user);
        }
      }
    } else {
      session.removeAttribute("user");
    }
    chain.doFilter(request, response);
  }
}
