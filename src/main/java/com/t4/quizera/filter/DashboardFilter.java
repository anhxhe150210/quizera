package com.t4.quizera.filter;

import com.t4.quizera.model.Role;
import com.t4.quizera.model.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "DashboardFilter")
public class DashboardFilter implements Filter {
  public void init(FilterConfig config) {}

  public void destroy() {}

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws ServletException, IOException {
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    HttpServletResponse httpResponse = (HttpServletResponse) response;

    // Prohibits customers & guests
    User user = (User) httpRequest.getSession().getAttribute("user");
    if (user == null || user.getRole() == Role.CUSTOMER || user.getRole() == Role.GUEST) {
      httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }

    chain.doFilter(request, response);
  }
}
