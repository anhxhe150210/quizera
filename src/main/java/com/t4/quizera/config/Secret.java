package com.t4.quizera.config;

import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;

public class Secret {
  private static final String secret = "3eVGzpJcojyvh/vDeSDkD8KBGXU7+QdqaCQajteGwMU=";

  public static String getSecret() {
    return secret;
  }

  public static SecretKey getSecretKey() {
    return Keys.hmacShaKeyFor(Decoders.BASE64.decode(secret));
  }
}
