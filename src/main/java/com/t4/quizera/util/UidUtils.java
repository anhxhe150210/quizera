package com.t4.quizera.util;

import com.t4.quizera.config.Secret;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UidUtils {
  public static String getUserPublicKey(String username) {
    return Jwts.builder()
        .setSubject("UserPublicKey")
        .claim("username", username)
        .signWith(Secret.getSecretKey())
        .compact();
  }

  public static boolean verify(String pubKey, String username) {
    try {
      Jwts.parserBuilder()
          .requireSubject("UserPublicKey")
          .require("username", username)
          .setSigningKey(Secret.getSecretKey())
          .build()
          .parseClaimsJws(pubKey);
      return true;
    } catch (JwtException e) {
      return false;
    }
  }

  public static boolean validate(
      HttpServletRequest request, HttpServletResponse response, boolean logout) {
    Cookie usernameCookie = null;
    Cookie authKeyCookie = null;
    Cookie[] cookies = request.getCookies();
    if (cookies == null) {
      return false;
    }
    for (Cookie c : cookies) {
      switch (c.getName()) {
        case "username":
          usernameCookie = c;
          break;
        case "authKey":
          authKeyCookie = c;
          break;
      }
    }

    if (usernameCookie == null || authKeyCookie == null) {
      return false;
    }
    request.getSession().removeAttribute("user");
    if (logout || !UidUtils.verify(authKeyCookie.getValue(), usernameCookie.getValue())) {
      usernameCookie.setMaxAge(0);
      authKeyCookie.setMaxAge(0);
      response.addCookie(usernameCookie);
      response.addCookie(authKeyCookie);
      return false;
    }
    return true;
  }

  public static void logout(HttpServletRequest request, HttpServletResponse response) {
    validate(request, response, true);
  }
}
