package com.t4.quizera.util;

import com.t4.quizera.model.Answer;
import com.t4.quizera.model.Question;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.text.StringEscapeUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class Utils {
  public static void forwardAttributes(
      String target,
      Map<String, Object> attributes,
      HttpServletRequest request,
      HttpServletResponse response)
      throws ServletException, IOException {
    attributes.forEach(request::setAttribute);
    request.getRequestDispatcher(target).forward(request, response);
  }

  public static void includeAttributes(
      String target,
      Map<String, Object> attributes,
      HttpServletRequest request,
      HttpServletResponse response)
      throws ServletException, IOException {
    attributes.forEach(request::setAttribute);
    request.getRequestDispatcher(target).include(request, response);
  }

  public static Map<String, String> cookiesToMap(Cookie[] cookies) {
    Map<String, String> map = new HashMap<>();
    for (Cookie c : cookies) {
      map.put(c.getName(), c.getValue());
    }
    return map;
  }

  public static boolean isEmailValid(String email) {
    return email.matches(
        "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])");
  }

  public static String md5(String s) {
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      md.update(s.getBytes());
      byte[] md5 = md.digest();
      return Hex.encodeHexString(md5);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static String getBaseURL(HttpServletRequest req) {
    StringBuffer url = req.getRequestURL();
    String uri = req.getRequestURI();
    String ctx = req.getContextPath();
    return url.substring(0, url.length() - uri.length() + ctx.length()) + "/";
  }

  public static List<Question> parseQuestions(String csv) {
    return parseQuestions(csv, ';');
  }

  public static List<Question> parseQuestions(String csv, char delimiter) {
    List<Question> list = new ArrayList<>();

    Scanner scan = new Scanner(csv);
    while (scan.hasNextLine()) {
      String line = scan.nextLine();
      String[] parts = line.split(";");

      Question question = new Question();

      if (parts.length < 3) {
        continue;
      }

      question.setQuestion(
          String.format(
              "{\"time\":1622128457663,\"blocks\":[{\"id\":\"OScDdsfVrP\",\"type\":\"paragraph\",\"data\":{\"text\":\"%s\"}}],\"version\":\"2.21.0\"}",
              StringEscapeUtils.escapeJson(parts[0])));

      List<Answer> answers = new ArrayList<>();
      for (int i = 1; i < parts.length; i++) {
        Answer answer = new Answer();
        if (parts[i].charAt(0) == '~') {
          answer.setAnswer(parts[i].substring(1));
          answer.setCorrect(true);
        } else {
          answer.setAnswer(parts[i]);
          answer.setCorrect(false);
        }
        answer.setQuestion(question);
        answers.add(answer);
      }

      question.setAnswers(answers);

      list.add(question);
    }

    return list;
  }
}
