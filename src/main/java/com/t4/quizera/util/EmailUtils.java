package com.t4.quizera.util;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

public class EmailUtils {
  private static final String username = "intagaming@gmail.com";
  private static final String password = "t0XbhqAS4znWPVfJ";
  private static final Session session;

  static {
    Properties prop = new Properties();
    prop.put("mail.smtp.auth", true);
    //        prop.put("mail.smtp.starttls.enable", "true");
    prop.put("mail.smtp.host", "smtp-relay.sendinblue.com");
    prop.put("mail.smtp.port", "587");
    //        prop.put("mail.smtp.ssl.trust", "smtp-relay.sendinblue.com");

    session =
        Session.getInstance(
            prop,
            new Authenticator() {
              @Override
              protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
              }
            });
  }

  public static void send(String to, String[] components) {
    try {
      Message message = new MimeMessage(session);
      message.setFrom(new InternetAddress("intagaming@gmail.com"));
      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
      message.setSubject(components[0]);

      String msg = components[1];

      MimeBodyPart mimeBodyPart = new MimeBodyPart();
      mimeBodyPart.setContent(msg, "text/html");

      Multipart multipart = new MimeMultipart();
      multipart.addBodyPart(mimeBodyPart);

      message.setContent(multipart);

      Transport.send(message);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static String[] registerTemplate(String verifyUrl) {
    String[] components = new String[2];
    components[0] = "Welcome to Quizera!";
    components[1] =
        String.format(
            "<p>\n"
                + "    Thank you for registering on Quizera. We hope to provide you with the best quiz experience while\n"
                + "    you breeze through your learning path. \n"
                + "</p>\n"
                + "\n"
                + "<p>Please verify your account via this link: <a href='%s'>Verify Your Email</a> </p>\n"
                + "\n"
                + "<p>Best regards,<br>Quizera Team</p>",
            verifyUrl);
    return components;
  }

  public static String[] resetPasswordTemplate(String resetPasswordUrl) {
    String[] components = new String[2];
    components[0] = "Reset your Quizera password";
    components[1] =
        String.format(
            "<p>\n"
                + "    You have requested to reset your password on Quizera.\n"
                + "</p>\n"
                + "\n"
                + "<p>Please proceed via this link: <a href='%s'>Reset password</a> </p>\n"
                + "\n"
                + "<p>Best regards,<br>Quizera Team</p>",
            resetPasswordUrl);
    return components;
  }
}
