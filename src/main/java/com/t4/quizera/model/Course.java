package com.t4.quizera.model;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class Course {
  private int id;
  private String title;
  private User owner;
  private String description;
  private Date updatedDate;
  private boolean featured;
  private String thumbnailUrl;
  private boolean archived;
  private boolean published;

  private List<CourseCategory> categories;
  private List<Price> prices;
  private List<Lecture> lectures;
  private List<Quiz> quizzes;
  private List<Question> questions;
  private List<CourseDimension> dimensions;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public User getOwner() {
    return owner;
  }

  public void setOwner(User owner) {
    this.owner = owner;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }

  public boolean isFeatured() {
    return featured;
  }

  public void setFeatured(boolean featured) {
    this.featured = featured;
  }

  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  public void setThumbnailUrl(String thumbnailUrl) {
    this.thumbnailUrl = thumbnailUrl;
  }

  public boolean isArchived() {
    return archived;
  }

  public void setArchived(boolean archived) {
    this.archived = archived;
  }

  public List<CourseCategory> getCategories() {
    return categories;
  }

  public void setCategories(List<CourseCategory> categories) {
    this.categories = categories;
  }

  public List<Price> getPrices() {
    return prices;
  }

  public void setPrices(List<Price> prices) {
    this.prices = prices;
  }

  public List<CourseDimension> getDimensions() {
    return dimensions;
  }

  public void setDimensions(List<CourseDimension> dimensions) {
    this.dimensions = dimensions;
  }

  public String getOwnerRepresentation() {
    if (owner == null) {
      return "Unknown";
    }
    if (owner.getFullName() != null) {
      return owner.getFullName();
    }
    return owner.getUsername();
  }

  public int getNumberOfLessons() {
    return lectures.size();
  }

  public boolean isPublished() {
    return published;
  }

  public void setPublished(boolean published) {
    this.published = published;
  }

  public Price getLowestPrice() {
    return getPrices().stream().min(Comparator.comparing(Price::getPrice)).orElse(null);
  }

  public List<Lecture> getLectures() {
    return lectures;
  }

  public void setLectures(List<Lecture> lectures) {
    this.lectures = lectures;
  }

  public List<Quiz> getQuizzes() {
    return quizzes;
  }

  public void setQuizzes(List<Quiz> quizzes) {
    this.quizzes = quizzes;
  }

  public List<Question> getQuestions() {
    return questions;
  }

  public void setQuestions(List<Question> questions) {
    this.questions = questions;
  }
}
