package com.t4.quizera.model;

public class CourseDimension {
  private int id;
  private Course course;
  private String type;
  private String dimension;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getDimension() {
    return dimension;
  }

  public void setDimension(String dimension) {
    this.dimension = dimension;
  }

  @Override
  public String toString() {
    return type + ": " + dimension;
  }
}
