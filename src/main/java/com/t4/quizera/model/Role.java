package com.t4.quizera.model;

import java.util.Arrays;
import java.util.Optional;

public enum Role {
  GUEST(1),
  CUSTOMER(2),
  MARKETING(3),
  SALE(4),
  EXPERT(5),
  ADMIN(6);

  public final int id;

  Role(int id) {
    this.id = id;
  }

  public static Optional<Role> valueOf(int id) {
    return Arrays.stream(values()).filter(role -> role.id == id).findFirst();
  }

  public String getName() {
    return name().substring(0, 1).toUpperCase() + name().substring(1).toLowerCase();
  }
}
