package com.t4.quizera.model;

import java.util.Arrays;

public enum FilterBy {
  DIMENSION(1),
  TYPE(2);

  int filterNumber;

  FilterBy(int filterNumber) {
    this.filterNumber = filterNumber;
  }

  public static FilterBy of(int filterNumber) {
    return Arrays.stream(values())
        .filter(f -> f.filterNumber == filterNumber)
        .findFirst()
        .orElse(null);
  }

  public int getFilterNumber() {
    return filterNumber;
  }
}
