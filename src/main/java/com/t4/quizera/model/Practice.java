package com.t4.quizera.model;

import java.time.Instant;
import java.util.Date;
import java.util.List;

public class Practice {
  private int id;
  private User user;
  private Quiz quiz;
  private Date startAt;
  private String filterString; // Filtering questions to choose from
  private boolean forceSubmitted;

  private List<PracticeSubmission> submissions; // Questions and answers

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Quiz getQuiz() {
    return quiz;
  }

  public void setQuiz(Quiz quiz) {
    this.quiz = quiz;
  }

  public Date getStartAt() {
    return startAt;
  }

  public void setStartAt(Date startAt) {
    this.startAt = startAt;
  }

  public String getFilterString() {
    return filterString;
  }

  public void setFilterString(String filterString) {
    this.filterString = filterString;
  }

  public List<PracticeSubmission> getSubmissions() {
    return submissions;
  }

  public void setSubmissions(List<PracticeSubmission> submissions) {
    this.submissions = submissions;
  }

  public boolean isTimeout() {
    return isForceSubmitted()
        || getStartAt()
            .toInstant()
            .plusSeconds(getQuiz().getDurationInSeconds())
            .isBefore(Instant.now());
  }

  public boolean isForceSubmitted() {
    return forceSubmitted;
  }

  public void setForceSubmitted(boolean forceSubmitted) {
    this.forceSubmitted = forceSubmitted;
  }
}
