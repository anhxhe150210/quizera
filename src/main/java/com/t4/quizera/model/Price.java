package com.t4.quizera.model;

public class Price {
  private int id;
  private Course course;
  private int days;
  private double price;
  private double saleFrom;

  public Price() {}

  public Price(Course course, int days, double price, double saleFrom) {
    this.course = course;
    this.days = days;
    this.price = price;
    this.saleFrom = saleFrom;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public int getDays() {
    return days;
  }

  public void setDays(int days) {
    this.days = days;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public double getSaleFrom() {
    return saleFrom;
  }

  public void setSaleFrom(double saleFrom) {
    this.saleFrom = saleFrom;
  }

  @Override
  public String toString() {
    return "$" + getPrice();
  }
}
