package com.t4.quizera.model;

import java.util.Date;
import java.util.List;

public class Exam {
  private int id;
  private User user;
  private Course course;
  private Date startAt;
  private Date endAt;
  private int numberOfQuestions;

  private List<ExamSubmission> submissions; // Questions and answers

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public Date getStartAt() {
    return startAt;
  }

  public void setStartAt(Date startAt) {
    this.startAt = startAt;
  }

  public Date getEndAt() {
    return endAt;
  }

  public void setEndAt(Date endAt) {
    this.endAt = endAt;
  }

  public List<ExamSubmission> getSubmissions() {
    return submissions;
  }

  public void setSubmissions(List<ExamSubmission> submissions) {
    this.submissions = submissions;
  }

  public int getNumberOfQuestions() {
    return numberOfQuestions;
  }

  public void setNumberOfQuestions(int numberOfQuestions) {
    this.numberOfQuestions = numberOfQuestions;
  }
}
