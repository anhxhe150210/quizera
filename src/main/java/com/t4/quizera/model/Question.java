package com.t4.quizera.model;

import java.util.List;

public class Question {
  private int id;
  private Course course;
  private String question;
  private List<Answer> answers;
  private CourseDimension dimension;

  public Question() {}

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public List<Answer> getAnswers() {
    return answers;
  }

  public void setAnswers(List<Answer> answers) {
    this.answers = answers;
  }

  public CourseDimension getDimension() {
    return dimension;
  }

  public void setDimension(CourseDimension dimension) {
    this.dimension = dimension;
  }
}
