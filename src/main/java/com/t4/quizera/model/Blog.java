package com.t4.quizera.model;

import java.util.Date;
import java.util.List;

public class Blog {
  private int id;
  private User author;
  private String title;
  private String content;
  private Date publicDate;
  private String thumbnailUrl;

  private List<BlogCategory> categories;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public User getAuthor() {
    return author;
  }

  public void setAuthor(User author) {
    this.author = author;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public Date getPublicDate() {
    return publicDate;
  }

  public void setPublicDate(Date publicDate) {
    this.publicDate = publicDate;
  }

  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  public void setThumbnailUrl(String thumbnailUrl) {
    this.thumbnailUrl = thumbnailUrl;
  }

  public List<BlogCategory> getCategories() {
    return categories;
  }

  public void setCategories(List<BlogCategory> categories) {
    this.categories = categories;
  }
}
