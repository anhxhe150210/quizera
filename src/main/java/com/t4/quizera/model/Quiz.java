package com.t4.quizera.model;

import java.util.List;

public class Quiz {
  private int id;
  private Course course;
  private String title;
  private Level level;
  private int durationInSeconds;
  private int numberOfQuestions;

  private List<QuizQuestionFilter> questionFilters;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Course getCourse() {
    return course;
  }

  public void setCourse(Course course) {
    this.course = course;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Level getLevel() {
    return level;
  }

  public void setLevel(Level level) {
    this.level = level;
  }

  public int getDurationInSeconds() {
    return durationInSeconds;
  }

  public void setDurationInSeconds(int durationInSeconds) {
    this.durationInSeconds = durationInSeconds;
  }

  public int getNumberOfQuestions() {
    return numberOfQuestions;
  }

  public void setNumberOfQuestions(int numberOfQuestions) {
    this.numberOfQuestions = numberOfQuestions;
  }

  public List<QuizQuestionFilter> getQuestionFilters() {
    return questionFilters;
  }

  public void setQuestionFilters(List<QuizQuestionFilter> questionFilters) {
    this.questionFilters = questionFilters;
  }
}
