package com.t4.quizera.model;

public class QuizQuestionFilter {
  private Quiz quiz;
  private FilterBy filterBy;
  private String target;
  private int numberOfQuestions;

  public QuizQuestionFilter() {}

  public QuizQuestionFilter(Quiz quiz, FilterBy filterBy, String target, int numberOfQuestions) {
    this.quiz = quiz;
    this.filterBy = filterBy;
    this.target = target;
    this.numberOfQuestions = numberOfQuestions;
  }

  public Quiz getQuiz() {
    return quiz;
  }

  public void setQuiz(Quiz quiz) {
    this.quiz = quiz;
  }

  public FilterBy getFilterBy() {
    return filterBy;
  }

  public void setFilterBy(FilterBy filterBy) {
    this.filterBy = filterBy;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public int getNumberOfQuestions() {
    return numberOfQuestions;
  }

  public void setNumberOfQuestions(int numberOfQuestions) {
    this.numberOfQuestions = numberOfQuestions;
  }

  public boolean match(Question q) {
    CourseDimension dimension = q.getDimension();
    if ((getFilterBy() == FilterBy.DIMENSION
            && dimension.getDimension().equalsIgnoreCase(getTarget()))
        || (getFilterBy() == FilterBy.TYPE && dimension.getType().equalsIgnoreCase(getTarget()))) {
      return true;
    }
    return false;
  }
}
