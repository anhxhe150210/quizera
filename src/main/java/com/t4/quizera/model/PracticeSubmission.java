package com.t4.quizera.model;

import java.util.List;

public class PracticeSubmission {
  private Practice practice;
  private Question question;
  private List<Answer> answers; // Null means not answered yet

  public Practice getPractice() {
    return practice;
  }

  public void setPractice(Practice practice) {
    this.practice = practice;
  }

  public Question getQuestion() {
    return question;
  }

  public void setQuestion(Question question) {
    this.question = question;
  }

  public List<Answer> getAnswers() {
    return answers;
  }

  public void setAnswers(List<Answer> answers) {
    this.answers = answers;
  }
}
