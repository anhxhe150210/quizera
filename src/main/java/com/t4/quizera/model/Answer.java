package com.t4.quizera.model;

public class Answer {
  private int id;
  private Question question;
  private String answer;
  private boolean correct;
  private String explanation;

  public Answer() {}

  public Answer(Question question, String answer, boolean correct, String explanation) {
    this.question = question;
    this.answer = answer;
    this.correct = correct;
    this.explanation = explanation;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Question getQuestion() {
    return question;
  }

  public void setQuestion(Question question) {
    this.question = question;
  }

  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  public String getExplanation() {
    return explanation;
  }

  public void setExplanation(String explanation) {
    this.explanation = explanation;
  }

  public boolean isCorrect() {
    return correct;
  }

  public void setCorrect(boolean correct) {
    this.correct = correct;
  }
}
