package com.t4.quizera.controller;

import com.t4.quizera.config.Secret;
import com.t4.quizera.dao.QuizeraDAO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "VerifyServlet", value = "/verify")
public class VerifyServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public VerifyServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws IOException {
    String jwt = request.getParameter("code");
    if (jwt == null || jwt.isEmpty()) {
      response.getWriter().println("Code invalid.");
      return;
    }

    Jws<Claims> jws;
    try {
      jws =
          Jwts.parserBuilder()
              .requireSubject("EmailVerification")
              .setSigningKey(Secret.getSecretKey())
              .build()
              .parseClaimsJws(jwt);
    } catch (JwtException e) {
      response.getWriter().println("Code invalid.");
      return;
    }

    String username = (String) jws.getBody().get("username");
    quizeraDAO.verify(username);

    response.sendRedirect(request.getContextPath() + "/login?verify=1");
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {}
}
