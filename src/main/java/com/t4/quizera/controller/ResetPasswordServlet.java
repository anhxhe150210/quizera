package com.t4.quizera.controller;

import com.t4.quizera.config.Secret;
import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.User;
import com.t4.quizera.util.EmailUtils;
import com.t4.quizera.util.Utils;
import io.jsonwebtoken.Jwts;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "ResetPasswordServlet", value = "/resetpwd")
public class ResetPasswordServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public ResetPasswordServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.getRequestDispatcher("/WEB-INF/resetpwd.jsp").forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    String email = request.getParameter("email");

    if (!Utils.isEmailValid(email)) {
      back("Invalid email.", request, response);
      return;
    }

    User user = quizeraDAO.getUserFromEmail(email);
    if (user == null) {
      back("There is no account associated with this email.", request, response);
      return;
    }

    String jwt =
        Jwts.builder()
            .setSubject("ResetPassword")
            .claim("username", user.getUsername())
            .claim("passwordHash", user.getPasswordHash())
            .signWith(Secret.getSecretKey())
            .compact();
    String verifyUrl = Utils.getBaseURL(request) + "confirmresetpwd?code=" + jwt;
    EmailUtils.send(email, EmailUtils.resetPasswordTemplate(verifyUrl));

    response.sendRedirect(request.getContextPath() + "/login?changed=0");
  }

  private void back(String message, HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    Map<String, Object> attrs = new HashMap<>();
    attrs.put("message", message);
    Utils.forwardAttributes("/WEB-INF/resetpwd.jsp", attrs, request, response);
  }
}
