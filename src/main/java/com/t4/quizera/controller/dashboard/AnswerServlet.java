package com.t4.quizera.controller.dashboard;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Answer;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AnswerServlet", value = "/dashboard/answers/*")
public class AnswerServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public AnswerServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null) {
      return;
    }
    switch (path) {
      case "/add":
        add(request, response);
        return;
      case "/delete":
        delete(request, response);
        return;
    }
  }

  protected void add(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int question = Integer.parseInt(request.getParameter("question"));

    Answer answer = new Answer();
    answer.setQuestion(quizeraDAO.getQuestion(question));
    answer.setAnswer("New answer");
    quizeraDAO.addAnswer(answer);

    response.sendRedirect(
        request.getContextPath() + String.format("/dashboard/questions/update?id=%s", question));
  }

  protected void delete(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));

    Answer answer = quizeraDAO.getAnswer(id);

    quizeraDAO.removeAnswer(id);

    response.sendRedirect(
        request.getContextPath()
            + String.format("/dashboard/questions/update?id=%s", answer.getQuestion().getId()));
  }
}
