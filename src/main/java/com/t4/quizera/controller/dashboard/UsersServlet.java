package com.t4.quizera.controller.dashboard;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Role;
import com.t4.quizera.model.User;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet(name = "UsersServlet", value = "/dashboard/users/*")
public class UsersServlet extends HttpServlet {
  private static final Integer ENTRIES_PER_PAGE = 10;
  private final QuizeraDAO quizeraDAO;

  @Inject
  public UsersServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null || path.equals("/list")) {
      listUsers(request, response);
      return;
    }
    switch (path) {
      case "/update":
        showUserUpdatePage(request, response);
        return;
      case "/delete":
        deleteUser(request, response);
        return;
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null) {
      return;
    }
    switch (path) {
      case "/update":
        updateUser(request, response);
        return;
    }
  }

  private void listUsers(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String filter = request.getParameter("filter");
    String filterType = request.getParameter("filter-type");

    String pageInput = request.getParameter("page");
    int page = 1;
    if (pageInput != null) {
      try {
        page = Integer.parseInt(pageInput);
        if (page < 1) {
          page = 1;
        }
      } catch (NumberFormatException ignored) {
      }
    }
    request.setAttribute("page", page);

    if (filter == null || filterType == null) {
      filter = null;
      filterType = null;
    } else {
      filter = filter.trim();
      filterType = filterType.trim();
    }

    List<User> users;
    if (filterType == null) {
      users = quizeraDAO.getUsers(page);
    } else {
      switch (filterType) {
        case "username":
          users = quizeraDAO.getUsersByUsername(filter, page, ENTRIES_PER_PAGE);
          break;
        case "email":
          users = quizeraDAO.getUsersByEmail(filter, page, ENTRIES_PER_PAGE);
          break;
        default:
          users = quizeraDAO.getUsers(page, ENTRIES_PER_PAGE);
          break;
      }
    }

    request.setAttribute("users", users);

    Map<String, String> queries = new HashMap<>();

    if (filter != null) {
      queries.put("filter", filter);
      queries.put(filterType, filterType);
    }

    String query =
        "?"
            + queries.entrySet().stream()
                .map(e -> e.getKey() + "=" + e.getValue())
                .collect(Collectors.joining("&"));

    request.setAttribute("query", query);
    request.setAttribute("maxPage", quizeraDAO.getTotalUsers() / ENTRIES_PER_PAGE + 1);
    request.setAttribute("filter", filter);

    request.setAttribute("pane", "users");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  public void showUserUpdatePage(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String username = request.getParameter("username");
    if (username == null) {
      throw new ServletException("Username not provided.");
    }

    User user = quizeraDAO.getUser(username);
    if (user == null) { // User not exists when clicking edit
      response.sendRedirect(request.getContextPath() + "/dashboard/users");
      return;
    }
    request.setAttribute("theUser", user);

    request.setAttribute("pane", "user-edit");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  public void updateUser(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String role = request.getParameter("role");
    String fullName = request.getParameter("fullName");
    String gender = request.getParameter("gender");
    String mobile = request.getParameter("mobile");
    String avatarUrl = request.getParameter("avatarUrl");

    User user = quizeraDAO.getUser(request.getParameter("username"));
    if (role != null) {
      user.setRole(Role.valueOf(role));
    }
    user.setFullName(fullName);
    try {
      user.setGender(quizeraDAO.getGender(Integer.parseInt(gender)));
    } catch (Exception ignored) {
    }
    user.setMobile(mobile);
    user.setAvatarUrl(avatarUrl);

    quizeraDAO.updateUser(user);

    request.setAttribute("theUser", user);

    request.setAttribute("pane", "user-edit");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  public void deleteUser(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String username = request.getParameter("username");
    if (username == null) {
      throw new ServletException("Username not provided.");
    }

    User user = quizeraDAO.getUser(username);
    if (user == null) { // User not exists when clicking edit
      response.sendRedirect(request.getContextPath() + "/dashboard/users");
      return;
    }

    quizeraDAO.remove(username);

    response.sendRedirect(request.getContextPath() + "/dashboard/users");
  }
}
