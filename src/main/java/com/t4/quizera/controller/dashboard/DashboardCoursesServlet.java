package com.t4.quizera.controller.dashboard;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Course;
import com.t4.quizera.model.Role;
import com.t4.quizera.model.User;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "DashboardCoursesServlet", value = "/dashboard/courses/*")
public class DashboardCoursesServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public DashboardCoursesServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null || path.equals("/list")) {
      listCourses(request, response);
      return;
    }
    switch (path) {
      case "/delete":
        deleteCourse(request, response);
        return;
      case "/add":
        addCourseForm(request, response);
        return;
      case "/update":
        updateCourseForm(request, response);
        return;
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null) {
      return;
    }
    switch (path) {
      case "/add":
        addCourse(request, response);
        return;
      case "/update":
        updateCourse(request, response);
        return;
      case "/list":
        listCoursesFilter(request, response);
        return;
    }
  }

  protected void listCourses(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");
    request.setAttribute(
        "courses",
        quizeraDAO.getCourses().stream()
            .filter(
                c ->
                    user.getRole() == Role.ADMIN
                        || c.getOwner().getUsername().equals(user.getUsername()))
            .collect(Collectors.toList()));
    request.setAttribute("categories", quizeraDAO.getCourseCategories());

    request.setAttribute("pane", "courses");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  protected void listCoursesFilter(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");
    List<Integer> categories;
    if (request.getParameterValues("categories") != null) {
      categories =
          Arrays.stream(request.getParameterValues("categories"))
              .map(Integer::parseInt)
              .collect(Collectors.toList());
    } else {
      categories = new ArrayList<>();
    }
    String visibility = request.getParameter("visibility");
    String name = request.getParameter("name");

    request.setAttribute(
        "courses",
        quizeraDAO.getCourses().stream()
            .filter(
                c ->
                    (user.getRole() == Role.ADMIN
                            || c.getOwner().getUsername().equals(user.getUsername()))
                        && (categories.isEmpty()
                            || c.getCategories().stream()
                                .anyMatch(cat -> categories.contains(cat.getId())))
                        && (visibility == null
                            || visibility.equals("all")
                            || c.isArchived() == visibility.equals("hidden"))
                        && (name.isEmpty() || c.getTitle().contains(name)))
            .collect(Collectors.toList()));

    request.setAttribute("categories", quizeraDAO.getCourseCategories());

    request.setAttribute("pane", "courses");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  protected void updateCourseForm(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");
    int id = Integer.parseInt(request.getParameter("id"));

    Course course = quizeraDAO.getCourse(id);
    if (user.getRole() != Role.ADMIN
        && !course.getOwner().getUsername().equals(user.getUsername())) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }
    request.setAttribute("course", course);

    request.setAttribute("pane", "edit-course");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  protected void updateCourse(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));

    String title = request.getParameter("title");
    String description = request.getParameter("description");
    String thumbnailUrl = request.getParameter("thumbnailUrl");
    boolean isFeatured = request.getParameter("featured") != null;
    boolean isArchived = request.getParameter("archived") != null;
    boolean isPublished = request.getParameter("published") != null;

    Course course = quizeraDAO.getCourse(id);
    course.setTitle(title);
    course.setDescription(description);
    course.setFeatured(isFeatured);
    course.setArchived(isArchived);
    course.setPublished(isPublished);
    course.setThumbnailUrl(thumbnailUrl);

    quizeraDAO.updateCourse(course);

    response.sendRedirect(request.getContextPath() + "/dashboard/courses/update?id=" + id);
  }

  protected void addCourseForm(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setAttribute("pane", "new-course");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  protected void addCourse(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");

    String title = request.getParameter("title");
    String description = request.getParameter("description");
    String thumbnailUrl = request.getParameter("thumbnailUrl");
    boolean isFeatured = request.getParameter("featured") != null;
    boolean isArchived = request.getParameter("archived") != null;
    boolean isPublished = request.getParameter("published") != null;

    Course course = new Course();
    course.setTitle(title);
    course.setOwner(user);
    course.setDescription(description);
    course.setFeatured(isFeatured);
    course.setArchived(isArchived);
    course.setPublished(isPublished);
    course.setThumbnailUrl(thumbnailUrl);

    quizeraDAO.newCourse(course);

    response.sendRedirect(request.getContextPath() + "/dashboard/courses");
  }

  protected void deleteCourse(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));

    Course course = quizeraDAO.getCourse(id);

    quizeraDAO.deleteCourse(course.getId());

    response.sendRedirect(request.getContextPath() + "/dashboard/courses");
  }
}
