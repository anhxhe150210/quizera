package com.t4.quizera.controller.dashboard;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.*;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "QuizzesServlet", value = "/dashboard/quizzes/*")
public class QuizzesServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public QuizzesServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null || path.equals("/list")) {
      list(request, response);
      return;
    }
    switch (path) {
      case "/add":
        add(request, response);
        return;
      case "/update":
        updateForm(request, response);
        return;
      case "/delete":
        delete(request, response);
        return;
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null) {
      return;
    }
    switch (path) {
      case "/update":
        update(request, response);
        return;
    }
  }

  private void list(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");

    String courseId = request.getParameter("id");
    if (courseId != null) {
      Course course = quizeraDAO.getCourse(Integer.parseInt(courseId));
      if (user.getRole() != Role.ADMIN
          && !course.getOwner().getUsername().equals(user.getUsername())) {
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
        return;
      }

      request.setAttribute("quizzes", course.getQuizzes());
      request.setAttribute("course", course);
    }

    request.setAttribute(
        "courses",
        quizeraDAO.getCourses().stream()
            .filter(
                c ->
                    (user.getRole() == Role.ADMIN
                        || c.getOwner().getUsername().equals(user.getUsername())))
            .collect(Collectors.toList()));

    request.setAttribute("pane", "quizzes");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  private void add(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String chapterId = request.getParameter("id");

    quizeraDAO.insertQuiz(Integer.parseInt(chapterId));

    response.sendRedirect(request.getContextPath() + "/dashboard/quizzes?id=" + chapterId);
  }

  private void delete(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String id = request.getParameter("id");

    Quiz quiz = quizeraDAO.getQuiz(Integer.parseInt(id));

    quizeraDAO.deleteQuiz(quiz);

    response.sendRedirect(
        request.getContextPath() + "/dashboard/quizzes?id=" + quiz.getCourse().getId());
  }

  private void updateForm(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String id = request.getParameter("id");

    Quiz quiz = quizeraDAO.getQuiz(Integer.parseInt(id));

    request.setAttribute("quiz", quiz);

    List<CourseDimension> dimensions = quiz.getCourse().getDimensions();
    request.setAttribute(
        "dimensions",
        dimensions.stream().map(CourseDimension::getDimension).collect(Collectors.joining(",")));
    request.setAttribute(
        "types",
        dimensions.stream().map(CourseDimension::getType).collect(Collectors.joining(",")));

    request.setAttribute("pane", "edit-quiz");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  private void update(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String id = request.getParameter("id");
    String title = request.getParameter("title");
    String levelId = request.getParameter("levelId");
    String durationInSeconds = request.getParameter("durationInSeconds");
    String numberOfQuestions = request.getParameter("numberOfQuestions");

    Quiz quiz = quizeraDAO.getQuiz(Integer.parseInt(id));
    FilterBy filterBy = FilterBy.of(Integer.parseInt(request.getParameter("filterBy")));

    List<QuizQuestionFilter> filters = new ArrayList<>();
    int i = 1;
    while (true) {
      String filter = request.getParameter("filter_" + i);
      if (filter == null) {
        break;
      }
      int questionCount = Integer.parseInt(request.getParameter("filter_" + i + "_number"));

      filters.add(new QuizQuestionFilter(quiz, filterBy, filter, questionCount));
      i++;
    }

    quiz.setTitle(title);
    quiz.setLevel(quizeraDAO.getQuizLevel(Integer.parseInt(levelId)));
    quiz.setDurationInSeconds(Integer.parseInt(durationInSeconds));
    quiz.setNumberOfQuestions(Integer.parseInt(numberOfQuestions));
    quiz.setQuestionFilters(filters);

    quizeraDAO.updateQuiz(quiz);

    response.sendRedirect(request.getContextPath() + "/dashboard/quizzes/update?id=" + id);
  }
}
