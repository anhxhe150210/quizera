package com.t4.quizera.controller.dashboard;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Course;
import com.t4.quizera.model.Price;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "CoursePricesServlet", value = "/dashboard/courses/prices/*")
public class CoursePricesServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public CoursePricesServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null || path.equals("/list")) {
      listPrices(request, response);
      return;
    }
    switch (path) {
      case "/update":
        showUpdatePricePage(request, response);
        return;
      case "/delete":
        delete(request, response);
        return;
      case "/add":
        add(request, response);
        return;
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null) {
      return;
    }
    switch (path) {
      case "/update":
        update(request, response);
        return;
    }
  }

  protected void listPrices(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));

    Course course = quizeraDAO.getCourse(id);
    request.setAttribute("course", course);

    request.setAttribute("prices", course.getPrices());

    request.setAttribute("pane", "prices");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  protected void showUpdatePricePage(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));

    Price price = quizeraDAO.getPrice(id);
    request.setAttribute("price", price);

    request.setAttribute("pane", "edit-price");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  protected void update(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));
    Price price = quizeraDAO.getPrice(id);

    int days = Integer.parseInt(request.getParameter("days"));
    double listPrice = Double.parseDouble(request.getParameter("listPrice"));
    double salePrice = Double.parseDouble(request.getParameter("salePrice"));

    price.setDays(days);
    price.setSaleFrom(listPrice);
    price.setPrice(salePrice);

    quizeraDAO.updatePrice(price);

    response.sendRedirect(
        request.getContextPath() + "/dashboard/courses/prices?id=" + price.getCourse().getId());
  }

  protected void delete(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));
    Price price = quizeraDAO.getPrice(id);

    quizeraDAO.deletePrice(id);

    response.sendRedirect(
        request.getContextPath() + "/dashboard/courses/prices?id=" + price.getCourse().getId());
  }

  protected void add(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));
    Course course = quizeraDAO.getCourse(id);

    quizeraDAO.insertPrice(new Price(course, 30, 12.3, 12.3));

    response.sendRedirect(
        request.getContextPath() + "/dashboard/courses/prices?id=" + course.getId());
  }
}
