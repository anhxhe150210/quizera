package com.t4.quizera.controller.dashboard;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Course;
import com.t4.quizera.model.Lecture;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LecturesServlet", value = "/dashboard/lectures/*")
public class LecturesServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public LecturesServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null || path.equals("/list")) {
      list(request, response);
      return;
    }
    switch (path) {
      case "/add":
        add(request, response);
        return;
      case "/update":
        updateForm(request, response);
        return;
      case "/delete":
        delete(request, response);
        return;
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null) {
      return;
    }
    switch (path) {
      case "/update":
        update(request, response);
        return;
    }
  }

  private void list(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String courseId = request.getParameter("id");
    Course course = quizeraDAO.getCourse(Integer.parseInt(courseId));

    request.setAttribute("lectures", course.getLectures());
    request.setAttribute("course", course);

    request.setAttribute("pane", "lectures");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  private void add(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String courseId = request.getParameter("id");

    quizeraDAO.insertLecture(Integer.parseInt(courseId));

    response.sendRedirect(request.getContextPath() + "/dashboard/lectures?id=" + courseId);
  }

  private void delete(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String id = request.getParameter("id");

    Lecture lecture = quizeraDAO.getLecture(Integer.parseInt(id));

    quizeraDAO.deleteLecture(Integer.parseInt(id));

    response.sendRedirect(
        request.getContextPath() + "/dashboard/lectures?id=" + lecture.getCourse().getId());
  }

  private void updateForm(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String id = request.getParameter("id");

    Lecture lecture = quizeraDAO.getLecture(Integer.parseInt(id));

    request.setAttribute("lecture", lecture);
    request.setAttribute("pane", "edit-lecture");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  private void update(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String id = request.getParameter("id");
    String title = request.getParameter("title");
    String content = request.getParameter("content");

    Lecture lecture = quizeraDAO.getLecture(Integer.parseInt(id));

    lecture.setTitle(title);
    lecture.setContent(content);
    quizeraDAO.updateLecture(lecture);

    response.sendRedirect(request.getContextPath() + "/dashboard/lectures/update?id=" + id);
  }
}
