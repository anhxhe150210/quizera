package com.t4.quizera.controller.dashboard;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.*;
import com.t4.quizera.util.Utils;
import org.apache.commons.text.StringEscapeUtils;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "QuestionsServlet", value = "/dashboard/questions/*")
public class QuestionsServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public QuestionsServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null || path.equals("/list")) {
      list(request, response);
      return;
    }
    switch (path) {
      case "/add":
        add(request, response);
        return;
      case "/update":
        updateForm(request, response);
        return;
      case "/delete":
        delete(request, response);
        return;
      case "/import":
        importForm(request, response);
        return;
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null) {
      return;
    }
    switch (path) {
      case "/update":
        update(request, response);
        return;
      case "/import":
        importHandle(request, response);
        return;
    }
  }

  protected void list(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");

    request.setAttribute(
        "courses",
        quizeraDAO.getCourses().stream()
            .filter(
                c ->
                    (user.getRole() == Role.ADMIN
                        || c.getOwner().getUsername().equals(user.getUsername())))
            .collect(Collectors.toList()));

    String courseId = request.getParameter("course");

    if (courseId != null) {
      Course course = quizeraDAO.getCourse(Integer.parseInt(courseId));
      if (course != null) {
        if (user.getRole() != Role.ADMIN
            && !course.getOwner().getUsername().equals(user.getUsername())) {
          response.sendError(HttpServletResponse.SC_FORBIDDEN);
          return;
        }
        request.setAttribute("course", course);
        request.setAttribute("questions", course.getQuestions());
      }
    }

    request.setAttribute("pane", "questions");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  protected void updateForm(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));

    Question question = quizeraDAO.getQuestion(id);
    request.setAttribute("question", question);
    request.setAttribute("questionEscaped", StringEscapeUtils.escapeHtml4(question.getQuestion()));
    request.setAttribute("dimensions", question.getCourse().getDimensions());

    request.setAttribute("pane", "edit-question");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  protected void update(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int questionId = Integer.parseInt(request.getParameter("questionId"));
    Question question = quizeraDAO.getQuestion(questionId);

    String newQuestion = request.getParameter("question");
    question.setQuestion(newQuestion);

    String dimensionId = request.getParameter("dimension");
    if (dimensionId != null && !dimensionId.isEmpty()) {
      question.setDimension(quizeraDAO.getCourseDimension(Integer.parseInt(dimensionId)));
    }

    for (Answer a : question.getAnswers()) {
      String editedAnswer = request.getParameter("answer_" + a.getId());
      String editedExplanation = request.getParameter("answer_explanation_" + a.getId());
      String editedCorrectness = request.getParameter("answer_correct_" + a.getId());
      if (editedAnswer == null || editedExplanation == null) {
        // TODO
        return;
      }
      editedExplanation = editedExplanation.trim();
      a.setAnswer(editedAnswer);
      a.setExplanation(editedExplanation.isEmpty() ? null : editedExplanation);
      a.setCorrect(editedCorrectness != null);
    }

    quizeraDAO.updateQuestion(question);

    response.sendRedirect(
        request.getContextPath() + String.format("/dashboard/questions/update?id=%s", questionId));
  }

  protected void delete(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));

    Question question = quizeraDAO.getQuestion(id);

    quizeraDAO.removeQuestion(id);

    response.sendRedirect(
        request.getContextPath()
            + String.format("/dashboard/questions?course=%s", question.getCourse().getId()));
  }

  protected void importForm(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int courseId = Integer.parseInt(request.getParameter("courseId"));
    request.setAttribute("courseId", courseId);

    request.setAttribute("pane", "import-questions");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  protected void importHandle(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String csv = request.getParameter("csv");
    int courseId = Integer.parseInt(request.getParameter("courseId"));

    Course course = quizeraDAO.getCourse(courseId);

    List<Question> questions = Utils.parseQuestions(csv);
    for (Question q : questions) {
      q.setCourse(course);
      quizeraDAO.addQuestion(q);
    }

    response.sendRedirect(
        request.getContextPath() + String.format("/dashboard/questions?course=%s", courseId));
  }

  protected void add(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int courseId = Integer.parseInt(request.getParameter("courseId"));
    Course course = quizeraDAO.getCourse(courseId);

    Question q = new Question();
    q.setQuestion(
        "{\"time\":1622127161091,\"blocks\":[{\"id\":\"bXDyDgMpnJ\",\"type\":\"header\",\"data\":{\"text\":\"Example question title\",\"level\":1}},{\"id\":\"CaokFS_q-_\",\"type\":\"image\",\"data\":{\"url\":\"https://helpx.adobe.com/content/dam/help/en/photoshop/using/convert-color-image-black-white/jcr_content/main-pars/before_and_after/image-before/Landscape-Color.jpg\",\"caption\":\"Figure 1. Example image\",\"withBorder\":false,\"withBackground\":false,\"stretched\":false}},{\"id\":\"LfqegYmx2a\",\"type\":\"paragraph\",\"data\":{\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse egestas ullamcorper tortor at convallis. Etiam nec volutpat orci. Sed tincidunt euismod libero id ullamcorper. Vivamus mattis lectus vel odio egestas scelerisque. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam pellentesque lobortis convallis. Sed dictum, eros in fringilla luctus, nisl dolor posuere velit, sit amet sagittis tellus orci id lacus. Morbi viverra sagittis lacus, sed hendrerit mauris bibendum nec. Phasellus libero sem, accumsan ac turpis eu, viverra maximus nulla. Sed eget volutpat leo. Etiam vitae facilisis orci. Nam accumsan quam nec nunc vulputate porta. Integer luctus orci id ante pulvinar tincidunt.\"}},{\"id\":\"F_U-HXnaOG\",\"type\":\"paragraph\",\"data\":{\"text\":\"Cras quis tellus lobortis, gravida dui nec, consequat lectus. Vivamus lacinia turpis sed sodales feugiat. Nulla tincidunt sed lectus eu imperdiet. Fusce quis efficitur mi. Integer tincidunt vulputate quam, sit amet finibus neque maximus eget. Quisque rutrum mauris non tincidunt pretium. Morbi eu feugiat sapien, id semper arcu. Praesent quis scelerisque nisl, dictum finibus purus. Etiam id risus rhoncus, laoreet diam in, pellentesque nisi. Nunc porttitor scelerisque est, eu aliquam lorem convallis eget. Mauris vel consectetur velit. Phasellus non cursus augue, eu sollicitudin nisi. Morbi eu nunc at neque malesuada accumsan et vitae ex. Aenean tortor dolor, pellentesque sit amet sollicitudin a, pellentesque accumsan diam. Aliquam tortor turpis, mollis quis lectus at, ultricies interdum velit.\"}}],\"version\":\"2.21.0\"}");
    q.setCourse(course);

    List<Answer> answers = new ArrayList<>();
    answers.add(new Answer(q, "Incorrect answer", false, "false answer"));
    answers.add(new Answer(q, "Correct answer", true, "true answer"));
    q.setAnswers(answers);

    quizeraDAO.addQuestion(q);
    response.sendRedirect(request.getContextPath() + "/dashboard/questions?course=" + courseId);
  }
}
