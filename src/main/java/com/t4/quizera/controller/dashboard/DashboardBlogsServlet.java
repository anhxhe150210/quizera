package com.t4.quizera.controller.dashboard;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Blog;
import com.t4.quizera.model.BlogCategory;
import com.t4.quizera.model.Category;
import com.t4.quizera.model.User;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "DashboardBlogsServlet", value = "/dashboard/blogs/*")
public class DashboardBlogsServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public DashboardBlogsServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null || path.equals("/list")) {
      list(request, response);
      return;
    }
    switch (path) {
      case "/delete":
        delete(request, response);
        return;
      case "/add":
        addForm(request, response);
        return;
      case "/update":
        updateForm(request, response);
        return;
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null) {
      return;
    }
    switch (path) {
      case "/add":
        add(request, response);
        return;
      case "/update":
        update(request, response);
        return;
    }
  }

  protected void list(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setAttribute("blogs", quizeraDAO.getBlogs());

    request.setAttribute("pane", "blogs");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  protected void updateForm(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));

    Blog blog = quizeraDAO.getBlog(id);
    request.setAttribute("blog", blog);

    request.setAttribute("categories", quizeraDAO.getBlogCategories());
    request.setAttribute(
        "blogCategories",
        blog.getCategories().stream().map(Category::getId).collect(Collectors.toList()));

    request.setAttribute("pane", "edit-blog");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  protected void update(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));

    String author = request.getParameter("author");
    String title = request.getParameter("title");
    String content = request.getParameter("content");
    String thumbnailUrl = request.getParameter("thumbnailUrl");

    Blog blog = quizeraDAO.getBlog(id);
    blog.setAuthor(quizeraDAO.getUser(author));
    blog.setTitle(title);
    blog.setContent(content);
    blog.setThumbnailUrl(thumbnailUrl);

    List<Integer> categories;
    if (request.getParameterValues("categories") != null) {
      categories =
          Arrays.stream(request.getParameterValues("categories"))
              .map(Integer::parseInt)
              .collect(Collectors.toList());
    } else {
      categories = new ArrayList<>();
    }

    List<BlogCategory> collect =
        quizeraDAO.getBlogCategories().stream()
            .filter(c -> categories.contains(c.getId()))
            .collect(Collectors.toList());
    blog.setCategories(collect);

    quizeraDAO.updateBlog(blog);

    response.sendRedirect(request.getContextPath() + "/dashboard/blogs/update?id=" + id);
  }

  protected void addForm(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setAttribute("pane", "new-blog");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  protected void add(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");

    String title = request.getParameter("title");
    String content = request.getParameter("content");
    String thumbnailUrl = request.getParameter("thumbnailUrl");

    Blog blog = new Blog();
    blog.setAuthor(user);
    blog.setTitle(title);
    blog.setPublicDate(Date.from(Instant.now()));
    blog.setContent(content);
    blog.setThumbnailUrl(thumbnailUrl);

    quizeraDAO.newBlog(blog);

    response.sendRedirect(request.getContextPath() + "/dashboard/blogs");
  }

  protected void delete(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));

    quizeraDAO.deleteBlog(id);

    response.sendRedirect(request.getContextPath() + "/dashboard/blogs");
  }
}
