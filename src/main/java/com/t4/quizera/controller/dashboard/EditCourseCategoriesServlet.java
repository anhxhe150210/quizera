package com.t4.quizera.controller.dashboard;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Course;
import com.t4.quizera.model.CourseCategory;
import com.t4.quizera.model.Role;
import com.t4.quizera.model.User;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "EditCourseCategoriesServlet", value = "/dashboard/courses/editCategories")
public class EditCourseCategoriesServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public EditCourseCategoriesServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");
    int id = Integer.parseInt(request.getParameter("id"));

    Course course = quizeraDAO.getCourse(id);
    if (user.getRole() != Role.ADMIN
        && !course.getOwner().getUsername().equals(user.getUsername())) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }
    request.setAttribute("course", course);

    request.setAttribute("categories", quizeraDAO.getCourseCategories());
    request.setAttribute(
        "courseCategories",
        course.getCategories().stream().map(CourseCategory::getId).collect(Collectors.toList()));

    request.setAttribute("pane", "edit-course-categories");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int id = Integer.parseInt(request.getParameter("id"));

    List<Integer> categories;
    if (request.getParameterValues("categories") != null) {
      categories =
          Arrays.stream(request.getParameterValues("categories"))
              .map(Integer::parseInt)
              .collect(Collectors.toList());
    } else {
      categories = new ArrayList<>();
    }

    List<CourseCategory> collect =
        quizeraDAO.getCourseCategories().stream()
            .filter(c -> categories.contains(c.getId()))
            .collect(Collectors.toList());

    Course course = quizeraDAO.getCourse(id);
    course.setCategories(collect);

    quizeraDAO.updateCourseCategories(course);

    response.sendRedirect(request.getContextPath() + "/dashboard/courses/editCategories?id=" + id);
  }
}
