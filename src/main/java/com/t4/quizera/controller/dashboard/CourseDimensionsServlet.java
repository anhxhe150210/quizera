package com.t4.quizera.controller.dashboard;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Course;
import com.t4.quizera.model.CourseDimension;
import com.t4.quizera.model.Role;
import com.t4.quizera.model.User;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "CourseDimensionsServlet", value = "/dashboard/courses/dimensions/*")
public class CourseDimensionsServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public CourseDimensionsServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null || path.equals("/list")) {
      listDimensions(request, response);
      return;
    }
    switch (path) {
      case "/add":
        addDimension(request, response);
        return;
      case "/update":
        updateDimensionForm(request, response);
        return;
      case "/delete":
        deleteDimension(request, response);
        return;
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null) {
      return;
    }
    switch (path) {
      case "/update":
        updateDimension(request, response);
        return;
    }
  }

  private void listDimensions(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    User user = (User) request.getSession().getAttribute("user");
    int id = Integer.parseInt(request.getParameter("id"));

    Course course = quizeraDAO.getCourse(id);
    if (user.getRole() != Role.ADMIN
        && !course.getOwner().getUsername().equals(user.getUsername())) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }
    request.setAttribute("course", course);

    request.setAttribute("dimensions", quizeraDAO.getCourseDimensions(course));

    request.setAttribute("pane", "course-dimensions");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  private void updateDimensionForm(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    User user = (User) request.getSession().getAttribute("user");
    int id = Integer.parseInt(request.getParameter("id"));

    CourseDimension courseDimension = quizeraDAO.getCourseDimension(id);
    Course course = courseDimension.getCourse();
    if (user.getRole() != Role.ADMIN
        && !course.getOwner().getUsername().equals(user.getUsername())) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }

    request.setAttribute("dimension", courseDimension);

    request.setAttribute("pane", "edit-course-dimension");
    request.getRequestDispatcher("/WEB-INF/dashboard/dashboard.jsp").forward(request, response);
  }

  private void updateDimension(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    String id = request.getParameter("id");
    String type = request.getParameter("type");
    String dimension = request.getParameter("dimension");

    CourseDimension courseDimension = quizeraDAO.getCourseDimension(Integer.parseInt(id));
    courseDimension.setType(type);
    courseDimension.setDimension(dimension);

    quizeraDAO.updateCourseDimension(courseDimension);

    response.sendRedirect(
        request.getContextPath()
            + "/dashboard/courses/dimensions/list?id="
            + courseDimension.getCourse().getId());
  }

  private void deleteDimension(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    User user = (User) request.getSession().getAttribute("user");
    int id = Integer.parseInt(request.getParameter("id"));

    CourseDimension courseDimension = quizeraDAO.getCourseDimension(id);
    Course course = courseDimension.getCourse();
    if (user.getRole() != Role.ADMIN
        && !course.getOwner().getUsername().equals(user.getUsername())) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }

    quizeraDAO.deleteCourseDimension(id);

    response.sendRedirect(
        request.getContextPath() + "/dashboard/courses/dimensions/list?id=" + course.getId());
  }

  private void addDimension(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    User user = (User) request.getSession().getAttribute("user");
    int courseId = Integer.parseInt(request.getParameter("courseId"));

    Course course = quizeraDAO.getCourse(courseId);
    if (user.getRole() != Role.ADMIN
        && !course.getOwner().getUsername().equals(user.getUsername())) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }
    CourseDimension dimension = new CourseDimension();
    dimension.setCourse(course);
    dimension.setType("New Topic");
    dimension.setDimension("Example topic: \"Data Structure: List\"");
    quizeraDAO.insertCourseDimension(dimension);

    response.sendRedirect(
        request.getContextPath() + "/dashboard/courses/dimensions/list?id=" + courseId);
  }
}
