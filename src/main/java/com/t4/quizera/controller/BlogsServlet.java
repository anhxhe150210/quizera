package com.t4.quizera.controller;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Blog;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "BlogsServlet", value = "/blogs")
public class BlogsServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public BlogsServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String blogId = request.getParameter("id");
    if (blogId != null) {
      Blog blog = quizeraDAO.getBlog(Integer.parseInt(blogId));
      request.setAttribute("blog", blog);
      request.getRequestDispatcher("/WEB-INF/blog/detail.jsp").forward(request, response);
      return;
    }

    // Search and categories
    String query = request.getParameter("query");
    String categoryId = request.getParameter("category");

    List<Blog> blogs = quizeraDAO.getBlogs();
    if (query != null) {
      blogs =
          blogs.stream()
              .filter(b -> b.getTitle().toLowerCase().contains(query.toLowerCase()))
              .collect(Collectors.toList());
      request.setAttribute("query", query);
    }
    if (categoryId != null) {
      blogs =
          blogs.stream()
              .filter(
                  b ->
                      b.getCategories().stream()
                          .anyMatch(c -> String.valueOf(c.getId()).equals(categoryId)))
              .collect(Collectors.toList());
    }
    request.setAttribute("blogs", blogs);

    request.getRequestDispatcher("/WEB-INF/blog/list.jsp").forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {}
}
