package com.t4.quizera.controller;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.User;
import com.t4.quizera.util.Utils;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "ChangePasswordServlet", value = "/changepwd")
public class ChangePasswordServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public ChangePasswordServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.getRequestDispatcher("/WEB-INF/changepwd.jsp").forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    User user = (User) request.getSession().getAttribute("user");
    if (user == null) {
      response.sendRedirect(request.getContextPath() + "/login");
      return;
    }

    String oldPassword = request.getParameter("oldpassword");
    String newPassword = request.getParameter("newpassword");
    String rePassword = request.getParameter("repassword");

    if (!user.getPasswordHash().equalsIgnoreCase(Utils.md5(oldPassword))) {
      back("Old password is not correct.", request, response);
      return;
    }
    if (newPassword.isEmpty() || !newPassword.equals(rePassword)) {
      back("New password is not valid or does not match.", request, response);
      return;
    }

    quizeraDAO.changePassword(user.getUsername(), Utils.md5(newPassword));
    back("Password changed.", request, response);
  }

  private void back(String message, HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    Map<String, Object> attrs = new HashMap<>();
    attrs.put("message", message);
    Utils.forwardAttributes("/WEB-INF/changepwd.jsp", attrs, request, response);
  }
}
