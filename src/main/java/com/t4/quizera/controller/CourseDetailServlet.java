package com.t4.quizera.controller;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Course;
import com.t4.quizera.model.Price;
import com.t4.quizera.model.User;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;

@WebServlet(name = "CourseDetailServlet", value = "/course")
public class CourseDetailServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public CourseDetailServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");
    String id = request.getParameter("id");

    Course course = quizeraDAO.getCourse(Integer.parseInt(id));
    request.setAttribute("course", course);
    request.setAttribute(
        "showPrice",
        course.getPrices().stream().min(Comparator.comparing(Price::getPrice)).orElse(null));
    request.setAttribute(
        "bought",
        user != null
            && quizeraDAO.getUserSubscriptions(user).stream()
                .anyMatch(s -> s.getCourse().getId() == course.getId()));

    request.getRequestDispatcher("/WEB-INF/course-detail.jsp").forward(request, response);
  }
}
