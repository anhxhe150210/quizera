package com.t4.quizera.controller;

import com.t4.quizera.config.Secret;
import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.User;
import com.t4.quizera.util.Utils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "ConfirmResetPasswordServlet", value = "/confirmresetpwd")
public class ConfirmResetPasswordServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public ConfirmResetPasswordServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String jwt = request.getParameter("code");
    if (jwt == null || jwt.isEmpty()) {
      response.sendRedirect(request.getContextPath() + "/");
      return;
    }

    Jws<Claims> jws;
    try {
      jws =
          Jwts.parserBuilder()
              .requireSubject("ResetPassword")
              .setSigningKey(Secret.getSecretKey())
              .build()
              .parseClaimsJws(jwt);
    } catch (JwtException e) {
      response.getWriter().println("Code invalid.");
      return;
    }

    String username = (String) jws.getBody().get("username");

    User user = quizeraDAO.getUser(username);
    if (!user.getPasswordHash().equals(jws.getBody().get("passwordHash"))) {
      response.sendRedirect(request.getContextPath() + "/login?changed=-1");
      return;
    }

    request.setAttribute("code", jwt);
    request.getRequestDispatcher("/WEB-INF/confirmresetpwd.jsp").forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    String jwt = request.getParameter("code");
    if (jwt == null || jwt.isEmpty()) {
      response.sendRedirect(request.getContextPath() + "/");
      return;
    }

    Jws<Claims> jws;
    try {
      jws =
          Jwts.parserBuilder()
              .requireSubject("ResetPassword")
              .setSigningKey(Secret.getSecretKey())
              .build()
              .parseClaimsJws(jwt);
    } catch (JwtException e) {
      response.getWriter().println("Code invalid.");
      return;
    }

    String username = (String) jws.getBody().get("username");

    User user = quizeraDAO.getUser(username);
    if (!user.getPasswordHash().equals(jws.getBody().get("passwordHash"))) {
      response.sendRedirect(request.getContextPath() + "/login?changed=-1");
      return;
    }

    String password = request.getParameter("newpassword");
    String repassword = request.getParameter("repassword");

    if (password.isEmpty()) {
      back(jwt, "Password must not be empty", request, response);
      return;
    }
    if (!password.equals(repassword)) {
      back(jwt, "Password does not match", request, response);
      return;
    }
    String md5 = Utils.md5(password);
    if (user.getPasswordHash().equals(md5)) {
      back(jwt, "Password must differ from the current password.", request, response);
      return;
    }

    quizeraDAO.changePassword(username, md5);

    response.sendRedirect(request.getContextPath() + "/login?changed=1");
  }

  private void back(
      String jwt, String message, HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    Map<String, Object> attrs = new HashMap<>();
    attrs.put("code", jwt);
    attrs.put("message", message);
    Utils.forwardAttributes("/WEB-INF/confirmresetpwd.jsp", attrs, request, response);
  }
}
