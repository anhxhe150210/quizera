package com.t4.quizera.controller;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Course;
import com.t4.quizera.model.Subscription;
import com.t4.quizera.model.User;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.time.Period;
import java.util.Date;

@WebServlet(name = "CheckoutServlet", value = "/checkout")
public class CheckoutServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public CheckoutServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");

    String courseIdString = request.getParameter("courseId");
    int courseId = Integer.parseInt(courseIdString);

    if (quizeraDAO.getUserSubscriptions(user).stream()
        .anyMatch(s -> s.getCourse().getId() == courseId)) {
      return;
    }

    Course course = quizeraDAO.getCourse(courseId);
    Subscription subscription = new Subscription();
    subscription.setUser(user);
    subscription.setCourse(course);
    subscription.setFromDate(new Date());
    subscription.setPrice(course.getLowestPrice());
    subscription.setToDate(
        Date.from(Instant.now().plus(Period.ofDays(subscription.getPrice().getDays()))));
    quizeraDAO.insertUserSubscription(subscription);

    String redirect = request.getParameter("redirect");

    response.sendRedirect(
        request.getContextPath() + (redirect != null ? redirect : "/course?id=" + courseId));
  }
}
