package com.t4.quizera.controller;

import com.t4.quizera.config.Secret;
import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Role;
import com.t4.quizera.model.User;
import com.t4.quizera.util.EmailUtils;
import com.t4.quizera.util.Utils;
import io.jsonwebtoken.Jwts;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "RegisterServlet", value = "/register")
public class RegisterServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public RegisterServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.getRequestDispatcher("/WEB-INF/register.jsp").forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String username = request.getParameter("username");
    String email = request.getParameter("email");
    String password = request.getParameter("password");
    String repassword = request.getParameter("repassword");

    if (password.isEmpty()) {
      back("Password must not be empty", request, response);
      return;
    }
    if (!password.equals(repassword)) {
      back("Password does not match", request, response);
      return;
    }
    if (!Utils.isEmailValid(email)) {
      back("Email is not valid.", request, response);
      return;
    }

    if (quizeraDAO.getUser(username) != null) {
      back("Username existed.", request, response);
      return;
    }
    if (quizeraDAO.getUserFromEmail(email) != null) {
      back("Email existed.", request, response);
      return;
    }

    User newUser = new User();
    newUser.setUsername(username);
    newUser.setEmail(email);
    newUser.setPasswordHash(Utils.md5(password));
    newUser.setRole(Role.GUEST);
    newUser.setVerified(false);
    quizeraDAO.add(newUser);

    String jwt =
        Jwts.builder()
            .setSubject("EmailVerification")
            .claim("username", username)
            .signWith(Secret.getSecretKey())
            .compact();
    String verifyUrl = Utils.getBaseURL(request) + "verify?code=" + jwt;
    EmailUtils.send(email, EmailUtils.registerTemplate(verifyUrl));

    back("Please check your email for confirmation details.", request, response);
  }

  private void back(String message, HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    Map<String, Object> attrs = new HashMap<>();
    attrs.put("message", message);
    Utils.forwardAttributes("/WEB-INF/register.jsp", attrs, request, response);
  }
}
