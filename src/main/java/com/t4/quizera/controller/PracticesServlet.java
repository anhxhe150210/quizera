package com.t4.quizera.controller;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.*;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@WebServlet(name = "PracticesServlet", value = "/practices/*")
public class PracticesServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public PracticesServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = request.getPathInfo();
    if (path == null || path.equals("/list")) {
      list(request, response);
      return;
    }
    switch (path) {
      case "/add":
        add(request, response);
        return;
      case "/handle":
        handle(request, response);
        return;
      case "/save":
        save(request, response);
        return;
      case "/review":
        review(request, response);
        return;
      case "/new":
        newPractice(request, response);
        return;
      case "/submit":
        submit(request, response);
        return;
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {}

  protected void list(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");
    List<Practice> practices =
        quizeraDAO.getPractices(user).stream()
            .sorted(Comparator.comparing(Practice::getStartAt).reversed())
            .collect(Collectors.toList());
    request.setAttribute("practices", practices);

    Map<Integer, String> scores = new HashMap<>();
    for (Practice p : practices) {
      scores.put(p.getId(), String.valueOf((int) Math.floor(getScore(p) * 100)));
    }
    request.setAttribute("scores", scores);

    request.getRequestDispatcher("/WEB-INF/user/practices.jsp").forward(request, response);
  }

  protected void handle(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");

    String practiceId = request.getParameter("id");
    Practice practice = quizeraDAO.getPractice(Integer.parseInt(practiceId));
    if (!practice.getUser().getUsername().equals(user.getUsername())) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }
    if (practice.isTimeout()) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }

    request.setAttribute("practice", practice);

    request.getRequestDispatcher("/WEB-INF/quiz/handle.jsp").forward(request, response);
  }

  protected void add(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");
    String quizId = request.getParameter("quizId");

    Quiz quiz = quizeraDAO.getQuiz(Integer.parseInt(quizId));

    Integer practiceId = quizeraDAO.insertPractice(user, quiz);
    // Insert questions
    List<Question> questions = quizeraDAO.getQuestions(quiz.getCourse());
    List<Question> chosenQuestions = new ArrayList<>();
    quiz.getQuestionFilters().stream()
        .map(
            f ->
                questions.stream()
                    .filter(f::match)
                    .limit(f.getNumberOfQuestions())
                    .collect(Collectors.toList()))
        .forEach(chosenQuestions::addAll);
    Collections.shuffle(chosenQuestions);
    chosenQuestions.stream()
        .limit(quiz.getNumberOfQuestions())
        .forEach(q -> quizeraDAO.insertPracticeQuestion(practiceId, q.getId()));

    response.sendRedirect(request.getContextPath() + "/practices/handle?id=" + practiceId);
  }

  protected void save(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    int practiceId = Integer.parseInt(request.getParameter("practiceId"));
    int questionId = Integer.parseInt(request.getParameter("questionId"));
    int answerId = Integer.parseInt(request.getParameter("answerId"));
    int ticked = Integer.parseInt(request.getParameter("ticked"));

    Practice practice = quizeraDAO.getPractice(practiceId);
    if (practice.isTimeout()) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }

    if (ticked == 1) {
      quizeraDAO.insertPracticeAnswer(practiceId, questionId, answerId);
    } else {
      quizeraDAO.deletePracticeAnswer(practiceId, questionId, answerId);
    }
  }

  protected void review(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");

    String practiceId = request.getParameter("id");
    Practice practice = quizeraDAO.getPractice(Integer.parseInt(practiceId));
    if (!practice.getUser().getUsername().equals(user.getUsername())) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }
    if (!practice.isTimeout()) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }

    request.setAttribute("practice", practice);
    request.setAttribute(
        "answers",
        practice.getQuiz().getCourse().getQuestions().stream()
            .map(Question::getAnswers)
            .flatMap(List::stream)
            .collect(Collectors.toList()));

    request.getRequestDispatcher("/WEB-INF/quiz/review.jsp").forward(request, response);
  }

  public int getCorrectAnswers(Practice practice) {
    int correctAnswers = 0;
    List<PracticeSubmission> practiceSubmissions = quizeraDAO.getPracticeSubmissions(practice);
    List<Answer> answers = new ArrayList<>(); // All answers for all questions
    for (PracticeSubmission ps : practiceSubmissions) {
      answers.addAll(ps.getQuestion().getAnswers());
    }

    for (PracticeSubmission submission : practice.getSubmissions()) {
      boolean questionCorrect = true;
      for (Answer answer : submission.getQuestion().getAnswers()) {
        boolean checked =
            submission.getAnswers().stream().anyMatch(a -> a.getId() == answer.getId());
        boolean correct =
            answers.stream().anyMatch(a -> a.getId() == answer.getId() && a.isCorrect());

        if ((checked && !correct) || (!checked && correct)) {
          questionCorrect = false;
          break;
        }
      }

      if (questionCorrect) {
        correctAnswers++;
      }
    }
    return correctAnswers;
  }

  public double getScore(Practice practice) {
    return (double) getCorrectAnswers(practice) / practice.getSubmissions().size();
  }

  protected void newPractice(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");
    String courseIdString = request.getParameter("courseId");

    request.setAttribute("courses", quizeraDAO.getCourses(user));
    if (courseIdString != null) {
      int courseId = Integer.parseInt(courseIdString);

      if (quizeraDAO.getUserSubscriptions(user).stream()
          .noneMatch(s -> s.getCourse().getId() == courseId)) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        return;
      }

      request.setAttribute("courseId", courseId);
      request.setAttribute("quizzes", quizeraDAO.getCourse(courseId).getQuizzes());
    }

    request.getRequestDispatcher("/WEB-INF/user/new-practice.jsp").forward(request, response);
  }

  protected void submit(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");

    String practiceId = request.getParameter("id");
    Practice practice = quizeraDAO.getPractice(Integer.parseInt(practiceId));
    if (!practice.getUser().getUsername().equals(user.getUsername())) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }
    if (practice.isTimeout()) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }
    practice.setForceSubmitted(true);
    quizeraDAO.updatePractice(practice);

    response.sendRedirect(request.getContextPath() + "/practices/review?id=" + practiceId);
  }
}
