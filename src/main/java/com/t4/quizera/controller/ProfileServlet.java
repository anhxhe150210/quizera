package com.t4.quizera.controller;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.User;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ProfileServlet", value = "/profile")
public class ProfileServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public ProfileServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.getRequestDispatcher("/WEB-INF/profile.jsp").forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String fullName = request.getParameter("fullName");
    String gender = request.getParameter("gender");
    String mobile = request.getParameter("mobile");
    String avatarUrl = request.getParameter("avatarUrl");

    User user = (User) request.getSession().getAttribute("user");
    user.setFullName(fullName);
    try {
      user.setGender(quizeraDAO.getGender(Integer.parseInt(gender)));
    } catch (Exception ignored) {
    }
    user.setMobile(mobile);
    user.setAvatarUrl(avatarUrl);

    quizeraDAO.updateUser(user);

    response.sendRedirect(request.getContextPath() + "/profile");
  }
}
