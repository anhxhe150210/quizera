package com.t4.quizera.controller;

import com.t4.quizera.dao.QuizeraService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "IndexServlet", value = "/index")
public class IndexServlet extends HttpServlet {
  private final QuizeraService quizeraService;

  @Inject
  public IndexServlet(QuizeraService quizeraService) {
    this.quizeraService = quizeraService;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setAttribute("courses", quizeraService.getLatestCourses(3));
    request.setAttribute("blogs", quizeraService.getLatestBlogs(3));
    request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {}
}
