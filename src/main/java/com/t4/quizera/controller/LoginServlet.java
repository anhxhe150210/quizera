package com.t4.quizera.controller;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.util.UidUtils;
import com.t4.quizera.util.Utils;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "LoginServlet", value = "/login")
public class LoginServlet extends HttpServlet {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public LoginServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String justVerified = request.getParameter("verify");
    if (justVerified != null && justVerified.equals("1")) {
      back("Your account has been verified. Please log in.", request, response);
      return;
    }

    String justChangedPassword = request.getParameter("changed");
    if (justChangedPassword != null) {
      switch (justChangedPassword) {
        case "0":
          back("A reset link has been sent to your email address.", request, response);
          break;
        case "1":
          back("Password changed.", request, response);
          break;
        case "-1":
          back("Link has expired.", request, response);
          break;
      }
      return;
    }

    request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String username = request.getParameter("username");
    String password = request.getParameter("password");
    if (username == null || password == null) {
      back("Please provide your information", request, response);
      return;
    }

    boolean correct = quizeraDAO.checkPassword(username, password);
    if (!correct) {
      back("Incorrect username/password", request, response);
      return;
    }

    String authKey = UidUtils.getUserPublicKey(username);

    Cookie usernameCookie = new Cookie("username", username);
    usernameCookie.setMaxAge(999999999);
    Cookie authKeyCookie = new Cookie("authKey", authKey);
    authKeyCookie.setMaxAge(999999999);
    response.addCookie(usernameCookie);
    response.addCookie(authKeyCookie);

    response.sendRedirect(request.getContextPath() + "/");
  }

  private void back(String message, HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    Map<String, Object> attrs = new HashMap<>();
    attrs.put("message", message);
    Utils.forwardAttributes("/WEB-INF/login.jsp", attrs, request, response);
  }
}
