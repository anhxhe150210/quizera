package com.t4.quizera.controller;

import com.t4.quizera.dao.QuizeraDAO;
import com.t4.quizera.model.Course;
import com.t4.quizera.model.User;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet(name = "CourseListServlet", value = "/courses")
public class CourseListServlet extends HttpServlet {

  private final QuizeraDAO quizeraDAO;
  private static final Integer ENTRIES_PER_PAGE = 10;

  @Inject
  public CourseListServlet(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    listCourse(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {}

  private void listCourse(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    User user = (User) request.getSession().getAttribute("user");
    String courseName = request.getParameter("courseName");
    String categoryId = request.getParameter("category");
    String pageInput = request.getParameter("page");
    int page = 1;
    if (pageInput != null) {
      try {
        page = Integer.parseInt(pageInput);
        if (page < 1) {
          page = 1;
        }
      } catch (NumberFormatException ignored) {
      }
    }
    request.setAttribute("page", page);

    List<Course> courses;
    Map<String, String> queries = new HashMap<>();
    if (courseName != null && categoryId == null) {
      courses = quizeraDAO.getCourses(courseName, page, ENTRIES_PER_PAGE);
      queries.put("courseName", courseName);
    } else if (courseName == null && categoryId != null) {
      courses = quizeraDAO.getCourses(Integer.parseInt(categoryId), page, ENTRIES_PER_PAGE);
      queries.put("categoryId", categoryId);
    } else {
      courses = quizeraDAO.getCourses();
    }

    String query =
        "?"
            + queries.entrySet().stream()
                .map(e -> e.getKey() + "=" + e.getValue())
                .collect(Collectors.joining("&"));
    request.setAttribute("category", categoryId);
    request.setAttribute("query", query);
    request.setAttribute("maxPage", quizeraDAO.getTotalCourses() / ENTRIES_PER_PAGE + 1);
    request.setAttribute("course", new ArrayList<>(courses));
    request.setAttribute(
        "coursesOwnership",
        courses.stream()
            .collect(
                Collectors.toMap(
                    Course::getId,
                    course ->
                        user != null
                            && quizeraDAO.getUserSubscriptions(user).stream()
                                .anyMatch(s -> s.getCourse().getId() == course.getId()))));

    request.getRequestDispatcher("/WEB-INF/course-list.jsp").forward(request, response);
  }
}
