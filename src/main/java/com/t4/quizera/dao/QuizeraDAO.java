package com.t4.quizera.dao;

import com.t4.quizera.model.*;
import com.t4.quizera.util.Utils;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class QuizeraDAO extends MysqlDAO {
  public String getPasswordHash(String username) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT password FROM user WHERE username = ?");
      stmt.setString(1, username);
      ResultSet result = stmt.executeQuery();
      if (result.next()) {
        return result.getString("password");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public boolean checkPassword(String username, String passwordWithoutHash) {
    String correctHash = getPasswordHash(username);
    return Objects.requireNonNull(Utils.md5(passwordWithoutHash)).equalsIgnoreCase(correctHash);
  }

  /** @param page starts at 1 */
  public List<User> getUsers(int page) {
    return getUsers(page, 10);
  }

  /** @param page starts at 1 */
  public List<User> getUsers(int page, int entriesPerPage) {
    page -= 1; // Compensate for verbose reason
    List<User> users = new ArrayList<>();
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM user LIMIT ? OFFSET ?");
      stmt.setInt(1, entriesPerPage);
      stmt.setInt(2, page * entriesPerPage);
      ResultSet rs = stmt.executeQuery();
      User user;
      while (rs.next()) {
        Optional<Role> role = Role.valueOf(rs.getInt("roleId"));
        user = new User();
        user.setUsername(rs.getString("username"));
        user.setEmail(rs.getString("email"));
        user.setPasswordHash(rs.getString("password"));
        user.setRole(role.orElse(Role.GUEST));
        user.setVerified(rs.getBoolean("verified"));
        user.setAvatarUrl(rs.getString("avatarUrl"));
        user.setFullName(rs.getString("fullName"));
        user.setMobile(rs.getString("mobile"));
        user.setGender(getGender(rs.getInt("gender")));
        users.add(user);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return users;
  }

  public Gender getGender(int id) {
    Gender gender = null;
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM gender WHERE id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        gender = new Gender();
        gender.setId(id);
        gender.setDescription(rs.getString("description"));
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return gender;
  }

  public int getTotalUsers() {
    int count = 0;
    try {
      PreparedStatement stmt = getConnection().prepareStatement("SELECT COUNT(*) FROM user");
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        count = rs.getInt(1);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return count;
  }

  public int getTotalCourses() {
    int count = 0;
    try {
      PreparedStatement stmt = getConnection().prepareStatement("SELECT COUNT(*) FROM course");
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        count = rs.getInt(1);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return count;
  }

  public List<User> getUsersByUsername(String usernameFilter, int page) {
    return getUsersByUsername(usernameFilter, page, 10);
  }

  public List<User> getUsersByUsername(String usernameFilter, int page, int entriesPerPage) {
    page -= 1; // Compensate for verbose reason
    List<User> users = new ArrayList<>();
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement("SELECT * FROM user WHERE username LIKE ? LIMIT ? OFFSET ?");
      stmt.setString(1, "%" + usernameFilter + "%");
      stmt.setInt(2, entriesPerPage);
      stmt.setInt(3, page * entriesPerPage);
      ResultSet rs = stmt.executeQuery();
      User user;
      while (rs.next()) {
        Optional<Role> role = Role.valueOf(rs.getInt("roleId"));
        user = new User();
        user.setUsername(rs.getString("username"));
        user.setEmail(rs.getString("email"));
        user.setPasswordHash(rs.getString("password"));
        user.setRole(role.orElse(Role.GUEST));
        user.setVerified(rs.getBoolean("verified"));
        user.setAvatarUrl(rs.getString("avatarUrl"));
        user.setFullName(rs.getString("fullName"));
        user.setMobile(rs.getString("mobile"));
        user.setGender(getGender(rs.getInt("gender")));
        users.add(user);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return users;
  }

  public List<User> getUsersByEmail(String emailFilter, int page) {
    return getUsersByUsername(emailFilter, page, 10);
  }

  public List<User> getUsersByEmail(String emailFilter, int page, int entriesPerPage) {
    page -= 1; // Compensate for verbose reason
    List<User> users = new ArrayList<>();
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement("SELECT * FROM user WHERE email LIKE ? LIMIT ? OFFSET ?");
      stmt.setString(1, "%" + emailFilter + "%");
      stmt.setInt(2, entriesPerPage);
      stmt.setInt(3, page * entriesPerPage);
      ResultSet rs = stmt.executeQuery();
      User user;
      while (rs.next()) {
        Optional<Role> role = Role.valueOf(rs.getInt("roleId"));
        user = new User();
        user.setUsername(rs.getString("username"));
        user.setEmail(rs.getString("email"));
        user.setPasswordHash(rs.getString("password"));
        user.setRole(role.orElse(Role.GUEST));
        user.setVerified(rs.getBoolean("verified"));
        user.setAvatarUrl(rs.getString("avatarUrl"));
        user.setFullName(rs.getString("fullName"));
        user.setMobile(rs.getString("mobile"));
        user.setGender(getGender(rs.getInt("gender")));
        users.add(user);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return users;
  }

  public User getUser(String username) {
    User user = null;
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM user WHERE username = ?");
      stmt.setString(1, username);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        Optional<Role> role = Role.valueOf(rs.getInt("roleId"));
        user = new User();
        user.setUsername(rs.getString("username"));
        user.setEmail(rs.getString("email"));
        user.setPasswordHash(rs.getString("password"));
        user.setRole(role.orElse(Role.GUEST));
        user.setVerified(rs.getBoolean("verified"));
        user.setAvatarUrl(rs.getString("avatarUrl"));
        user.setFullName(rs.getString("fullName"));
        user.setMobile(rs.getString("mobile"));
        user.setGender(getGender(rs.getInt("gender")));
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return user;
  }

  public void remove(String username) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("DELETE FROM user WHERE username = ?");
      stmt.setString(1, username);
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public User getUserFromEmail(String email) {
    User user = null;
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM user WHERE email = ?");
      stmt.setString(1, email);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        Optional<Role> role = Role.valueOf(rs.getInt("roleId"));
        user = new User();
        user.setUsername(rs.getString("username"));
        user.setEmail(rs.getString("email"));
        user.setPasswordHash(rs.getString("password"));
        user.setRole(role.orElse(Role.GUEST));
        user.setVerified(rs.getBoolean("verified"));
        user.setAvatarUrl(rs.getString("avatarUrl"));
        user.setFullName(rs.getString("fullName"));
        user.setMobile(rs.getString("mobile"));
        user.setGender(getGender(rs.getInt("gender")));
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return user;
  }

  public void add(User user) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement("INSERT INTO user (username, password, email) VALUES(?, ?, ?)");
      stmt.setString(1, user.getUsername());
      stmt.setString(2, user.getPasswordHash());
      stmt.setString(3, user.getEmail());
      stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void verify(String username) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("UPDATE user SET verified = 1 WHERE username = ?");
      stmt.setString(1, username);
      stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void changePassword(String username, String passwordHash) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("UPDATE user SET password = ? WHERE username = ?");
      stmt.setString(1, passwordHash);
      stmt.setString(2, username);
      stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public List<Course> getCourses(User user) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "SELECT * FROM course WHERE id IN (SELECT courseId FROM user_subscription WHERE username = ?)");
      stmt.setString(1, user.getUsername());
      return getCourses(stmt);
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return null;
  }

  public List<Course> getCourses() {
    PreparedStatement stmt = null;
    try {
      stmt = getConnection().prepareStatement("SELECT * FROM course");
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return getCourses(stmt);
  }

  public List<Course> getCourses(PreparedStatement stmt) {
    List<Course> courses = new ArrayList<>();
    try {
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        Course course = new Course();

        course.setId(rs.getInt("id"));
        course.setTitle(rs.getString("title"));
        course.setOwner(getUser(rs.getString("ownerUsername")));
        course.setDescription(rs.getString("description"));
        course.setUpdatedDate(rs.getDate("updatedDate"));
        course.setFeatured(rs.getBoolean("featured"));
        course.setThumbnailUrl(rs.getString("thumbnailUrl"));
        course.setArchived(rs.getBoolean("archived"));
        course.setPublished(rs.getBoolean("published"));
        course.setDimensions(getCourseDimensions(course));
        course.setQuestions(getQuestions(course));
        course.setLectures(getLectures(course));
        course.setQuizzes(getQuizzes(course));
        course.setPrices(getPrices(course));
        course.setCategories(getCourseCategories(course));

        courses.add(course);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return courses;
  }

  public Course getCourse(int id) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM course WHERE id = ?");
      stmt.setInt(1, id);
      return getCourses(stmt).stream().findFirst().orElse(null);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public List<Course> getCourses(String getCourseName, int page, int entriesPerPage) {
    page -= 1; // Compensate for verbose reason
    List<Course> courses = new ArrayList<>();
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement("SELECT * FROM course WHERE title LIKE ? LIMIT ? OFFSET ?");
      stmt.setString(1, "%" + getCourseName + "%");
      stmt.setInt(2, entriesPerPage);
      stmt.setInt(3, page * entriesPerPage);
      ResultSet rs = stmt.executeQuery();
      Course course;
      while (rs.next()) {
        course = new Course();
        course.setId(rs.getInt("id"));
        course.setTitle(rs.getString("title"));
        course.setOwner(getUser(rs.getString("ownerUsername")));
        course.setDescription(rs.getString("description"));
        course.setUpdatedDate(rs.getDate("updatedDate"));
        course.setFeatured(rs.getBoolean("featured"));
        course.setThumbnailUrl(rs.getString("thumbnailUrl"));
        course.setArchived(rs.getBoolean("archived"));
        course.setPublished(rs.getBoolean("published"));
        course.setDimensions(getCourseDimensions(course));
        course.setQuestions(getQuestions(course));
        course.setLectures(getLectures(course));
        course.setQuizzes(getQuizzes(course));
        course.setPrices(getPrices(course));
        course.setCategories(getCourseCategories(course));

        courses.add(course);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return courses;
  }

  public List<Course> getCourses(int categoryId, int page, int entriesPerPage) {
    page -= 1; // Compensate for verbose reason
    List<Course> courses = new ArrayList<>();
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "SELECT * FROM course join course_category ON id = courseId where categoryId = ? LIMIT ? OFFSET ?");
      stmt.setInt(1, categoryId);
      stmt.setInt(2, entriesPerPage);
      stmt.setInt(3, page * entriesPerPage);
      ResultSet rs = stmt.executeQuery();
      Course course;
      while (rs.next()) {
        course = new Course();
        course.setId(rs.getInt("id"));
        course.setTitle(rs.getString("title"));
        course.setOwner(getUser(rs.getString("ownerUsername")));
        course.setDescription(rs.getString("description"));
        course.setUpdatedDate(rs.getDate("updatedDate"));
        course.setFeatured(rs.getBoolean("featured"));
        course.setThumbnailUrl(rs.getString("thumbnailUrl"));
        course.setArchived(rs.getBoolean("archived"));
        course.setPublished(rs.getBoolean("published"));
        course.setDimensions(getCourseDimensions(course));
        course.setQuestions(getQuestions(course));
        course.setLectures(getLectures(course));
        course.setQuizzes(getQuizzes(course));
        course.setPrices(getPrices(course));
        course.setCategories(getCourseCategories(course));

        courses.add(course);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return courses;
  }

  public List<Lecture> getLectures(Course course) {
    List<Lecture> lectures = new ArrayList<>();
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM lecture WHERE courseId = ?");
      stmt.setInt(1, course.getId());
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        Lecture lecture = new Lecture();
        lecture.setId(rs.getInt("id"));
        lecture.setCourse(course);
        lecture.setTitle(rs.getString("title"));
        lecture.setContent(rs.getString("content"));
        lectures.add(lecture);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return lectures;
  }

  public Lecture getLecture(int id) {
    Lecture lecture = null;
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM lecture WHERE id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        lecture = new Lecture();
        lecture.setId(rs.getInt("id"));
        lecture.setCourse(getCourse(rs.getInt("courseId")));
        lecture.setTitle(rs.getString("title"));
        lecture.setContent(rs.getString("content"));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return lecture;
  }

  public List<Quiz> getQuizzes(Course course) {
    List<Quiz> quizzes = new ArrayList<>();
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM quiz WHERE courseId = ?");
      stmt.setInt(1, course.getId());
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        Quiz quiz = new Quiz();
        quiz.setId(rs.getInt("id"));
        quiz.setCourse(course);
        quiz.setTitle(rs.getString("title"));
        quiz.setLevel(getQuizLevel(rs.getInt("levelId")));
        quiz.setDurationInSeconds(rs.getInt("durationInSeconds"));
        quiz.setNumberOfQuestions(rs.getInt("numberOfQuestions"));
        quiz.setQuestionFilters(getQuestionFilters(quiz));
        quizzes.add(quiz);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return quizzes;
  }

  public Quiz getQuiz(int id) {
    Quiz quiz = null;
    try {
      PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM quiz WHERE id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        quiz = new Quiz();
        quiz.setId(id);
        quiz.setCourse(getCourse(rs.getInt("courseId")));
        quiz.setTitle(rs.getString("title"));
        quiz.setLevel(getQuizLevel(rs.getInt("levelId")));
        quiz.setDurationInSeconds(rs.getInt("durationInSeconds"));
        quiz.setNumberOfQuestions(rs.getInt("numberOfQuestions"));
        quiz.setQuestionFilters(getQuestionFilters(quiz));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return quiz;
  }

  public List<Question> getQuestions(Course course) {
    List<Question> questions = new ArrayList<>();
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM question WHERE courseId = ?");
      stmt.setInt(1, course.getId());
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        Question question = new Question();
        question.setId(rs.getInt("id"));
        question.setCourse(course);
        question.setQuestion(rs.getString("question"));
        question.setAnswers(getAnswers(question));
        question.setDimension(getCourseDimension(rs.getInt("dimensionId"), course));

        questions.add(question);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return questions;
  }

  public Question getQuestion(int id) {
    Question question = null;
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM question WHERE id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        question = new Question();
        question.setId(id);
        Course course = getCourse(rs.getInt("courseId"));
        question.setCourse(course);
        question.setQuestion(rs.getString("question"));
        question.setAnswers(getAnswers(question));
        question.setDimension(getCourseDimension(rs.getInt("dimensionId"), course));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return question;
  }

  public List<Answer> getAnswers(PreparedStatement stmt, Question question) {
    List<Answer> answers = new ArrayList<>();
    try {
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        Answer answer = new Answer();
        answer.setId(rs.getInt("id"));
        answer.setQuestion(question);
        answer.setAnswer(rs.getString("answer"));
        answer.setExplanation(rs.getString("explanation"));
        answer.setCorrect(rs.getBoolean("correct"));
        answers.add(answer);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return answers;
  }

  public List<Answer> getAnswers(Question question) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM answer WHERE questionId = ?");
      stmt.setInt(1, question.getId());
      return getAnswers(stmt, question);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public Answer getAnswer(int id) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM answer WHERE id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        Answer answer = new Answer();
        answer.setId(id);
        answer.setQuestion(getQuestion(rs.getInt("questionId")));
        answer.setAnswer(rs.getString("answer"));
        answer.setExplanation(rs.getString("explanation"));
        answer.setCorrect(rs.getBoolean("correct"));

        return answer;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public void removeQuestion(int id) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("DELETE FROM question WHERE id = ?");
      stmt.setInt(1, id);
      stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void removeAnswer(int id) {
    try {
      PreparedStatement stmt = getConnection().prepareStatement("DELETE FROM answer WHERE id = ?");
      stmt.setInt(1, id);
      stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void addAnswer(Answer answer) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "INSERT INTO answer (questionId, answer, correct, explanation) VALUES (?, ?, ?, ?)");
      stmt.setInt(1, answer.getQuestion().getId());
      stmt.setString(2, answer.getAnswer());
      stmt.setBoolean(3, answer.isCorrect());
      stmt.setString(4, answer.getExplanation());
      stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void addQuestion(Question question) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "INSERT INTO question (courseId, question) VALUES (?, ?)",
                  Statement.RETURN_GENERATED_KEYS);
      stmt.setInt(1, question.getCourse().getId());
      stmt.setString(2, question.getQuestion());
      stmt.execute();

      try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
        if (generatedKeys.next()) {
          question.setId((int) generatedKeys.getLong(1));

          for (Answer a : question.getAnswers()) {
            addAnswer(a);
          }
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void updateQuestion(Question question) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement("UPDATE question SET question = ?, dimensionId = ? WHERE id = ?");
      stmt.setString(1, question.getQuestion());
      stmt.setInt(2, question.getDimension().getId());
      stmt.setInt(3, question.getId());
      stmt.execute();

      for (Answer a : question.getAnswers()) {
        updateAnswer(a);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void updateAnswer(Answer answer) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "UPDATE answer SET answer = ?, explanation = ?, correct = ? WHERE id = ?");
      stmt.setString(1, answer.getAnswer());
      stmt.setString(2, answer.getExplanation());
      stmt.setBoolean(3, answer.isCorrect());
      stmt.setInt(4, answer.getId());
      stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Blog getBlog(int id) {
    try {
      PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM blog WHERE id = ?");
      stmt.setInt(1, id);

      return getBlogs(stmt).stream().findFirst().orElse(null);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public List<Blog> getBlogsLatest(int page, int entriesPerPage) {
    List<Blog> list = null;
    page -= 1; // verbose reason
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement("SELECT * FROM blog ORDER BY publicDate DESC LIMIT ? OFFSET ?");
      stmt.setInt(1, entriesPerPage);
      stmt.setInt(2, page * entriesPerPage);

      list = getBlogs(stmt);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return list;
  }

  public List<Blog> getBlogsOldest(int page, int entriesPerPage) {
    List<Blog> list = null;
    page -= 1; // verbose reason
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement("SELECT * FROM blog ORDER BY publicDate ASC LIMIT ? OFFSET ?");
      stmt.setInt(1, entriesPerPage);
      stmt.setInt(2, page * entriesPerPage);

      list = getBlogs(stmt);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return list;
  }

  public int getNumberOfBlogs() {
    try {
      PreparedStatement stmt = getConnection().prepareStatement("SELECT COUNT(*) FROM blog");
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        return rs.getInt(1);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return 0;
  }

  public BlogCategory getBlogCategory(int id) {
    BlogCategory category = null;
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM blog_category_list WHERE id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        category = new BlogCategory();
        category.setId(id);
        category.setDescription(rs.getString("description"));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return category;
  }

  public List<BlogCategory> getBlogCategories(int blogId) {
    List<BlogCategory> categories = new ArrayList<>();
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM blog_category WHERE blogId = ?");
      stmt.setInt(1, blogId);
      ResultSet rs = stmt.executeQuery();
      while (rs.next()) {
        BlogCategory category = getBlogCategory(rs.getInt("categoryId"));
        categories.add(category);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return categories;
  }

  public List<Blog> getBlogs() {
    try {
      PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM blog");
      return getBlogs(stmt);
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return null;
  }

  public List<Blog> getBlogs(PreparedStatement stmt) {
    List<Blog> blogs = new ArrayList<>();
    try {
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Blog blog = new Blog();
        blog.setId(rs.getInt("id"));
        blog.setAuthor(getUser(rs.getString("authorUser")));
        blog.setTitle(rs.getString("title"));
        blog.setContent(rs.getString("content"));
        blog.setPublicDate(rs.getDate("publicDate"));
        blog.setThumbnailUrl(rs.getString("thumbnailUrl"));
        blog.setCategories(getBlogCategories(blog.getId()));

        blogs.add(blog);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return blogs;
  }

  public List<Price> getPrices() {
    try {
      PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM price");
      return getPrices(stmt);
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return null;
  }

  public List<Price> getPrices(PreparedStatement stmt) {
    List<Price> prices = new ArrayList<>();
    try {
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Price price = new Price();
        price.setId(rs.getInt("id"));
        price.setCourse(getCourse(rs.getInt("courseId")));
        price.setDays(rs.getInt("days"));
        price.setPrice(rs.getDouble("price"));
        price.setSaleFrom(rs.getDouble("saleFrom"));

        prices.add(price);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return prices;
  }

  public Price getPrice(int id) {
    try {
      PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM price WHERE id = ?");
      stmt.setInt(1, id);
      return getPrices(stmt).stream().findFirst().orElse(null);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public List<Price> getPrices(Course course) {
    List<Price> prices = new ArrayList<>();
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM price WHERE courseId = ?");
      stmt.setInt(1, course.getId());
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Price price = new Price();
        price.setId(rs.getInt("id"));
        price.setCourse(course);
        price.setDays(rs.getInt("days"));
        price.setPrice(rs.getDouble("price"));
        price.setSaleFrom(rs.getDouble("saleFrom"));

        prices.add(price);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return prices;
  }

  public List<BlogCategory> getBlogCategories() {
    try {
      PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM blog_category_list");
      return getBlogCategories(stmt);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public List<BlogCategory> getBlogCategories(PreparedStatement stmt) {
    List<BlogCategory> categories = new ArrayList<>();
    try {
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        BlogCategory category = new BlogCategory();
        category.setId(rs.getInt("id"));
        category.setDescription(rs.getString("description"));

        categories.add(category);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return categories;
  }

  public List<CourseCategory> getCourseCategories() {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM course_category_list");
      return getCourseCategories(stmt);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public List<CourseCategory> getCourseCategories(PreparedStatement stmt) {
    List<CourseCategory> categories = new ArrayList<>();
    try {
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        CourseCategory category = new CourseCategory();
        category.setId(rs.getInt("id"));
        category.setDescription(rs.getString("description"));

        categories.add(category);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return categories;
  }

  public CourseCategory getCourseCategory(int id) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM course_category_list WHERE id = ?");
      stmt.setInt(1, id);
      return getCourseCategories(stmt).stream().findFirst().orElse(null);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public List<CourseCategory> getCourseCategories(Course course) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "SELECT * FROM course_category_list WHERE id IN (SELECT categoryId FROM course_category WHERE courseId = ?)");
      stmt.setInt(1, course.getId());
      return getCourseCategories(stmt);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public List<CourseDimension> getCourseDimensions(Course course) {
    List<CourseDimension> dims = new ArrayList<>();
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM course_dimension WHERE courseId = ?");
      stmt.setInt(1, course.getId());
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        CourseDimension dimension = new CourseDimension();
        dimension.setId(rs.getInt("id"));
        dimension.setCourse(course);
        dimension.setType(rs.getString("type"));
        dimension.setDimension(rs.getString("dimension"));

        dims.add(dimension);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return dims;
  }

  public List<CourseDimension> getCourseDimensions(Question question) {
    List<CourseDimension> dimensions = new ArrayList<>();
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "SELECT * FROM course_dimension WHERE id IN (SELECT dimensionId FROM question_dimension WHERE questionId = ?)");
      stmt.setInt(1, question.getId());
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        CourseDimension dimension = new CourseDimension();
        dimension.setId(rs.getInt("id"));
        dimension.setCourse(question.getCourse());
        dimension.setType(rs.getString("type"));
        dimension.setDimension(rs.getString("dimension"));

        dimensions.add(dimension);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return dimensions;
  }

  public Level getQuizLevel(int id) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM quiz_level_list WHERE id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        Level level = new Level();
        level.setId(id);
        level.setDescription(rs.getString("description"));
        return level;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public List<Slider> getSliders() {
    try {
      PreparedStatement stmt = getConnection().prepareStatement("SELECT * FROM slider");
      return getSliders(stmt);
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return null;
  }

  public List<Slider> getSliders(PreparedStatement stmt) {
    List<Slider> sliders = new ArrayList<>();
    try {
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Slider slider = new Slider();
        slider.setId(rs.getInt("id"));
        slider.setTitle(rs.getString("title"));
        slider.setContent(rs.getString("content"));
        slider.setHidden(rs.getBoolean("hidden"));

        sliders.add(slider);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return sliders;
  }

  public List<Exam> getExams(PreparedStatement stmt) {
    List<Exam> exams = new ArrayList<>();
    try {
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Exam exam = new Exam();
        exam.setId(rs.getInt("id"));
        exam.setUser(getUser(rs.getString("username")));
        exam.setCourse(getCourse(rs.getInt("courseId")));
        exam.setStartAt(rs.getDate("startAt"));
        exam.setEndAt(rs.getDate("endAt"));
        exam.setSubmissions(getExamSubmissions(exam));

        exams.add(exam);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return exams;
  }

  public List<ExamSubmission> getExamSubmissions(Exam exam) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM user_exam_submission WHERE examId = ?");
      stmt.setInt(1, exam.getId());
      return getExamSubmissions(exam, stmt);
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return null;
  }

  public List<ExamSubmission> getExamSubmissions(Exam exam, PreparedStatement stmt) {
    List<ExamSubmission> submissions = new ArrayList<>();
    try {
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        ExamSubmission submission = new ExamSubmission();
        submission.setExam(exam);
        submission.setQuestion(getQuestion(rs.getInt("questionId")));
        submission.setAnswer(getAnswer(rs.getInt("answerId")));

        submissions.add(submission);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return submissions;
  }

  public List<Practice> getPractices(User user) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM user_practice WHERE username = ?");
      stmt.setString(1, user.getUsername());
      return getPractices(stmt);
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return null;
  }

  public List<Practice> getPractices(PreparedStatement stmt) {
    List<Practice> practices = new ArrayList<>();
    try {
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Practice practice = new Practice();
        practice.setId(rs.getInt("id"));
        practice.setUser(getUser(rs.getString("username")));
        practice.setQuiz(getQuiz(rs.getInt("quizId")));
        practice.setStartAt(java.util.Date.from(rs.getTimestamp("startAt").toInstant()));
        practice.setFilterString(rs.getString("filterString"));
        practice.setSubmissions(getPracticeSubmissions(practice));
        practice.setForceSubmitted(rs.getBoolean("forceSubmitted"));

        practices.add(practice);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return practices;
  }

  public List<PracticeSubmission> getPracticeSubmissions(Practice practice) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement("SELECT * FROM user_practice_question WHERE practiceId = ?");
      stmt.setInt(1, practice.getId());
      return getPracticeSubmissions(practice, stmt);
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return null;
  }

  public List<PracticeSubmission> getPracticeSubmissions(
      Practice practice, PreparedStatement stmt) {
    List<PracticeSubmission> submissions = new ArrayList<>();
    try {
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        PracticeSubmission submission = new PracticeSubmission();
        submission.setPractice(practice);
        submission.setQuestion(getQuestion(rs.getInt("questionId")));
        submission.setAnswers(
            getPracticeAnswers(practice.getId(), submission.getQuestion().getId()));

        submissions.add(submission);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return submissions;
  }

  public List<Subscription> getUserSubscriptions(User user) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM user_subscription WHERE username = ?");
      stmt.setString(1, user.getUsername());
      return getUserSubscriptions(user, stmt);
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return null;
  }

  public List<Subscription> getUserSubscriptions(User user, PreparedStatement stmt) {
    List<Subscription> subscriptions = new ArrayList<>();
    try {
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        Subscription subscription = new Subscription();
        subscription.setId(rs.getInt("id"));
        subscription.setUser(user);
        subscription.setCourse(getCourse(rs.getInt("courseId")));
        subscription.setPrice(getPrice(rs.getInt("priceId")));
        subscription.setFromDate(rs.getDate("fromDate"));
        subscription.setToDate(rs.getDate("toDate"));

        subscriptions.add(subscription);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return subscriptions;
  }

  public void updateUser(User user) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "UPDATE user SET password = ?, roleId = ?, email = ?, verified = ?, fullName = ?, gender = ?, mobile = ?, avatarUrl = ? WHERE username = ?");
      stmt.setString(1, user.getPasswordHash());
      stmt.setInt(2, user.getRole().id);
      stmt.setString(3, user.getEmail());
      stmt.setBoolean(4, user.isVerified());
      stmt.setString(5, user.getFullName());
      if (user.getGender() == null) {
        stmt.setNull(6, Types.INTEGER);
      } else {
        stmt.setInt(6, user.getGender().getId());
      }
      stmt.setString(7, user.getMobile());
      stmt.setString(8, user.getAvatarUrl());
      stmt.setString(9, user.getUsername());

      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void updateCourse(Course course) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "UPDATE course SET title = ?, description = ?, updatedDate = ?,"
                      + " featured = ?, thumbnailUrl = ?, archived = ?, published = ? WHERE id = ?");
      stmt.setString(1, course.getTitle());
      stmt.setString(2, course.getDescription());
      stmt.setDate(3, java.sql.Date.valueOf(LocalDate.now()));
      stmt.setBoolean(4, course.isFeatured());
      stmt.setString(5, course.getThumbnailUrl());
      stmt.setBoolean(6, course.isArchived());
      stmt.setBoolean(7, course.isPublished());
      stmt.setInt(8, course.getId());
      stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void updateCourseCategories(Course course) {
    try {
      // Categories
      PreparedStatement stmt =
          getConnection().prepareStatement("DELETE FROM course_category WHERE courseId = ?");
      stmt.setInt(1, course.getId());
      stmt.execute();
      for (CourseCategory cc : course.getCategories()) {
        stmt =
            getConnection()
                .prepareStatement(
                    "INSERT INTO course_category (courseId, categoryId) VALUES (?, ?)");
        stmt.setInt(1, course.getId());
        stmt.setInt(2, cc.getId());
        stmt.execute();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void newCourse(Course course) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "INSERT INTO course (title, ownerUsername, description, updatedDate, featured, thumbnailUrl, archived, published) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

      stmt.setString(1, course.getTitle());
      stmt.setString(2, course.getOwner().getUsername());
      stmt.setString(3, course.getDescription());
      stmt.setDate(4, java.sql.Date.valueOf(LocalDate.now()));
      stmt.setBoolean(5, course.isFeatured());
      stmt.setString(6, course.getThumbnailUrl());
      stmt.setBoolean(7, course.isArchived());
      stmt.setBoolean(8, course.isPublished());
      stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void insertCourseDimension(CourseDimension dimension) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "INSERT INTO course_dimension (courseId, type, dimension) VALUES (?, ?, ?)");
      stmt.setInt(1, dimension.getCourse().getId());
      stmt.setString(2, dimension.getType());
      stmt.setString(3, dimension.getDimension());
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void deleteCourseDimension(int id) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("DELETE FROM course_dimension WHERE id = ?");
      stmt.setInt(1, id);
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public CourseDimension getCourseDimension(int id) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM course_dimension WHERE id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();

      if (rs.next()) {
        CourseDimension dimension = new CourseDimension();
        dimension.setId(rs.getInt("id"));
        dimension.setType(rs.getString("type"));
        dimension.setDimension(rs.getString("dimension"));
        dimension.setCourse(getCourse(rs.getInt("courseId")));

        return dimension;
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return null;
  }

  public CourseDimension getCourseDimension(int id, Course course) {
    return course.getDimensions().stream().filter(t -> t.getId() == id).findFirst().orElse(null);
  }

  public void updateCourseDimension(CourseDimension courseDimension) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement("UPDATE course_dimension SET type = ?, dimension = ? WHERE id = ?");
      stmt.setString(1, courseDimension.getType());
      stmt.setString(2, courseDimension.getDimension());
      stmt.setInt(3, courseDimension.getId());
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void deleteLecture(int id) {
    try {
      PreparedStatement stmt = getConnection().prepareStatement("DELETE FROM lecture WHERE id = ?");
      stmt.setInt(1, id);
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void deleteLecture(Lecture lecture) {
    deleteLecture(lecture.getId());
  }

  public void updateLecture(Lecture lecture) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "UPDATE lecture SET courseId = ?, title = ?, content = ? WHERE id = ?");
      stmt.setInt(1, lecture.getCourse().getId());
      stmt.setString(2, lecture.getTitle());
      stmt.setString(3, lecture.getContent());
      stmt.setInt(4, lecture.getId());
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void insertLecture(int courseId) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement("INSERT INTO lecture (courseId, title, content) VALUES (?, ?, ?)");
      stmt.setInt(1, courseId);
      stmt.setString(2, "Title");
      stmt.setString(3, "Content");
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void insertQuiz(int courseId) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "INSERT INTO quiz (courseId, title, levelId, durationInSeconds, numberOfQuestions) VALUES (?, ?, ?, ?, ?)");
      stmt.setInt(1, courseId);
      stmt.setString(2, "New Quiz");
      stmt.setInt(3, 1);
      stmt.setInt(4, 900);
      stmt.setInt(5, 20);
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void deleteQuiz(Quiz quiz) {
    deleteQuiz(quiz.getId());
  }

  public void deleteQuiz(int id) {
    try {
      PreparedStatement stmt = getConnection().prepareStatement("DELETE FROM quiz WHERE id = ?");
      stmt.setInt(1, id);
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void updateQuiz(Quiz quiz) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "UPDATE quiz SET title = ?, levelId = ?, durationInSeconds = ?, numberOfQuestions = ? WHERE id = ?");
      stmt.setString(1, quiz.getTitle());
      stmt.setInt(2, quiz.getLevel().getId());
      stmt.setInt(3, quiz.getDurationInSeconds());
      stmt.setInt(4, quiz.getNumberOfQuestions());
      stmt.setInt(5, quiz.getId());
      stmt.execute();

      stmt = getConnection().prepareStatement("DELETE FROM quiz_question WHERE quizId = ?");
      stmt.setInt(1, quiz.getId());
      stmt.execute();

      for (QuizQuestionFilter filter : quiz.getQuestionFilters()) {
        stmt =
            getConnection()
                .prepareStatement(
                    "INSERT INTO quiz_question (quizId, filterBy, target, numberOfQuestions) VALUES (?, ?, ?, ?)");
        stmt.setInt(1, quiz.getId());
        stmt.setInt(2, filter.getFilterBy().getFilterNumber());
        stmt.setString(3, filter.getTarget());
        stmt.setInt(4, filter.getNumberOfQuestions());
        stmt.execute();
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void deleteCourse(int id) {
    try {
      PreparedStatement stmt = getConnection().prepareStatement("DELETE FROM course WHERE id = ?");
      stmt.setInt(1, id);
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void updateBlog(Blog blog) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "UPDATE blog SET authorUser = ?, title = ?, content = ?, thumbnailUrl = ? WHERE id = ?");
      if (blog.getAuthor() != null) {
        stmt.setString(1, blog.getAuthor().getUsername());
      } else {
        stmt.setNull(1, Types.VARCHAR);
      }
      stmt.setString(2, blog.getTitle());
      stmt.setString(3, blog.getContent());
      stmt.setString(4, blog.getThumbnailUrl());
      stmt.setInt(5, blog.getId());
      stmt.execute();

      stmt = getConnection().prepareStatement("DELETE FROM blog_category WHERE blogId = ?");
      stmt.setInt(1, blog.getId());
      stmt.execute();
      for (BlogCategory bc : blog.getCategories()) {
        stmt =
            getConnection()
                .prepareStatement("INSERT INTO blog_category (blogId, categoryId) VALUES (?, ?)");
        stmt.setInt(1, blog.getId());
        stmt.setInt(2, bc.getId());
        stmt.execute();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void newBlog(Blog blog) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "INSERT INTO blog (authorUser, title, content, publicDate, thumbnailUrl) VALUES (?, ?, ?, ?, ?)");

      if (blog.getAuthor() != null) {
        stmt.setString(1, blog.getAuthor().getUsername());
      } else {
        stmt.setNull(1, Types.VARCHAR);
      }
      stmt.setString(2, blog.getTitle());
      stmt.setString(3, blog.getContent());
      stmt.setDate(
          4,
          Date.valueOf(
              blog.getPublicDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
      stmt.setString(5, blog.getThumbnailUrl());
      stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void deleteBlog(int id) {
    try {
      PreparedStatement stmt = getConnection().prepareStatement("DELETE FROM blog WHERE id = ?");
      stmt.setInt(1, id);
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public Practice getPractice(int id) {
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM user_practice WHERE id = ?");
      stmt.setInt(1, id);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        Practice practice = new Practice();
        practice.setId(id);
        practice.setUser(getUser(rs.getString("username")));
        practice.setQuiz(getQuiz(rs.getInt("quizId")));
        practice.setStartAt(java.util.Date.from(rs.getTimestamp("startAt").toInstant()));
        practice.setFilterString(rs.getString("filterString"));
        practice.setSubmissions(getPracticeSubmissions(practice));
        practice.setForceSubmitted(rs.getBoolean("forceSubmitted"));
        return practice;
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return null;
  }

  public Integer insertPractice(User user, Quiz quiz) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "INSERT INTO user_practice (username, quizId, startAt) VALUES (?, ?, ?)",
                  Statement.RETURN_GENERATED_KEYS);
      stmt.setString(1, user.getUsername());
      stmt.setInt(2, quiz.getId());
      stmt.setTimestamp(3, new Timestamp(Instant.now().toEpochMilli()));
      stmt.execute();
      try (ResultSet rs = stmt.getGeneratedKeys()) {
        if (rs.next()) {
          return rs.getInt(1);
        }
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return null;
  }

  public void insertPracticeQuestion(int practiceId, int questionId) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "INSERT INTO user_practice_question (practiceId, questionId) VALUES (?, ?)");
      stmt.setInt(1, practiceId);
      stmt.setInt(2, questionId);
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void insertPracticeAnswer(int practiceId, int questionId, Integer answerId) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "INSERT INTO user_practice_answer (practiceId, questionId, answerId) VALUES (?, ?, ?)");
      stmt.setInt(1, practiceId);
      stmt.setInt(2, questionId);
      stmt.setInt(3, answerId);
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void deletePracticeAnswer(int practiceId, int questionId, Integer answerId) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "DELETE FROM user_practice_answer WHERE practiceId = ? AND questionId = ? AND answerId = ?");
      stmt.setInt(1, practiceId);
      stmt.setInt(2, questionId);
      stmt.setInt(3, answerId);
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public List<Answer> getPracticeAnswers(int practiceId, int questionId) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "SELECT * FROM answer WHERE id IN (SELECT answerId FROM user_practice_answer WHERE practiceId = ? AND user_practice_answer.questionId = ?)");
      stmt.setInt(1, practiceId);
      stmt.setInt(2, questionId);
      return getAnswers(stmt, getQuestion(questionId));
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return null;
  }

  public void updatePractice(Practice practice) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "UPDATE user_practice SET startAt = ?, forceSubmitted = ? WHERE id = ?");
      stmt.setTimestamp(1, new Timestamp(practice.getStartAt().getTime()));
      stmt.setBoolean(2, practice.isForceSubmitted());
      stmt.setInt(3, practice.getId());
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void insertUserSubscription(Subscription subscription) {
    try {
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "INSERT INTO user_subscription (username, courseId, priceId, fromDate, toDate) VALUES (?, ?, ?, ?, ?)");
      stmt.setString(1, subscription.getUser().getUsername());
      stmt.setInt(2, subscription.getCourse().getId());
      stmt.setInt(3, subscription.getPrice().getId());
      stmt.setTimestamp(4, new Timestamp(subscription.getFromDate().toInstant().toEpochMilli()));
      stmt.setTimestamp(5, new Timestamp(subscription.getToDate().toInstant().toEpochMilli()));
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public List<QuizQuestionFilter> getQuestionFilters(Quiz quiz) {
    List<QuizQuestionFilter> filters = new ArrayList<>();
    try {
      PreparedStatement stmt =
          getConnection().prepareStatement("SELECT * FROM quiz_question WHERE quizId = ?");
      stmt.setInt(1, quiz.getId());
      ResultSet rs = stmt.executeQuery();

      while (rs.next()) {
        QuizQuestionFilter filter = new QuizQuestionFilter();
        filter.setQuiz(quiz);
        filter.setFilterBy(FilterBy.of(rs.getInt("filterBy")));
        filter.setTarget(rs.getString("target"));
        filter.setNumberOfQuestions(rs.getInt("numberOfQuestions"));

        filters.add(filter);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return filters;
  }

  public void deletePrice(int id) {
    try {
      PreparedStatement stmt = getConnection().prepareStatement("DELETE FROM price WHERE id = ?");
      stmt.setInt(1, id);
      stmt.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void updatePrice(Price price) {
    try {
      // Prices
      PreparedStatement stmt =
          getConnection()
              .prepareStatement("UPDATE price SET days = ?, price = ?, saleFrom = ? WHERE id = ?");
      stmt.setInt(1, price.getDays());
      stmt.setDouble(2, price.getPrice());
      stmt.setDouble(3, price.getSaleFrom());
      stmt.setInt(4, price.getId());
      stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void insertPrice(Price price) {
    try {
      // Prices
      PreparedStatement stmt =
          getConnection()
              .prepareStatement(
                  "INSERT INTO price (courseId, days, price, saleFrom) VALUES (?, ?, ?, ?)");
      stmt.setInt(1, price.getCourse().getId());
      stmt.setInt(2, price.getDays());
      stmt.setDouble(3, price.getPrice());
      stmt.setDouble(4, price.getSaleFrom());
      stmt.execute();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
