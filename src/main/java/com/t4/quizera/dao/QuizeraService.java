package com.t4.quizera.dao;

import com.t4.quizera.model.Blog;
import com.t4.quizera.model.Course;

import javax.inject.Inject;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class QuizeraService {
  private final QuizeraDAO quizeraDAO;

  @Inject
  public QuizeraService(QuizeraDAO quizeraDAO) {
    this.quizeraDAO = quizeraDAO;
  }

  public List<Course> getLatestCourses(int n) {
    return quizeraDAO.getCourses().stream()
        .sorted(Comparator.comparing(Course::getUpdatedDate).reversed())
        .limit(3)
        .collect(Collectors.toList());
  }

  public List<Blog> getLatestBlogs(int n) {
    return quizeraDAO.getBlogs().stream()
        .sorted(Comparator.comparing(Blog::getPublicDate).reversed())
        .limit(3)
        .collect(Collectors.toList());
  }
}
