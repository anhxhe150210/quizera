package com.t4.quizera.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public abstract class MysqlDAO {

  private Connection connection = null;

  public Connection getConnection() {
    if (connection == null) {
      try {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        String url =
            System.getProperty("JDBC_URL", "jdbc:mysql://localhost:3306/quizera?useSSL=false");
        String username = System.getProperty("JDBC_USERNAME", "root");
        String password = System.getProperty("JDBC_PASSWORD", "123123");
        connection = DriverManager.getConnection(url, username, password);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return connection;
  }
}
